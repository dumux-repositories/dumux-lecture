// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//


#ifndef DUMUX_LENS2P_PROPERTIES_HH
#define DUMUX_LENS2P_PROPERTIES_HH


#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/n2.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>

#include <dumux/material/components/tabulatedcomponent.hh>

#include "lens2pspatialparams.hh"
#include "lens2pproblem.hh"



//////////
// Specify the properties for the lens problem
//////////

namespace Dumux::Properties {


// Create new type tags
namespace TTag {
struct LensTwoPProblem { using InheritsFrom = std::tuple<TwoP, BoxModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::LensTwoPProblem> { using type = LensTwoPProblem<TypeTag>; };


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::LensTwoPProblem> { using type = Dune::YaspGrid<2>; };



// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::LensTwoPProblem>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using WettingPhase = FluidSystems::OnePLiquid<Scalar, Components::TabulatedComponent<Components::H2O<Scalar>> >;
    using NonwettingPhase = FluidSystems::OnePGas<Scalar, Components::N2<Scalar> >;
    using type = FluidSystems::TwoPImmiscible<Scalar, WettingPhase, NonwettingPhase>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::LensTwoPProblem>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = Lens2pSpatialParams<FVGridGeometry, Scalar>;
};


} // end namespace Dumux::Properties

#endif
