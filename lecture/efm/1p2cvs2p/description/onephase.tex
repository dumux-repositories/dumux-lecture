% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\section{One-Phase System}
\label{onephase}
\subsection{Theory}
\label{theory}
The continuity equation has to be fullfilled.
\begin{equation}
\label{conti}
 \frac{\partial \rho }{\partial t} =  -\nabla \cdot \mathbf{v}
\end{equation}
The following equation describes the transport of a conservative tracer or hydrodynamic neutral contaminant
in saturated porous media in general:
\begin{equation}
\label{transport}
 \frac{\partial (c \phi)}{\partial t} = - \nabla \cdot [\underbrace{ \phi( c \mathbf{v} - \mathbf{D} \nabla c) }_{\vec{\mathcal{F}}}]+ r\phi
\end{equation}


and for the two-dimensional system:
\begin{equation}
\label{onephase_transp_gen}
\frac{\partial (c \phi)}{\partial t} =
-\frac{\partial}{\partial x}\left [ \phi \left (cv_x - \left ( D_m + D_t\right ) \frac {\partial c}{\partial x} \right ) \right ]
-\frac{\partial}{\partial z}\left [ \phi \left (cv_z - \left ( D_m + D_l\right ) \frac {\partial c}{\partial z} \right ) \right ]
+ r\phi
\end{equation}
In case of flow only in a vertical direction ($v_x = 0$), shown in figure \ref{fig_tr_eq}, we can simplify (\ref{onephase_transp_gen}) to
\begin{equation}
\label{onephase_transp}
\frac{\partial (c \phi)}{\partial t} =
\frac{\partial}{\partial x}\left ( \phi \left ( D_m + D_t\right ) \frac {\partial c}{\partial x} \right )
- \frac{\partial}{\partial z} \left [ \phi \left (cv_z - ( D_m + D_l)  \frac {\partial c}{\partial z} \right) \right]
+ r\phi
\end{equation}
\begin{equation}
\label{disp}
D_l = \alpha _l v_z \qquad \qquad  D_t = \alpha _t v_z
\end{equation}
\begin{equation}
\label{darcy}
v_z = \frac{- k}{\mu \phi} \cdot \frac{\partial p}{\partial z}
\end{equation}
With these assumptions and an incompressible fluid (\ref{conti}) reduces to
\begin{equation}
\label{conti_red}
\frac{\partial^2 p}{\partial z^2} =0.0
\end{equation}

The unknowns $p(x,z)$, $c(x,z)$ in equations (\ref{conti_red}),(\ref{onephase_transp}) lead to a closed system.

\begin{table}[ht!]
\label{transp_equation_param1}
\begin{tabular}[t]{lll}
$\rho$         & Density                             & [kg/m$^3$] \\
$c$            & Concentration                       & [kg/m$^3$] \\
$\phi$         & Effective porosity                  & [-] \\
$t$            & Time                                & [s] \\
$v_z$          & Seepage velocity in z-direction     & [m/s] \\
$D_m$          & Diffusion coefficient               & [m$^2$/s] \\
$D_l$          & Longitudinal dispersion coefficient & [m$^2$/s] \\
$D_t$          & Transversal dispersion coefficient  & [m$^2$/s] \\
$r$            & Mass source/sink rate               & [kg/m$^3$s] \\
$k$            & Permeability                        & [m$^2$] \\
$\mu$          & Dynamic viscosity                   & [kg/ms] \\
$p$            & Pressure                            & [Pa] \\
$\alpha _l$    & Longitudinal dispersivity           & [m] \\
$\alpha _t$    & Transversal dispersivity            & [m] \\
\end{tabular}
\end{table}
The units are given for a three dimensional case.

To have Neumann boundary conditions on the left and right hand side for equation (\ref{conti}) means $\mathbf{v}\cdot\mathbf{n}=v_{\mathrm{x}}= \frac{\partial p}{\partial x}=0$ and for (\ref{transport}): $\vec{\mathcal{F}}\cdot\mathbf{n}=0$ which can be reformulated as
\begin{equation}
 \phi\left(c v_{\mathrm{x}} - D \frac{\partial c}{\partial x}\right) =0 \qquad \stackrel{v_{\mathrm{x}}=0}{\Longrightarrow} \, \frac{\partial c}{\partial x} =0.
\end{equation}

On the top a pressure Dirichlet value is given as well as a homogenous Dirichlet boundary condition for the concentration except for the inlet, where a Neumann boundary is prescribed $\vec{\mathcal{F}}\cdot\mathbf{n}=f(t)$. At the lower boundary also a pressure Dirichlet value is given and a no flow condition for the transport equation $\vec{\mathcal{F}}\cdot\mathbf{n}=0$.

%\begin{sidewaysfigure}[ht]\centering
\begin{figure}[ht]\centering
\includegraphics[width=0.4\linewidth]{pics/exercise1_bc.pdf}
\caption{Boundary conditions for the One-Phase--Two-Component setup.}
\label{fig_tr_eq}
\end{figure}

\subsection{Properties and parameters - exercise1.input}
\label{prop_paramI}
{\scriptsize
\lstset{numbers=left, breaklines=true, morecomment=[l]{\#}, commentstyle=\color{blue}\tiny, breakindent=28em}
\lstinputlisting{../exercise1.input}
}

The time stepping of the numerical solver is adaptive. {\em MaxTimeStepSize} determines the maximum time step. Adapting this value can be usefull if processes occur very fast and cannot be seen anymore if the time step gets too large.

%%We currently do not consider dispersion in the example!
% \begin{table}[ht!]
% \label{transp_equation_param2}
% \begin{tabular}[t]{lll}
% $D_m=$            & $1.0 \cdot 10^{-9}$                       & [m$^2$/s] \\
% $\alpha_l=$            & $1.0 \cdot 10^{-5}$                       & [m] \\
% $\alpha_t=$            & $1.0 \cdot 10^{-6}$                       & [m] \\
% $t_0$             & injection starts after the first time step                       &  \\
% \end{tabular}
% \end{table}

\paragraph{Remark}
The diffusion coefficient is
% and dispersion coefficients are
fixed values for this exercise and we do not consider dispersion in this exercise ($\alpha _l=\alpha _t=0$).
A very strong diffusion in flow direction can be seen, which is caused by
numerical diffusion due to the used
model and the spatial and temporal resolution (grid resolution and time step sizes).

\subsection{How to...}
\label{how_toI}
\vspace{0.1cm}

{\bfseries ... get the exercise stuff:}\\

Download the handout of the exercise from the ILIAS system.\\
\vspace{0.1cm}

{\bfseries ... open a window:}\\

In the tool bar at the bottom of the screen, click the symbol with a window
and a shell. We will call this window ``window 1''. Only one window at the time is
active. Activate by clicking somewhere in the window.\\
\vspace{0.1cm}

{\bfseries ... change to the right directory:}\\

On the command line in window 1 type\\

{\em cd /temp/efm2019/ex1 }\\
\vspace{0.1cm}

{\bfseries ... set the model parameters:}\\

The parameters used by the program are listed in the file ``exercise1.input''.
To open the file, type (in window 1)\\

{\em kate exercise1.input \&}\\

You can change parameter values used by the simulation in this input file.
For a start, change the parameters
``MaxTimeStepSize'' to ``5.0e2'',
``TEnd'' to ``1.0e4'',
``EpisodeLength'' to ``5.0e2'',
``Name'' to ``lens1p2c'',
``InfiltrationEndTime'' to ``5000'',
and save the file before you leave the editor.\\
\vspace{0.1cm}

{\bfseries ... start the simulation:}\\

On the command line (window 1), type\\

{\em ./lens1p2cexercise1 -{}-parameterFile exercise1.input}\\

The simulation will start with the given time step size DtInitial (e. g. 10) and run until the simulation time tEnd (use e. g. $10\,000$) is reached.
If you want to stop the simulation earlier, activate the window from where you started the simulation and press  ``Ctrl C'' (or ``Strg C'').\\
\vspace{0.1cm}

{\bfseries ... look at the results:}\\

In window 1, type\\

{\em paraview\&}\\

In paraview you can open your result files (lens1p2c.pvd). \\
\vspace{0.1cm}

{\bfseries ...choose the variable to visualize:}\\

You can either look at the water pressure or at the contaminant mass or mole fraction. You can choose the variable currently visualized at a small menue at the upper left side of the paraview window. \\
 \vspace{0.1cm}

{\bfseries ...adapt the color scale:}\\

To show the contaminant  mass or mole fraction or the water pressure more clearly it might be necessary to
adjust the visualisation scale. You may do this by clicking on the button 'rescale to data range'. You find it on the upper left side of the paraview window next to where you choose your variable currently visualized with a double arrow on it.

If you want to rescale to the minimum/maximum of the entire simulated time, you can find the button \emph{``rescale to temporal range''} in the ``display tab'' in the ``object inspector'' by editing the color map.


\subsection{Questions}
Please answer the following questions:
\begin{enumerate}
\item What are the driving forces for the movement of the contaminant?
\item What other physical processes or parameters influence the transport?
\item What parameters do you have to change and how in order to make it more
      difficult for the contaminant to enter $\Omega _2$?
      Use the model to answer the question. In
      section \ref{prop_paramI}, the current values
      of properties and parameters of the model of the one-phase system are
      listed.
\item In general, what remediation techniques would you suggest to remove a
      dissolved contaminant? What advantages / disadvantages do the proposed
      methods have?
\end{enumerate}

Hint: If you want to compare different parameter combinations, it might be good to each time run the simulation with a different name, e.g.  ``lens1p2c-highPerm'' for a case with higher permeability.
You can do this either by changing the parameter ``Name'' in the file ``exercise1.input'' or by starting the simulation with: \\

{\em ./lens1p2cexercise1 -{}-parameterFile exercise1.input -{}-Problem.Name lens1p2c-highPerm}\\
\clearpage
