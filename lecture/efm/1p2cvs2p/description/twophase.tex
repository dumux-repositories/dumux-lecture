% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\section{Two-Phase System}
\label{twophase}

\subsection{Theory}
The following equations are the $(p_w, S_n)$-formulation for incompressible two phase flow in porous media, derived by the mass balance equations and the generalized Darcy's law:

For the wetting phase (water):
\begin{equation}
- \phi \varrho_{w} \frac{\partial S_{n}}{\partial t} - \nabla
\cdot \left( \varrho_{w} \underbrace{\frac{k_{rw}}{\mu_w} \mathbf{K} \cdot
(\nabla p_{w} - \varrho_{w} \mathbf{g})}_{v_w} \right) - q_{w} = 0
\label{DGLw}
\end{equation}

For the nonwetting phase (gas or NAPL):
\begin{equation}
\phi \varrho_{n} \frac{\partial ( S_{n})}{\partial t} - \nabla
\cdot \left( \varrho_{n} \underbrace{\frac{k_{rn}}{\mu_n} \mathbf{K} \cdot
(\nabla p_{w} + \nabla p_c - \varrho_{n} \mathbf{g})}_{v_n} \right) -
q_{n} = 0 \; .
\label{DGLn}
\end{equation}

The constraints, which are already included in the equations above, are:
\begin{equation}
p_{n} = p_{w} + p_c ,\qquad  S_{w} = (1 - S_{n})
\end{equation}

\begin{table}[ht]
\label{transp_equation_paramII}
\begin{tabular}[t]{lll}
$p_w$            & pressure of wetting phase                                & $[Pa]$ \\
$p_n$            & pressure of non--wetting phase                        & $[Pa]$ \\
$S_w$            & saturation of wetting phase                              & $[-]$ \\
$S_n$            & saturation of non--wetting phase                      & $[-]$ \\
$S_{wr}$            & residual saturation of wetting phase               & $[-]$ \\
$S_{nr}$            & residual saturation of non--wetting phase       & $[-]$ \\
$\phi$         & Effective porosity                                                  & $[-]$ \\
$t$            & Time                                                                         & $[s]$ \\
$\rho_w$ & density of wetting phase                                          & $[kg/m^3]$ \\
$\rho_n$ & density of non--wetting phase                                  & $[kg/m^3]$ \\
$\mu_w$ & dynamic viscosity of wetting phase                          &$[Pa s]$ \\
$\mu_n$  & dynamic viscosity of non--wetting phase                 &$[Pa s]$ \\
$K$          & intrinsic permeability                                                 & $[m^2]$ \\
$k_{rw}$          & relative permeability for wetting phase             & $[-]$ \\
$k_{rn}$          & relative permeability for non--wetting phase     & $[-]$ \\
$p_c$             & capillary pressure                                                & $Pa$ \\
$p_d$             & entry pressure, BC parameter                            & $[Pa]$ \\
$\lambda$       & BC parameter                                                      & $[-]$ \\
$q_w$            & Mass source/sink rate   for wetting phase          & $[kg/(m^3s)]$ \\
$q_w$            & Mass source/sink rate  for non--wetting phase  & $[kg/(m^3s)]$ \\
\end{tabular}
\end{table}

\begin{figure}[h]
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{pics/BCcappress.pdf}
%\vspace{9pt}
%\centerline{
%\epsfig{figure=EPS/BCcappress.eps,width=7cm,angle=0}
%}
\caption{Capillary Pressure Saturation Function after Brooks and Corey}
\label{fig.BCcappress}
\end{minipage}
\begin{minipage}[]{0.1\linewidth}
\end{minipage}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{pics/BCkr.pdf}
%    \centerline{\epsfig{file=EPS/BCkr.eps, width=7cm}}
    \caption{Relative Permeability Function after Brooks and Corey}
    \protect\label{fig.BCkrOmega1_2}
\end{minipage}
\end{figure}

\begin{figure}\centering
\includegraphics[width=0.6\linewidth]{pics/ci_entry.pdf}
%\vspace{9pt}
%\centerline{
%\epsfig{figure=./XFIG/ci_entry.eps,width=10cm,angle=-0}
%}
\caption{DNAPL spreading at the interface of two domains}
\label{ci_entry}
\end{figure}

\clearpage
\subsection{Properties and parameters - exercise1.input}
\label{prop_paramII}
{\scriptsize
\lstset{numbers=left, breaklines=true, morecomment=[l]{\#}, commentstyle=\color{blue}\tiny, breakindent=28em}
\lstinputlisting{../exercise1.input}
}

\subsection{How to...}
\label{how_toII}
\vspace{0.1cm}

{\bfseries ... get the exercise stuff:}\\

Download the handout of the exercise.\\
\vspace{0.1cm}

{\bfseries ... open a window:}\\

In the tool bar at the bottom of the screen, click the symbol with a window
and a shell. We will call this window ``window 1''. Only one window at the time is
active. Activate by clicking somewhere in the window.\\
\vspace{0.1cm}

{\bfseries ... change to the right directory:}\\

On the command line in window 1 type\\

{\em cd /temp/efm2019/ex1 }\\
\vspace{0.1cm}

{\bfseries ... set the model parameters:}\\

The parameters used by the program are listed in the file ``exercise1.input''.
To open the file, type (in window 1)\\

{\em kate exercise1.input \&}\\

You can change parameter values used by the simulation in this input file.
Change the parameters
``MaxTimeStepSize'' to ``5.0e2'',
``TEnd'' to ``1.0e4'',
``EpisodeLength'' to ``5.0e2'',
``Name'' to ``lens2p'',
``InfiltrationEndTime'' to ``5000'',
and save the file before you leave the editor.\\
If you do not change the parameter ``Name'', your previous simulation results of the 1p2c part of the excercise will be overwritten!
\vspace{0.1cm}

{\bfseries ... start the simulation:}\\

On the command line (window 1), type\\

{\em ./lens2pexercise1 -{}-parameterFile=exercise1.input}\\

The simulation will start with the given time step size DtInitial (e. g. 10) and run until the simulation time tEnd (use e. g. $10\,000$) is reached.
If you want to stop the simulation earlier, activate the window from where you started the simulation and press  ``Ctrl C'' (or ``Strg C'').\\
\vspace{0.1cm}

{\bfseries ... look at the results:}\\

In window 1, type\\

{\em paraview \&}\\

In paraview you can open your result files (lens2p.pvd). \\
\vspace{0.1cm}

{\bfseries ...choose the variable to visualize:}\\

You can either look at the phase pressures or at the saturations. You can choose the variable currently visualized at a small menue at the upper left side of the paraview window. \\
 \vspace{0.1cm}

{\bfseries ...adapt the color scale:}\\

To show the saturation or the pressure more clearly it might be necessary to
adjust the visualisation scale. You may do this by clicking on the button 'rescale to data range'. You find it on the upper left side of the paraview window next to where you choose your variable currently visualized with a double arrow on it.

\subsection{Questions}
Please answer the following questions:

\begin{enumerate}
\item Which are the driving forces for the contaminant?  Which terms
      in the equations do account for them?
\item Which conditions will prevent the contaminant from entering into the
      fine sand lense? (Give parameter estimations!)  Which are the
      reasons for this?
\item What is the final state of the system if $S_{nr} = 0$ for
      the whole system, what if $S_{nr} > 0$? Which are the reasons?
\item What will happen if you give conditions which prevent the
      entering of the contaminant into the fine sand lense,
      and you give $S_{nr} > 0$ only for the fine sand lense,
      while $S_{nr}=0$ for the coarse sand?
\item Which are appropriate remediation techniques to remove
      residual saturation?
\item What is the final state if you have a coarse sand lense
      within fine sand (with $p_d = 6000 [Pa]$) and give $S_{nr} = 0$
      for the whole system? Will you have a long term contamination
      for the considered domain or not?
\item By which terms does the multiphase system differ from
      a one phase system with tracer transport (1 phase 2
      components system) in principle? What do those terms
      account for?
\end{enumerate}

Hint: If you want to compare different parameter combinations, it might be good to each time run the simulation with a different name, e.g.  ``lens2p-highPEntry'' for a case with higher entry pressure in the fine lense.
You can do this either by changing the parameter ``Name'' in the file ``exercise1.input'' or by starting the simulation with: \\

{\em ./lens2pexercise1 -{}-parameterFile exercise1.input -{}-Problem.Name lens2p-highPEntry}\\

Additionally, in case you finished the exercise and want to have a look at the code at:\\
{\em cd /temp/efm2019/DUMUX/dumux-lecture/lecture/efm/1p2cvs2p }\\
\clearpage
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "handouts-1"
%%% End:
