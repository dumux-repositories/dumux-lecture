# SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

add_input_file_links()

dumux_add_test(NAME lens2pexercise2
              TIMEOUT 1800
              SOURCES lens2pexercise2.cc
              COMMAND ${DUMUX_RUNTEST}
              CMD_ARGS --script fuzzy
                       --files ${CMAKE_SOURCE_DIR}/lecture/references/lens-2p-exercise2-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/lens-2p-00051.vtu
                       --command "${CMAKE_CURRENT_BINARY_DIR}/lens2pexercise2")
