//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief spatial parameters for the GRUWA-equivalent exercise
 */
#ifndef DUMUX_LECTURE_MHS_GROUNDWATER_SPATIALPARAMS_HH
#define DUMUX_LECTURE_MHS_GROUNDWATER_SPATIALPARAMS_HH

#include <dumux/common/parameters.hh>

#include <dumux/material/spatialparams/sequentialfv1p.hh>

namespace Dumux {

/*!
 * \brief spatial parameters for the test problem for diffusion models.
 */
template<class TypeTag>
class GroundwaterSpatialParams: public SequentialFVSpatialParamsOneP<TypeTag>
{
    typedef typename GetPropType<TypeTag, Properties::GridGeometry>::GridView GridView;
    typedef GetPropType<TypeTag, Properties::Problem> Problem;
    typedef GetPropType<TypeTag, Properties::Scalar> Scalar;

    enum
    {
        // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> Tensor;

    struct Lens
    {
        Lens(const GlobalPosition& upRight, const GlobalPosition& lowLeft, const Scalar& perm)
        : upperRight(upRight), lowerLeft(lowLeft), permeability(perm) {}

        const GlobalPosition upperRight;
        const GlobalPosition lowerLeft;
        const Scalar permeability;
    };

    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    static const unsigned int numEq = 1;

public:

    GroundwaterSpatialParams(const Problem& problem) : SequentialFVSpatialParamsOneP<TypeTag>(problem)
    {
        // factor converting K_f to instrinsic permeability
        const Scalar permFactor = 0.001/(1000*9.81);

        // the background permeability
        auto kii = getParam<Scalar>("SpatialParams.HydraulicConductivity")*permFactor;
        permeability_ = {{kii, 0}, {0, kii}};

        // Read the lenses
        auto lenses = getParam<std::vector<Scalar>>("SpatialParams.Lenses");
        unsigned int numLenses = std::trunc(lenses.size()/5); // make it robust by truncating

        for (unsigned int lensIdx = 0; lensIdx < numLenses; ++lensIdx)
        {
            const unsigned int i = 5*lensIdx;
            lenses_.push_back(Lens({lenses[i+1], lenses[i+3]},
                                   {lenses[i], lenses[i+2]},
                                   lenses[i+4]*permFactor));
        }
    }

    const Tensor& intrinsicPermeabilityAtPos (const GlobalPosition& globalPos) const
    {
        if(!lenses_.empty())
        {
            // check if we are in one of the lenses
            for (const auto& lens : lenses_)
                if (isInLens_(lens, globalPos))
                {
                    // the permeabilty is the lens permeability
                    lensPermeability_ = {{lens.permeability, 0}, {0, lens.permeability}};
                    return lensPermeability_;
                }
        }

        // if we are not inside any lens return background permeability
        return permeability_;
    }

    Scalar porosity(const Element& element) const
    { return porosity_; }

private:
    bool isInLens_(const Lens& lens, const GlobalPosition& globalPos) const
    {
        for (unsigned int dimIdx = 0; dimIdx < dimWorld; ++dimIdx)
        {
            if (globalPos[dimIdx] < lens.lowerLeft[dimIdx] + eps_ || globalPos[dimIdx] > lens.upperRight[dimIdx] - eps_)
                return false;
        }
        return true;
    }

    const Scalar eps_ = 1e-6;
    const Scalar porosity_ = 0.2;
    Tensor permeability_;
    mutable Tensor lensPermeability_;
    std::vector<Lens> lenses_;
};

} // end namespace Dumux
#endif
