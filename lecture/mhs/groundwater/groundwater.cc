//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Groundwater simulation program.
 *
 * Successor of the former GRUWA. It is a two dimensional, 1p-stationary,
 * finite volume program. The user interface is modifing the input file.
 */
#include <config.h>

#include <dumux/common/properties.hh>
#include <dumux/common/start.hh>

#include "groundwaterproperties.hh"

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0)
    {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe List of Mandatory arguments for this program is:\n"
                                        "\t-TimeManager.TEnd          The end of the simulation. [s] \n"
                                        "\t-TimeManager.DtInitial     The initial timestep size. [s] \n"
                                        "\t-Grid.File                 The file name of the file containing the grid \n"
                                        "\t                           definition in DGF format\n";

        std::cout << errorMessageOut << std::endl;
    }
}

////////////////////////
// the main function
////////////////////////
int main(int argc, char** argv)
{
    using TypeTag = Dumux::Properties::TTag::GroundwaterProblemTypeTag;
    return Dumux::start<TypeTag>(argc, argv, usage);
}
