//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Dumux-equivalent for GRUWA (1p-stationary, finite volumes)
 */
#ifndef DUMUX_LECTURE_MHS_GROUNDWATER_PROBLEM_HH
#define DUMUX_LECTURE_MHS_GROUNDWATER_PROBLEM_HH

#include <dumux/porousmediumflow/1p/sequential/diffusion/problem.hh>
#include <dumux/porousmediumflow/sequential/cellcentered/velocity.hh>


namespace Dumux {

/*!
 * \brief Dumux-equivalent for GRUWA (1p-stationary, finite volumes)
 */
template<class TypeTag>
class GroundwaterProblem : public DiffusionProblem1P<TypeTag>
{
    typedef DiffusionProblem1P<TypeTag> ParentType;
    typedef GetPropType<TypeTag, Properties::TimeManager> TimeManager;
    typedef typename GetPropType<TypeTag, Properties::GridGeometry>::GridView GridView;
    typedef typename GridView::Grid Grid;
    typedef GetPropType<TypeTag, Properties::FluidSystem> FluidSystem;
    typedef GetPropType<TypeTag, Properties::PrimaryVariables> PrimaryVariables;
    typedef GetPropType<TypeTag, Properties::SequentialBoundaryTypes> BoundaryTypes;
    typedef GetPropType<TypeTag, Properties::CellData> CellData;

    enum
    {
        // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef GetPropType<TypeTag, Properties::Scalar> Scalar;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename Dune::FieldVector<int, dim> ResolutionVector;

    static constexpr double eps_ = 1e-6;

    struct BoundarySegment
    {
        Scalar from, to;
        bool neumann;
        Scalar value;
        std::size_t axis;

        bool inside(const GlobalPosition& globalPos)
        { return globalPos[axis] > from - eps_ && globalPos[axis] < to + eps_; }
    };

    struct Source
    {
        Dune::FieldVector<double,2> globalPos;
        double q;
        int index;
    };

public:
    GroundwaterProblem(TimeManager& timeManager, Grid& grid)
    : ParentType(timeManager, grid), velocity_(*this)
    {
        FluidSystem::Component::setViscosity(0.001);
        FluidSystem::Component::setDensity(1000);

        // Read input parameters and write them into private variables
        domainSize_ = getParam<GlobalPosition>("Grid.UpperRight");
        geometryDepth_ = getParam<Scalar>("Grid.Depth");
        numCells_ = getParam<ResolutionVector>("Grid.Cells");

        // check input for too large grid size
        if (numCells_[0]*numCells_[1] > 10000)
        {
            std::cout << "Number of cells exceeds limit. Choose less than 10000 cells!\n";
            exit(2);
        }
        // check input for too small grid size
        if (numCells_[0] < 2 || numCells_[1] < 2)
        {
            std::cout << "Number of cells too low. Choose at least 2 cell in each direction!\n";
            exit(2);
        }

        // Read sources
        auto sources = getParam<std::vector<Scalar>>("Source.Sources");
        unsigned int numSources = std::trunc(sources.size()/3);

        for (int sourceCount = 0; sourceCount < numSources; sourceCount++)
        {
            Source tempSource;
            tempSource.globalPos[0]=sources[sourceCount*3];
            tempSource.globalPos[1]=sources[sourceCount*3+1];
            tempSource.q=sources[sourceCount*3+2];
            tempSource.index = std::floor(tempSource.globalPos[0]
               * numCells_[0]/domainSize_[0])
               + std::floor(tempSource.globalPos[1]*numCells_[1]/domainSize_[1])
               * numCells_[0];
            sources_.push_back(tempSource);
        }

        // Read boundary conditions
        auto bc = getParam<std::vector<double>>("BoundaryConditions.Left");
        int NumberOfSegments = std::trunc(bc.size()/4);
        for (int segmentCount = 0; segmentCount < NumberOfSegments; segmentCount++)
        {
            BoundarySegment tempSegment;
            tempSegment.from = bc[segmentCount*4];
            tempSegment.to = bc[segmentCount*4+1];
            tempSegment.neumann = (bc[segmentCount*4+2]!=0);
            tempSegment.value = bc[segmentCount*4+3];
            boundaryConditions_[2].push_back(tempSegment);
        }
        bc = getParam<std::vector<double>>("BoundaryConditions.Right");
        NumberOfSegments = std::trunc(bc.size()/4);
        for (int segmentCount = 0; segmentCount < NumberOfSegments; segmentCount++)
        {
            BoundarySegment tempSegment;
            tempSegment.from = bc[segmentCount*4];
            tempSegment.to = bc[segmentCount*4+1];
            tempSegment.neumann = (bc[segmentCount*4+2]!=0);
            tempSegment.value = bc[segmentCount*4+3];
            boundaryConditions_[3].push_back(tempSegment);
        }
        bc = getParam<std::vector<double>>("BoundaryConditions.Bottom");
        NumberOfSegments = std::trunc(bc.size()/4);
        for (int segmentCount = 0; segmentCount < NumberOfSegments; segmentCount++)
        {
            BoundarySegment tempSegment;
            tempSegment.from = bc[segmentCount*4];
            tempSegment.to = bc[segmentCount*4+1];
            tempSegment.neumann = (bc[segmentCount*4+2]!=0);
            tempSegment.value = bc[segmentCount*4+3];
            boundaryConditions_[1].push_back(tempSegment);
        }
        bc = getParam<std::vector<double>>("BoundaryConditions.Top");
        NumberOfSegments = std::trunc(bc.size()/4);
        for (int segmentCount = 0; segmentCount < NumberOfSegments; segmentCount++)
        {
            BoundarySegment tempSegment;
            tempSegment.from = bc[segmentCount*4];
            tempSegment.to = bc[segmentCount*4+1];
            tempSegment.neumann = (bc[segmentCount*4+2]!=0);
            tempSegment.value = bc[segmentCount*4+3];
            boundaryConditions_[0].push_back(tempSegment);
        }
    }

    /*!
    * \name Problem parameters
    */
    // \{

    /*!
    * \brief The problem name.
    *
    * This is used as a prefix for files generated by the simulation.
    */
    std::string name() const
    { return "groundwater"; }

    bool shouldWriteRestartFile() const
    { return false; }

    /*!
    * \brief Returns the temperature within the domain.
    *
    * This problem assumes a temperature of 10 degrees Celsius.
    */
    Scalar temperature(const Element& element) const
    {
        return 273.15 + 10; // 10°C
    }

    //! Returns the reference pressure for evaluation of constitutive relations
    Scalar referencePressure(const Element& element) const
    {
        return 1.0e5; // 1 bar
    }

    //!source term [kg/(m^3 s)] (or in 2D [kg/(m^2 s)]).
    void source(PrimaryVariables& values, const Element& element) const
    {
        values = 0;
        Scalar density=FluidSystem::density(0.0,0.0);
        for (unsigned int sourceCount = 0; sourceCount != sources_.size(); sourceCount++)
        {
            if (this->variables().index(element) == sources_[sourceCount].index)
                values += sources_[sourceCount].q*density/element.geometry().volume()/geometryDepth_;
        }
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
    * \brief Returns the type of boundary condition.
    *
    * BC can be dirichlet (pressure) or neumann (flux).
    */
    void boundaryTypesAtPos(BoundaryTypes &bcType,
            const GlobalPosition& globalPos) const
    {
        double coordinate = 0.0;
        int boundaryIndex = 0;
        // top boundary
        if (globalPos[1] > domainSize_[1] - eps_)
        {
            coordinate = globalPos[0];
            boundaryIndex = 0;
        }
        // bottom boundary
        if (globalPos[1] < eps_)
        {
            coordinate = globalPos[0];
            boundaryIndex = 1;
        }
        // left boundary
        if (globalPos[0] < eps_)
        {
            coordinate = globalPos[1];
            boundaryIndex = 2;
        }
        // right boundary
        if (globalPos[0] > domainSize_[0] - eps_)
        {
            coordinate = globalPos[1];
            boundaryIndex = 3;
        }

        for (unsigned int segmentCount = 0;
             segmentCount < boundaryConditions_[boundaryIndex].size();
             segmentCount++)
        {
            if ((boundaryConditions_[boundaryIndex][segmentCount].from < coordinate) &&
                    (coordinate < boundaryConditions_[boundaryIndex][segmentCount].to))
            {
                if (boundaryConditions_[boundaryIndex][segmentCount].neumann)
                    bcType.setAllNeumann();
                else
                    bcType.setAllDirichlet();
                return;
            }
        }
        bcType.setAllNeumann();
    }

    //! return Dirichlet conditions (pressure, [Pa])
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        double coordinate = 0;
        int boundaryIndex = 0;
        // top boundary
        if (globalPos[1] > domainSize_[1] - eps_)
        {
            coordinate = globalPos[0];
            boundaryIndex = 0;
        }
        // bottom boundary
        if (globalPos[1] < eps_)
        {
            coordinate = globalPos[0];
            boundaryIndex = 1;
        }
        // left boundary
        if (globalPos[0] < eps_)
        {
            coordinate = globalPos[1];
            boundaryIndex = 2;
        }
        // right boundary
        if (globalPos[0] > domainSize_[0] - eps_)
        {
            coordinate = globalPos[1];
            boundaryIndex = 3;
        }

        for (unsigned int segmentCount = 0;
             segmentCount < boundaryConditions_[boundaryIndex].size();
             segmentCount++)
        {
            if ((boundaryConditions_[boundaryIndex][segmentCount].from < coordinate) &&
                    (coordinate < boundaryConditions_[boundaryIndex][segmentCount].to))
            {
                values = boundaryConditions_[boundaryIndex][segmentCount].value*FluidSystem::density(0.0,0.0)*9.81;
                return;
            }
        }
        values = 0;
    }

    //! return Neumann conditions (flux, [kg/(m^2 s)])
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        double coordinate = 0;
        int boundaryIndex = 0;
        // top boundary
        if (globalPos[1] > domainSize_[1] - eps_)
        {
            coordinate = globalPos[0];
            boundaryIndex = 0;
        }
        // bottom boundary
        if (globalPos[1] < eps_)
        {
            coordinate=globalPos[0];
            boundaryIndex = 1;
        }
        // left boundary
        if (globalPos[0] < eps_)
        {
            coordinate=globalPos[1];
            boundaryIndex = 2;
        }
        // right boundary
        if (globalPos[0] > domainSize_[0] - eps_)
        {
            coordinate = globalPos[1];
            boundaryIndex = 3;
        }

        for (unsigned int segmentCount = 0;
             segmentCount < boundaryConditions_[boundaryIndex].size();
             segmentCount++)
        {
            if ((boundaryConditions_[boundaryIndex][segmentCount].from < coordinate) &&
                    (coordinate < boundaryConditions_[boundaryIndex][segmentCount].to))
            {
                values = boundaryConditions_[boundaryIndex][segmentCount].value*FluidSystem::density(0.0,0.0)*(-1);
                return;
            }
        }
        values = 0;
    }

    // \}

    bool shouldWriteOutput ()
    {
        return (this->timeManager().time() >= 0);
    }

    void writeOutput()
    {
        ParentType::writeOutput();
        Scalar zmax, zmin;
        zmax=this->variables().cellData(0).pressure()/(FluidSystem::density(0.0,0.0)*9.81);
        zmin=this->variables().cellData(0).pressure()/(FluidSystem::density(0.0,0.0)*9.81);

        for (int i = 0; i < numCells_[0]*numCells_[1]; i++)
        {
            Scalar currentHead= this->variables().cellData(i).pressure()/(FluidSystem::density(0.0,0.0)*9.81);
            zmax = std::max(currentHead,zmax);
            zmin = std::min(currentHead,zmin);
        }

        std::ofstream dataFile;
        dataFile.open("dumux-out.vgfc");
        dataFile << "Gridplot" << std::endl;
        dataFile << "## This is a DuMuX output for the ViPLab Graphics driver. \n";
        dataFile << "## This output file was generated at " << __TIME__ <<", "<< __DATE__<< "\n";
        dataFile << "# range boxed\n" ;
        dataFile << "# x-range 0 "<< domainSize_[0] << "\n" ;
        dataFile << "# y-range 0 "<< domainSize_[1] << "\n" ;
        dataFile << "# x-count " << numCells_[0] << "\n" ;
        dataFile << "# y-count " << numCells_[1] << "\n" ;

        dataFile << "# scale 1 1 1\n";

        dataFile << "# min-color 255 0 0\n";
        dataFile << "# max-color 0 0 255\n";
        dataFile << "# time 0 \n" ;
        dataFile << "# label piezometric head \n";

        for (int i = 0; i< numCells_[1]; i++)
        {
            for (int j = 0; j < numCells_[0]; j++)
            {
                int currentIdx = i*numCells_[0]+j;
                dataFile << this->variables().cellData(currentIdx).pressure()/(FluidSystem::density(0.0,0.0)*9.81);
                if(j != numCells_[0]-1) // all but last entry
                    dataFile << " ";
                else // write the last entry
                    dataFile << "\n";
            }
        }
        dataFile.close();

        velocity_.calculateVelocity();

        //Textoutput:
        std::cout << "         x          y          h           v_x           v_y"<<std::endl;
        std::cout << "------------------------------------------------------------"<<std::endl;

        std::ofstream contourFile;
        contourFile.open("contourfile.txt");
        contourFile << "x,y,h,v_x,v_y" << std::endl;

        for (const auto& element : elements(this->gridView()))
        {
            int cellIndex = this->variables().index(element);
            const CellData& cellData = this->variables().cellData(cellIndex);

            Scalar v_x, v_y, piezo, x, y;
            v_x = (cellData.fluxData().velocity(0)[0] + cellData.fluxData().velocity(1)[0])/2;
            v_y = (cellData.fluxData().velocity(2)[1] + cellData.fluxData().velocity(3)[1])/2;

            if (std::abs(v_x) < 1e-17) v_x = 0;
            if (std::abs(v_y) < 1e-17) v_y = 0;

            piezo = cellData.pressure()/(FluidSystem::density(0.0,0.0)*9.81);
            x = element.geometry().center()[0];
            y = element.geometry().center()[1];

            printf("%10.4g %10.4g %10.4g %13.4g %13.4g\n", x, y, piezo, v_x, v_y);

            contourFile << std::setprecision(15) << x << "," << y << "," << piezo << "," << v_x << "," << v_y << std::endl;
        }
    }

private:
    std::vector<Source> sources_;
    std::vector<BoundarySegment> boundaryConditions_[4];
    GlobalPosition domainSize_;
    ResolutionVector numCells_;

    Scalar geometryDepth_;
    Dumux::FVVelocity<TypeTag, GetPropType<TypeTag, Properties::Velocity>> velocity_;
};

} // end namespace Dumux

#endif
