//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Dumux-equivalent for GRUWA (1p-stationary, finite volumes)
 */
#ifndef DUMUX_LECTURE_MHS_GROUNDWATER_PROPERTIES_HH
#define DUMUX_LECTURE_MHS_GROUNDWATER_PROPERTIES_HH

#include <cmath>

#include <dune/grid/yaspgrid.hh>

#include <dumux/porousmediumflow/1p/sequential/diffusion/cellcentered/pressureproperties.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <lecture/mm/buckleyleverett/pseudoh2o.hh>
#include "groundwaterspatialparams.hh"
#include "groundwaterproblem.hh"


namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct GroundwaterProblemTypeTag { using InheritsFrom = std::tuple<FVPressureOneP>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::GroundwaterProblemTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::GroundwaterProblemTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, PseudoH2O<Scalar> >;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::GroundwaterProblemTypeTag> { using type = Dumux::GroundwaterSpatialParams<TypeTag>; };

//Set the problem
template<class TypeTag>
struct Problem<TypeTag, TTag::GroundwaterProblemTypeTag> { using type = Dumux::GroundwaterProblem<TypeTag>; };

} // end namespace Dumux::Properties

#endif
