// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/**
 * \file
 * \brief  Writes output to use with ViPLab.
 */

#ifndef DUMUX_VIPLAB_OUTPUT_HH
#define DUMUX_VIPLAB_OUTPUT_HH

#include <dumux/porousmediumflow/sequential/properties.hh>

namespace Dumux {
/**
 * \brief Writes output to use with ViPLab.
 *
 * DOC ME!
 */

template<class TypeTag>
class ViplabOutput
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = GetPropType<TypeTag, Properties::Indices>;
    enum{ wPhaseIdx = Indices::wPhaseIdx };

public:
    ViplabOutput(const Problem &problem, int plotDim = 1)
    : problem_(problem), plotDim_(plotDim)
    {
        outputName_="vipplot.vgf";
        if (plotDim == 2)
            outputName_="dumux-out.vgfc";

        dataFile_.open(outputName_);
        dataFile_.close();
    }

    //TODO make writeViplabOutput1d and 2d consistent.

    //this is a wrapper function
    void writeVipLabOutput(const std::vector<double> &solution, const std::vector<int> &color)
    {
        if (plotDim_ == 1)
            this->writeViplabOutput1d(solution, color);
        // else if (plotDim_==2) writeViplabOutput2d(solution);//TODO fix this
        else
            std::cout << "plots with dimension "<< plotDim_ <<" not supported" << std::endl;
    }

    //this is a wrapper function
    void writeVipLabOutput(const std::vector<double> &solution)
    {
        if (plotDim_ == 1)
        {
            //if no color is given set the defaul color to black
            std::vector<int> color({0, 0, 0});
            writeViplabOutput1d(solution, color);
        }
        // else if (plotDim_==2) writeViplabOutput2d(solution);//TODO fix this
        else
            std::cout << "plots with dimension "<< plotDim_ <<" not supported" << std::endl;
    }

    /**
     * \brief Plot a one-dimensional solution to a file using the ViPLab
     *        plot format.
     * \param solution One-dimensional solution to be plotted.
     * \param color    Color of the graph.
     *
     * \tparam Solution Type of the solution vector.
     * \tparam Color    Type of the color.
     */
    template<class Solution, class Color>
    void writeViplabOutput1d(const Solution &solution, const Color &color)
    {
        setColor(color);

        const auto cellNumber = getParam<std::vector<int>>("Grid.Cells");
        assert(cellNumber[0] == solution.size());
        upperRight_ = getParam<std::vector<double>>("Grid.UpperRight");
        const Scalar discretizationLength = upperRight_[0] / cellNumber[0];

        dataFile_.open(outputName_, std::fstream::app);
        dataFile_ << 0 << " " << solution[0] << " ";
        dataFile_ << discretizationLength << " " << solution[0] << std::endl;

        // horizontal lines
        for (unsigned int i = 1; i < solution.size(); i++)
            drawLine(i*discretizationLength, (i+1)*discretizationLength, solution[i], solution[i], dataFile_);

        dataFile_.close();
    }

    void writeViplabOutput2d(const std::vector<double> &solution)
    {
    // TODO: check if this is used anymore:
//     double ymin= *min_element(solution.begin(),solution.end());
//    Scalar zmax, zmin;
//    zmax=problem_.variables().cellData(0).pressure()/(Fluid::density(0,0)*9.81);
//    zmin=problem_.variables().cellData(0).pressure()/(Fluid::density(0,0)*9.81);
//
//    for (int i=0; i< resolution[0]*resolution[1]; i++)
//    {
//        Scalar currentHead= problem_.variables().cellData(i).pressure()/(Fluid::density(0,0)*9.81);
//        zmax = std::max(currentHead,zmax);
//        zmin = std::min(currentHead,zmin);
//    }
//
//        std::ofstream dataFile;
//        dataFile.open("dumux-out.vgfc");
//        dataFile << "Gridplot" << std::endl;
//
//        dataFile << "# range boxed\n" ;
//        dataFile << "# x-range 0 "<< domainSize_[0] << "\n" ;
//        dataFile << "# y-range 0 "<< domainSize_[1] << "\n" ;
//        dataFile << "# x-count " << resolution_[0] << "\n" ;
//        dataFile << "# y-count " << resolution_[1] << "\n" ;
//        if ((zmax-zmin)/zmax>0.01)
//            dataFile << "# scale 1 1 "<< sqrt(domainSize_[0]*domainSize_[1])/(zmax-zmin) << "\n";
//        else
//            dataFile << "# scale 1 1 1\n";
//
//        dataFile << "# min-color 255 0 0\n";
//        dataFile << "# max-color 0 0 255\n";
//        dataFile << "# time 0 \n" ;
//        dataFile << "# label piezometric head \n";
//
//        for (int i=0; i< resolution_[1]; i++)
//        {
//            for (int j=0; j<resolution_[0]; j++)
//            {
//                int currentIdx = i*resolution_[0]+j;
//                dataFile << problem_.variables().cellData(currentIdx).pressure()/(Fluid::density(0,0)*9.81);
//                if(j != resolution_[0]-1) // all but last entry
//                    dataFile << " ";
//                else // write the last entry
//                    dataFile << "\n";
//            }
//        }
//        dataFile.close();
    }


    void writeHeader()
    {
        if (plotDim_ == 1)
            writeHeader1d();
        else if (plotDim_ == 2)
            writeHeader2d();
        else
            std::cout << "plots with dimension "<< plotDim_ <<" not supported" << std::endl;

    }

    void writeHeader1d()
    {
        Scalar time = problem_.timeManager().time();
        if (time < 0) return;
        //Write header for ViPLab-Outputfile
        dataFile_.open(outputName_, std::fstream::app);
        if (time < 1.0e-8 ){
            dataFile_ << "## This is a DuMuX output for the ViPLab Graphics driver. \n";
            dataFile_ << "## This output file was generated at " << __TIME__ <<", "<< __DATE__<< "\n";
        }
        if (time  > 0) dataFile_ << "# newframe\n";

        //TODO The difficulty here is that we have to find the minimum and the maximum for the x- and y-values for every curve which is printed
        //(and over time, this behaviour might the user want to change!)
        //i.e. the header must be written after the last call of writeViplabOutput for every timestep (or at the end of the simulation!).
//                Scalar currentHead= problem_.variables().cellData(i).pressure()/(Fluid::density(0,0)*9.81);
//                zmax = std::max(currentHead,zmax);
//                zmin = std::min(currentHead,zmin);

        dataFile_ << "# title "<< problem_.name() <<" Problem\n";
        dataFile_ << "# x-label x\n";                //TODO replace the labels by actual parameters
        dataFile_ << "# y-label Saturation\n";
        dataFile_ << "# x-range 0.0 " << upperRight_[0] << "\n"; //TODO replace the ranges by actual ranges
        dataFile_ << "# y-range 0 1\n";
        dataFile_.close();
    }

    void writeHeader2d()
    {
//        Scalar time = problem_.timeManager().time();
//        if (time < 0) return;
//        //Write header for ViPLab-Outputfile
//        dataFile_.open(outputName_, std::fstream::app);
//        if (time < 1.0e-8 ){
//            dataFile_ << "## This is a DuMuX output for the ViPLab Graphics driver. \n";
//            dataFile_ << "## This output file was generated at " << __TIME__ <<", "<< __DATE__<< "\n";
//        }
//        if (time  > 0) dataFile_ << "# newframe\n";
//
//        //TODO The difficulty here is that we have to find the minimum and the maximum for the x- and y-values for every curve which is printed
//        //(and over time, this behaviour might the user want to change!)
//        //i.e. the header must be written after the last call of writeViplabOutput for every timestep (or at the end of the simulation!).
////                Scalar currentHead= problem_.variables().cellData(i).pressure()/(Fluid::density(0,0)*9.81);
////                zmax = std::max(currentHead,zmax);
////                zmin = std::min(currentHead,zmin);
//
//        dataFile_ << "# title "<< problem_.name() <<" Problem\n";
//        dataFile_ << "# x-label x\n";                //TODO replace the labels by actual parameters
//        dataFile_ << "# y-label Saturation\n";
//        dataFile_ << "# x-range 0.0 " << getPropValue<TypeTag, Properties::UpperRightX>() << "\n"; //TODO replace the ranges by actual ranges
//        dataFile_ << "# y-range 0 1\n";
//        dataFile_.close();
    }

    void writeLegend()
    {
        Scalar time = problem_.timeManager().time();
        if (time < 0) return;
        dataFile_.open(outputName_, std::fstream::app);
        dataFile_ << "# legend Numerical Solution,Analytic Solution\n"; //TODO adapt to actual legend
        dataFile_.close();
    }

private:

    template<typename Scalar, typename ViPLabOutputFile>
    void drawLine(const Scalar &x1, const Scalar &x2, const Scalar &y1, const Scalar &y2,
                  ViPLabOutputFile &viplaboutputfile) const
    {
        viplaboutputfile << x1 << " " << y1 << " " << x2 << " " << y2 << std::endl;
    }

    template<typename Color>
    void setColor(const Color &color)
    {
        dataFile_.open(outputName_, std::fstream::app);
        dataFile_ << "# color "<< color[0] << " " << color[1] << " " << color[2] << std::endl;
        dataFile_.close();
    }

    const Problem &problem_;
    std::ofstream dataFile_;
    int plotDim_;
    std::string outputName_;
    std::vector<double> upperRight_;
};

}//end namespace Dumux

#endif
