// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/**
 * \file
 *
 * \brief Provides the class with the tabulated values of CO2 for the
 *        benchmark3 problem
 */
#ifndef DUMUX_HETEROGENEOUS_CO2TABLES_HH
#define DUMUX_HETEROGENEOUS_CO2TABLES_HH

#include <dumux/material/components/defaultco2table.hh>

#endif
