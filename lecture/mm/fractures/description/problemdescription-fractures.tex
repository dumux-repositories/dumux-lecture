% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\section*{Flow through fractured porous media}

\subsection*{Discrete fracture model}

Fractures can substantially change the hydraulic behavior in porous media, while we have to keep in mind that almost all rocks are fractured on a certain scale of consideration, and it is in most cases a matter of the choice of the REV size if fractures need to be recognized as distinct features or can be included in the averaged properties of the REV.

Modeling approaches to fractured porous media can be classified into continuum fracture models and discrete fracture-matrix models. For example, a dual-continuum does not resolve fractures geometrically but considers a second overlapping continuum and employs transfer functions as an approach to describe the interaction between the continua.
In discrete fracture-matrix models, the fracture geometries are discretely captured either by (i) an \textit{equi-dimensional} discretization of the fractures or (ii) by \textit{lower-dimensional} geometries with corresponding values for the apertures.\\
Here, we adopt the latter approach and describe the fractures by means of lower-dimensional geometries.

In this exercise, let us consider a two-dimensional computational domain $\Omega \in \mathbb{R}^2$.
This domain is then split into a 2D domain for the matrix, $\Omega_\text{m}$, and a 1D domain for the fracture network, $\Omega_\text{f}$.
We denote by $\Gamma_\text{m}$ the outer boundary of $\Omega_\text{m}$ and further decompose this into $\Gamma_\text{m}^\text{D}$ and $\Gamma_\text{m}^\text{N}$, referring to the subsets on which Dirichlet and Neumann conditions are specified, respectively.
The boundary of $\Omega_\text{f}$ is denoted with $\Gamma_\text{f}$.
Finally, the interface between the matrix and the fracture domains are denoted with $\gamma = \Omega_\text{m} \cap \Omega_\text{f}$.
Making use of this notation, we state the governing system of equations as follows:
\begin{subequations}
  \label{eq:ex_frac_problem}
  \begin{align}
    \darcyVelocity_{\fluidPhaseIdx, i}
      + \frac{\relPerm{\fluidPhaseIdx, i}}{\dynVisc_\fluidPhaseIdx} \permeability_i
        \left( \nabla \pressure_{\fluidPhaseIdx, i} - \density_{\fluidPhaseIdx} \gravity \right)
    &= 0, &&\mathrm{in} \, \Omega_i, \label{eq:ex_frac_darcy}\\
    \frac{\partial \left( \porosity_\text{m} \density_{\fluidPhaseIdx} \saturation_{\fluidPhaseIdx, \text{m}} \right)}{\partial t}
    + \nabla \cdot \left( \density_{\fluidPhaseIdx} \darcyVelocity_{\fluidPhaseIdx, \text{m}} \right)
    &= 0,
    &&\mathrm{in} \, \Omega_\text{m}, \label{eq:ex_frac_massBalanceMatrix} \\
    \frac{\partial \left( a \, \porosity_\text{f} \density_{\fluidPhaseIdx} \saturation_{\fluidPhaseIdx, \text{f}} \right)}{\partial t}
    + \nabla \cdot \left( a \, \density_{\fluidPhaseIdx} \darcyVelocity_{\fluidPhaseIdx, \text{f}} \right)
    &= \llbracket \density_{\fluidPhaseIdx} \darcyVelocity_{\fluidPhaseIdx, \text{m}} \cdot \mathbf{n} \rrbracket,
    &&\mathrm{in} \, \Omega_\text{f}, \label{eq:ex_frac_massBalanceFracture} \\
    - \frac{\relPerm{\fluidPhaseIdx,f}}{\dynVisc_{\fluidPhaseIdx}} \mathbf{n}^T \permeability_\text{m}^\perp
    \left( \frac{\pressure_{\fluidPhaseIdx, \text{m}} - \pressure_{\fluidPhaseIdx, \text{f}}}{a/2} \, \mathbf{n} - \density_{\fluidPhaseIdx} \gravity \right)
    &= \darcyVelocity_{\fluidPhaseIdx, \text{m}} \cdot \mathbf{n}, &&\mathrm{on} \, \gamma, \label{eq:ex_frac_ifCondition}
  \end{align}
\end{subequations}

for $i \in \{\text{m}, \text{f} \}$ and $\fluidPhaseIdx \in \{ \text{w}, \text{n} \}$, where $\text{w}$ and $\text{n}$ refer to the wetting and the nonwetting phase, respectively.
Furthermore, $\density_\fluidPhaseIdx$ and $\dynVisc_\fluidPhaseIdx$ are the density and viscosity of a fluid phase $\fluidPhaseIdx$, while $\pressure_{\fluidPhaseIdx, i}$, $\saturation_{\fluidPhaseIdx, i}$ and $\relPerm{\fluidPhaseIdx, i}$ denote the fluid phase pressure, saturation and relative permeability as prevailing in subdomain $\Omega_i$, $i \in \{\text{m}, \text{f} \}$.
Correspondingly, $\permeability_i$ and $\porosity_i$ denote the intrinsic permeability and the porosity in a subdomain, and $\darcyVelocity_{\fluidPhaseIdx, i}$ is the respective Darcy velocity.
Finally, $\permeability_\text{f}^\perp$ denotes the normal part of the permeability in the fracture domain and $a$ is the aperture of the fracture.\\
\eqref{eq:ex_frac_darcy} states that Darcy's law is valid within both subdomains, while \eqref{eq:ex_frac_massBalanceMatrix} and \eqref{eq:ex_frac_massBalanceFracture} are the mass balance equations for the fluid phases in the matrix and fracture subdomain, respectively.
Note that the term $\llbracket \density_{\fluidPhaseIdx} \darcyVelocity_{\fluidPhaseIdx, \text{m}} \cdot \mathbf{n} \rrbracket$ describes the jump in the normal flux of a phase $\fluidPhaseIdx$ across the fracture and acts as an additional source or sink term, caused by the interaction with the surrounding matrix.
Flux and pressure continuity on the interface between matrix and fractures are enforced by means of \eqref{eq:ex_frac_ifCondition}, where the normal flux inside the fracture is approximated by a finite difference.
Note that the pressures $\pressure_{\fluidPhaseIdx, \text{f}}$ on the lower-dimensional fracture domain $\Omega_\text{f}$ refer to cross-section averaged pressures, and the gradient and divergence operators $\nabla$ and $\nabla \cdot$ on $\Omega_\text{f}$ are to be understood in tangential direction of the lower-dimensional manifold.
The system of equations is completed by the following set of boundary conditions:

\begin{subequations}
  \label{eq:ex_frac_bcs}
  \begin{align}
    a \, \density_{\fluidPhaseIdx} \darcyVelocity_{\fluidPhaseIdx, \text{f}} \cdot \mathbf{n}
    &= 0, &&\mathrm{on} \, \Gamma_\text{f} \label{eq:ex_frac_neumann_frac}\\
    \density_{\fluidPhaseIdx} \darcyVelocity_{\fluidPhaseIdx, \text{m}} \cdot \mathbf{n}
    &= 0, &&\mathrm{on} \, \Gamma_\text{m}^\text{N} \label{eq:ex_frac_neumann_matrix}\\
    \pressure_{\text{w}, \text{m}} &= \pressure^\text{D}, &&\mathrm{on} \, \Gamma_\text{m}^\text{D},
                                    \label{eq:ex_frac_Dirichlet_matrix_p}\\
    \saturation_{\text{n}, \text{m}} &= \saturation^\text{D}_{\text{n}}, &&\mathrm{on} \, \Gamma_\text{m}^\text{D}. \label{eq:ex_frac_Dirichlet_matrix_S}
  \end{align}
\end{subequations}

\subsection*{Background: Hydraulic fracturing}

Hydraulic fracturing (or: fracking) refers to a stimulation of rock by injection of fluids, typically water with additives, aiming at increasing the rock's permeability. This aims at facilitating the production of so-called unconventional natural gas. It is also common to denote this as shale gas when it is stored in shales with such low permeabilities that it cannot be produced with conventional technologies.

\begin{figure}[tbh]
\centerline{\includegraphics[width=10cm,angle=0.]{./pics/frackinggrafik.png}}
    \caption{Exemplary illustration: Hydraulic fracturing. The fracked shale layer is vertically strongly exaggerated in order to show the typical features of a fracked shale: fracks perpendicular to the horizontal well, a network of small fractures, methane released from the shale and potentially escaping, fracking fluid pumped into the shale.}
    \protect\label{frackinggrafik}
\end{figure}

Hydraulic fracturing typically requires a horizontally deviated wellbore which is used to inject a liquid, the so-called fracking fluid, in stages over intervals into the formation. These fracking fluids are water with a cocktail of chemicals and so-called proppants. The chemicals serve for different purposes, e.g. anti-corrosion, biocides, etc., and many of the additives are considered as being toxic and harmful to humans and to the environment. Proppants are added in order to keep the newly created fractures open such that gas flow is possible during the hydrocarbon production.

The stimulation period, where fractures are generated in the shale layers, is relatively short, a few hours, for example, while the subsequent gas production period can continue for a long time. Potentially hazardous events related to fracking are manifold and many of them do not need our flow and transport models to be prevented and managed, like spilling of fluids and accidents at the surface, not properly sealed wells, etc. However, there are hazards that are inherently related to subsurface flow processes and hydrogeological background, and these are those which are associated with enormous uncertainties. This makes risk assessment a difficult task and a delicate issue for communication to stakeholders and public opinion. It is precisely the short period of stimulation when these hazards are initiated and triggered. They can roughly be distinguished in three categories: (i) The release of fracking fluid in the subsurface which may harm groundwater resources, (ii) the uncontrolled migration of methane released from the shale into the overburden, eventually also into aquifers or even to the surface, and (iii) induced seismicity, which means microseismic events, small earthquakes, triggered by the high pressure during the stimulation period.

The example we have here in this exercise is strongly simplified and shows only in principle the effects of the two-phase flow system in the low-permeable matrix and the high-permeable fractures with high pressure gradients during the injection and buoyancy (methane is much less dense than water) as the main driving forces. Thus, the example addresses only the second above-mentioned category - methane migration.
The example assumes that we have a shale where water is injected at the bottom over a certain time (see input-file). The fracture network is pre-existing (which would actually be generated only during the injection!).

\subsection*{Questions}
\begin{enumerate}
\item Compile and run the exercise.\\
      Open paraview and load both pvd-files, one for the fracture domain, the
      other for the matrix domain. Check out how gas saturation develops
      and compare with the boundary conditions as implemented in the problem.
      You may also have a look at the water phase pressure.
\item Check out how the mass of escaped gas (into the overburden) and the mass of injected water evolve over time.
      You find information in the simulation output after each time step.
      The escapedGas is written into a file escapedGas.dat and can be plotted,
      for example, with gnuplot.
\item Compare the gas escaped into the overburden at different injection rates,
      permeability values, etc.
      In particular take care for the injection pressures at the bottom. Pressures higher
      than 100 MPa are not realistic to be generated with common techniques.
\item Advanced: Change the boundary conditions such that you model a scenario with gas production through an
      assumed horizontal well at the bottom of the domain. Try to set a Dirichlet pressure condition at the bottom boundary
      to apply underpressure.
\end{enumerate}
