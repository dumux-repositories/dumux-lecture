// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/numeqvector.hh>

// include the base problem we inherit from
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,
        contiH2OEqIdx = Indices::conti0EqIdx
    };

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , time_(0.0)
    , initialShaleSaturation_(getParamFromGroup<Scalar>(paramGroup, "Problem.InitialShaleMethaneSaturation"))
    , injectionRate_(getParam<Scalar>("Problem.InjectionRate"))
    , injectionPosition_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionPosition"))
    , injectionLength_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionLength"))
    , injectionDuration_(getParamFromGroup<Scalar>(paramGroup, "Problem.InjectionDuration"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::init();
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        values.setAllNeumann();

        // use Dirichlet at the top boundary
        if (globalPos[1] > this->gridGeometry().bBoxMax()[1] - 1e-6)
            values.setAllDirichlet();

        return values;
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated.
        values.setAllNeumann();
        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        auto values = initialAtPos(globalPos);

        // use zero-saturation within the overburden
        if (globalPos[1] > 35.0)
            values[saturationIdx] = 0.0;

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        auto values = NumEqVector(0.0);

        // within the injection region
        if (globalPos[1] < 1e-6
            && globalPos[0] > injectionPosition_
            && globalPos[0] < injectionPosition_ + injectionLength_
            && time_ < injectionDuration_ + 1e-6)
            values[contiH2OEqIdx] = injectionRate_;

        return values;
    }


    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        // For the grid used here, the height of the domain is equal
        // to the maximum y-coordinate
        const auto domainHeight = this->gridGeometry().bBoxMax()[1];

        // we assume a constant water density of 1000 for initial conditions!
        const auto& g = this->spatialParams().gravity(globalPos);
        PrimaryVariables values;
        Scalar densityW = 1000.0;
        values[pressureIdx] = 1e5 - (domainHeight - globalPos[1])*densityW*g[1];

        // in the overburden (and maybe in the upper meters of the shale there is no gas initially)
        if (globalPos[1] > 35.0)  // take 35 meters if you only want zero gas in the overburden
            values[saturationIdx] = 0.0;

        // the shale formation contains gas initially
        else
            values[saturationIdx] = initialShaleSaturation_;

        return values;
    }

    //! computes the gas flux from the reservoir into the overburden
    template<class GridVariables, class SolutionVector, class Assembler, std::size_t id>
    Scalar computeGasFluxToOverburden(const GridVariables& gridVars,
                                      const SolutionVector& sol,
                                      const Assembler& assembler,
                                      const Dune::index_constant<id> domainId) const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            // only proceed if elem could potentially be on the interface to overburden
            const auto geometry = element.geometry();
            if (geometry.center()[1] < 35.0 && geometry.center()[1] > 15.0)
            {
                couplingManagerPtr_->bindCouplingContext(domainId, element, assembler);

                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bind(element);

                auto elemVolVars = localView(gridVars.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());
                elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

                for (const auto& scvf : scvfs(fvGeometry))
                {
                    if (scvf.boundary())
                        continue;

                    const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                    const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());

                    const bool isOnInterface = insideScv.center()[1] < 35.0 && outsideScv.center()[1] > 35.0;
                    if (isOnInterface)
                    {
                        using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
                        FluxVariables fluxVars;
                        fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                        // mass upwind term
                        const auto gasPhaseIdx = GetPropType<TypeTag, Properties::FluidSystem>::comp1Idx;
                        auto upwindTerm = [gasPhaseIdx] (const auto& volVars)
                        { return volVars.mobility(gasPhaseIdx)*volVars.density(gasPhaseIdx); };

                        flux += fluxVars.advectiveFlux(gasPhaseIdx, upwindTerm);
                    }
                }
            }
        }

        return flux;
    }

    //! computes the influx of water into the domain
    Scalar computeInjectionFlux() const
    {
        Scalar flux = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bind(element);

            for (const auto& scvf : scvfs(fvGeometry))
                if (scvf.boundary())
                    flux -= neumannAtPos(scvf.ipGlobal())[contiH2OEqIdx]*scvf.area();
        }

        return flux;
    }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! sets the current time
    void setTime(Scalar time)
    { time_ = time; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

    Scalar time_;
    Scalar initialShaleSaturation_;

    // injection specifics
    Scalar injectionRate_;
    Scalar injectionPosition_;
    Scalar injectionLength_;
    Scalar injectionDuration_;
};

} // end namespace Dumux

#endif
