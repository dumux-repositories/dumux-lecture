// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief The spatial parameters for the fracture sub-domain in the exercise
 *        on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_SPATIALPARAMS_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_SPATIALPARAMS_HH


#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>

namespace Dumux {

/*!
 * \brief The spatial params the two-phase facet coupling test
 */
template< class FVGridGeometry, class Scalar >
class MatrixSpatialParams
: public FVPorousMediumFlowSpatialParamsMP< FVGridGeometry, Scalar, MatrixSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = MatrixSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVPorousMediumFlowSpatialParamsMP< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // use a regularized van-genuchten material law
    using PcKrSw = FluidMatrix::VanGenuchtenDefault<Scalar>;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    MatrixSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                        const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , pcKrSw_("SpatialParams")
    , pcKrSwOverburden_("SpatialParams.Overburden")
    {
        porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        temperature_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Temperature");
        porosityOverburden_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Overburden.Porosity");
        permeabilityOverburden_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Overburden.Permeability");
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if (globalPos[1] > 35.0)
            return permeabilityOverburden_;
        else if ((globalPos[1] > 32)||(globalPos[1] < 30))
            return permeability_;
        else return permeability_/10000.;
    }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        if (globalPos[1] > 35.0)
            return porosityOverburden_;
        else if ((globalPos[1] > 32)||(globalPos[1] < 30))
            return porosity_;
        else return 0.05;
    }

    //! Return the material law parameters
    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    { return globalPos[1] > 35.0 ? pcKrSwOverburden_ : pcKrSw_; }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // we set water as the wetting phase here
        // which is phase0Idx in the H2oN2 fluid system
        return FluidSystem::phase0Idx;
    }

    /*!
     * \brief Returns the temperature at the domain at the given position
     * \param globalPos The position in global coordinates where the temperature should be specified
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return temperature_;
    }

private:
    Scalar porosity_;
    Scalar porosityOverburden_;
    PermeabilityType permeability_;
    PermeabilityType permeabilityOverburden_;
    const PcKrSw pcKrSw_;
    const PcKrSw pcKrSwOverburden_;
    Scalar temperature_;
};

} // end namespace Dumux

#endif
