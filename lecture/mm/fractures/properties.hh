// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
 /*!
  * \file
  * \brief The properties file of the exercise on two-phase flow in fractured porous media.
  */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_PROPERTIES_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_PROPERTIES_HH

// Both matrix and fracture sub-problem
// we want to simulate methan gas transport in a water-saturated medium
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>
// include the model we inherit from
#include <dumux/porousmediumflow/2p/model.hh>


// Fracture sub-problem
// we use alu grid for the discretization of the fracture domain
// as this grid manager is able to represent network/surface grids
#include <dune/foamgrid/foamgrid.hh>
// we use a cell-centered finite volume scheme with tpfa here
#include <dumux/discretization/cctpfa.hh>
// the spatial parameters (permeabilities, material parameters etc.)
#include "fracturespatialparams.hh"
//The problem file, where setup-specific boundary and initial conditions are defined.
#include "fractureproblem.hh"


// Matrix sub-problem
// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>
// We are using the framework for models that consider coupling
// across the element facets of the bulk domain. This has some
// properties defined, which we have to inherit here. In this
// exercise we want to use a cell-centered finite volume scheme
// with tpfa.
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"
//The problem file, where setup-specific boundary and initial conditions are defined.
#include "matrixproblem.hh"

namespace Dumux::Properties {

// create the type tag nodes
// Create new type tags
namespace TTag {

// create the typetag for fracture sub-problem
struct FractureProblemTypeTag { using InheritsFrom = std::tuple<TwoP, CCTpfaModel>; };

// create the type tag node for the matrix sub-problem
// We need to put the facet-coupling type tag after the physics-related type tag
// because it overwrites some of the properties inherited from "TwoP". This is
// necessary because we need e.g. a modified implementation of darcys law that
// evaluates the coupling conditions on faces that coincide with the fractures.
struct MatrixProblemTypeTag { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, TwoP>; };
} // end namespace TTag

// Set the grid type for fracture sub-problem
template<class TypeTag>
struct Grid<TypeTag, TTag::FractureProblemTypeTag> { using type = Dune::FoamGrid<1, 2>; };

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct Grid<TypeTag, TTag::MatrixProblemTypeTag> { using type = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>; };

// Set the problem type for fracture sub-problem
template<class TypeTag>
struct Problem<TypeTag, TTag::FractureProblemTypeTag> { using type = FractureSubProblem<TypeTag>; };

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct Problem<TypeTag, TTag::MatrixProblemTypeTag> { using type = MatrixSubProblem<TypeTag>; };

// Set the spatial parameters for fracture sub-problem
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::FractureProblemTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FractureSpatialParams<FVGridGeometry, Scalar>;
};

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::MatrixProblemTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = MatrixSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system for fracture sub-problem
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FractureProblemTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2O = Dumux::Components::H2O<Scalar>;
    using TabH2O = Dumux::Components::TabulatedComponent<H2O>;
    using CH4 = Dumux::Components::CH4<Scalar>;

    // turn components into gas/liquid phase
    using LiquidPhase = Dumux::FluidSystems::OnePLiquid<Scalar, TabH2O>;
    using GasPhase = Dumux::FluidSystems::OnePGas<Scalar, CH4>;
public:
    using type = Dumux::FluidSystems::TwoPImmiscible<Scalar, LiquidPhase, GasPhase>;
};

// Set the grid type for matrix sub-problem
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::MatrixProblemTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2O = Dumux::Components::H2O<Scalar>;
    using TabH2O = Dumux::Components::TabulatedComponent<H2O>;
    using CH4 = Dumux::Components::CH4<Scalar>;

    // turn components into gas/liquid phase
    using LiquidPhase = Dumux::FluidSystems::OnePLiquid<Scalar, TabH2O>;
    using GasPhase = Dumux::FluidSystems::OnePGas<Scalar, CH4>;
public:
    using type = Dumux::FluidSystems::TwoPImmiscible<Scalar, LiquidPhase, GasPhase>;
};

} // end namespace Dumux::Properties

#endif
