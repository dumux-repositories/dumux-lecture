// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Non-isothermal SAGD properties file
 */
#ifndef DUMUX_SAGDPROPERTIES_HH
#define DUMUX_SAGDPROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/3pwateroil/model.hh>

//#include "../myh2oheavyoilwithsimpleh2odensities.hh"
#include <dumux/material/fluidsystems/h2oheavyoil.hh>
#include <dumux/material/solidsystems/1csolid.hh>
#include <dumux/material/components/constant.hh>

#include "spatialparams.hh"
#include "problem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct SagdTypeTag { using InheritsFrom = std::tuple<ThreePWaterOilNI>; };
struct ThreePWaterOilSagdBoxTypeTag { using InheritsFrom = std::tuple<BoxModel, SagdTypeTag>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::SagdTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::SagdTypeTag> { using type = Dumux::SagdProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::SagdTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = SagdSpatialParams<FVGridGeometry, Scalar>;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::SagdTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::H2OHeavyOil<Scalar>;
};

template<class TypeTag>
struct OnlyGasPhaseCanDisappear<TypeTag, TTag::SagdTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::SagdTypeTag> { static constexpr bool value = true; };

// Set the fluid system
template<class TypeTag>
struct SolidSystem<TypeTag, TTag::SagdTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using InertComponent = Components::Constant<1, Scalar>;
    using type = SolidSystems::InertSolidPhase<Scalar, InertComponent>;
};

} // end namespace Dumux::Properties

#endif
