// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Definition of the spatial parameters for the SAGD problem.
 */

#ifndef DUMUX_SAGD_SPATIAL_PARAMS_HH
#define DUMUX_SAGD_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/porousmediumflow/fvspatialparamsmp.hh>

#include <dumux/porousmediumflow/3pwateroil/indices.hh>

#include "../3p/parkervangenuchtenzerohysteresis.hh"

namespace Dumux {

/*!
 * \brief Definition of the spatial parameters for the SAGD problem
 */
template<class FVGridGeometry, class Scalar>
class SagdSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<FVGridGeometry, Scalar,
                         SagdSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    enum { dimWorld=GridView::dimensionworld };

    using GlobalPosition = typename SubControlVolume::GlobalPosition;

    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<FVGridGeometry, Scalar,
                                       SagdSpatialParams<FVGridGeometry, Scalar>>;

    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethods::box;
    static constexpr int dofCodim = isBox ? GridView::dimension : 0;

    using ThreePhasePcKrSw = FluidMatrix::ParkerVanGenZeroHyst3P<Scalar>;

public:
    using PermeabilityType = Scalar;

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    SagdSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), eps_(1e-6)
    {
        layerBottom_ = 35.0;

        // intrinsic permeabilities
        fineK_ = 1e-16;
        coarseK_ = 4e-14;

        // porosities
        finePorosity_ = 0.10;
        coarsePorosity_ = 0.1;

        typename ThreePhasePcKrSw::Params baseParamsFine;
        typename ThreePhasePcKrSw::Params baseParamsCoarse;

        // residual saturations
        baseParamsFine.setSwr(0.1);
        baseParamsFine.setSnr(0.09);   //Residual of NAPL if there is no water
        baseParamsFine.setSgr(0.01);
        baseParamsCoarse.setSwr(0.1);
        baseParamsCoarse.setSnr(0.09);
        baseParamsCoarse.setSgr(0.01);

        // parameters for the 3phase van Genuchten law
        baseParamsFine.setVgn(4.0);
        baseParamsCoarse.setVgAlpha(0.0005);
        baseParamsCoarse.setVgn(4.0);
        baseParamsCoarse.setVgAlpha(0.0015);

        baseParamsFine.setKrRegardsSnr(false);
        baseParamsCoarse.setKrRegardsSnr(false);

        pcKrSwCurveFine_ = std::make_unique<ThreePhasePcKrSw>(baseParamsFine);
        pcKrSwCurveCoarse_ = std::make_unique<ThreePhasePcKrSw>(baseParamsCoarse);
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$
     * \note  It is possibly solution dependent.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {  return permeabilityAtPos(scv.dofPosition());}

    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$
     *
     * \param globalPos The global position
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if (isFineMaterial_(globalPos))
            return fineK_;
        return coarseK_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        if (isFineMaterial_(globalPos))
            return finePorosity_;
        else
            return coarsePorosity_;
    }

    /*!
     * \brief Returns the fluid-matrix interaction law (kr-sw, pc-sw, etc.).
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        return fluidMatrixInteractionAtPos(scv.dofPosition());
    }

    /*!
     * \brief Returns the fluid-matrix interaction law (kr-sw, pc-sw, etc.).
     *
     * \param globalPos The global position
     */
    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    {
        assert(pcKrSwCurveFine_ && pcKrSwCurveCoarse_);
        if (isFineMaterial_(globalPos))
            return makeFluidMatrixInteraction(*pcKrSwCurveFine_);
        else
            return makeFluidMatrixInteraction(*pcKrSwCurveCoarse_);
    }

    struct MaxSaturations
    {
        Scalar MaxSatW;
        Scalar MaxSatN;
        Scalar MaxSatG;
    };

    struct trappedSaturations
    {
        Scalar trappedSatN;
    };

    template <class Problem, class VolumeVariables, class FluidSystem, class SolutionVector>
    void getMaxSaturation(Problem &problem, const SolutionVector& x)
    {
        // get the number of degrees of freedom
        auto fvGeometry = localView(this->gridGeometry());
        VolumeVariables volVars;
        maxSats_.resize(this->gridGeometry().numDofs());

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            fvGeometry.bindElement(element);
            auto elemSol = elementSolution(element, x, this->gridGeometry());

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto dofIdxGlobal = scv.dofIndex();
                volVars.update(elemSol, problem, element, scv);

                if (volVars.saturation(FluidSystem::wPhaseIdx)>maxSats_[dofIdxGlobal].MaxSatW)
                    maxSats_[dofIdxGlobal].MaxSatW = volVars.saturation(FluidSystem::wPhaseIdx);

                if (volVars.saturation(FluidSystem::nPhaseIdx)>maxSats_[dofIdxGlobal].MaxSatN)
                    maxSats_[dofIdxGlobal].MaxSatN = volVars.saturation(FluidSystem::nPhaseIdx);

                if (volVars.saturation(FluidSystem::gPhaseIdx)>maxSats_[dofIdxGlobal].MaxSatG)
                    maxSats_[dofIdxGlobal].MaxSatG = volVars.saturation(FluidSystem::gPhaseIdx);
            }
        }
    }

    template <class Problem, class VolumeVariables, class SolutionVector>
    void trappedSat(Problem &problem, const SolutionVector& x)
    {
        // get the number of degrees of freedom
        auto fvGeometry = localView(this->gridGeometry());
        VolumeVariables volVars;
        trapSats_.resize(this->gridGeometry().numDofs());

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            fvGeometry.bindElement(element);
            auto elemSol = elementSolution(element, x, this->gridGeometry());

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto dofIdxGlobal = scv.dofIndex();
                volVars.update(elemSol, problem, element, scv);

                static const Scalar landParameter = 9.0; // according to Land, 1968
                trapSats_[dofIdxGlobal].trappedSatN =
                    (maxSats_[dofIdxGlobal].MaxSatN)/(1.0 + landParameter*maxSats_[dofIdxGlobal].MaxSatN);

                // TODO this makes no sense, you are overwriting the value with each dof.
                // you probably want a vector of pcSwcurves for each dof?
                // or update the trapped sn just before you return it from fluidMatrixInteractionAtPos?
                pcKrSwCurveFine_->updateTappedSn(trapSats_[dofIdxGlobal].trappedSatN);
                pcKrSwCurveCoarse_->updateTappedSn(trapSats_[dofIdxGlobal].trappedSatN);
            }
        }
    }

private:
    bool isFineMaterial_(const GlobalPosition &pos) const
    {
        return pos[dimWorld-1] > layerBottom_ - eps_;
    };

    std::vector<trappedSaturations> trapSats_;
    std::vector<MaxSaturations> maxSats_;

    Scalar layerBottom_;

    Scalar fineK_;
    Scalar coarseK_;

    Scalar finePorosity_;
    Scalar coarsePorosity_;

    std::unique_ptr<ThreePhasePcKrSw> pcKrSwCurveFine_;
    std::unique_ptr<ThreePhasePcKrSw> pcKrSwCurveCoarse_;

    Scalar eps_;
};

}

#endif
