// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Implementation of van Genuchten's capillary pressure-saturation relation.
 *
 */
#ifndef DUMUX_RELATIVEPERMHYST3P_HH
#define DUMUX_RELATIVEPERMHYST3P_HH

#include <algorithm>

#include "relativepermeabilityhysteresisparams.hh"

namespace Dumux {

/*!
 *
 * \brief Implementation of van Genuchten's capillary pressure <->
 *        saturation relation. This class bundles the "raw" curves
 *        as static members and doesn't concern itself converting
 *        absolute to effective saturations and vince versa.
 *
 * \sa VanGenuchten, VanGenuchtenThreephase
 */
template <class ScalarT, class ParamsT = relativPermHyst3PParams<ScalarT> >
class relativPermHyst3P
{
public:
    using Params = ParamsT;
    using Scalar = typename Params::Scalar;

    static Scalar pc(const Params &params, Scalar sw)
    {
        DUNE_THROW(Dune::NotImplemented, "Capillary pressures for three phases is not so simple! Use pcgn, pcnw, and pcgw");
    }

    static Scalar pcgw(const Params &params, Scalar sw)
    {
        return 0;
    }

    static Scalar pcnw(const Params &params, Scalar sw)
    {
        return 0;
    }

    static Scalar pcgn(const Params &params, Scalar St)
    {
        return 0;
    }

    static Scalar pcAlpha(const Params &params, Scalar sn)
    {
        return(1);
    }

    static Scalar sw(const Params &params, Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, "sw(pc) for three phases not implemented! Do it yourself!");
    }

    static Scalar dpc_dsw(const Params &params, Scalar sw)
    {
        DUNE_THROW(Dune::NotImplemented, "dpc/dsw for three phases not implemented! Do it yourself!");
    }

    static Scalar dsw_dpc(const Params &params, Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, "dsw/dpc for three phases not implemented! Do it yourself!");
    }

    static Scalar C1_w(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C1_w_ = ((sn - params.Snc())*params.C1_wn() + (sg - params.Sgc())*params.C1_wg()) / (1-sw-params.Snc()- params.Sgc());
        return C1_w_;
    };

    static Scalar C1_n(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C1_n_ = ((sw - params.Swc())*params.C1_wn() + (sg - params.Sgc())*params.C1_ng()) / (1-sn-params.Swc()- params.Sgc());
        return C1_n_;
    };

    static Scalar C1_g(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C1_g_ = ((sw - params.Swc())*params.C1_wg() + (sn - params.Snc())*params.C1_ng()) / (1-sg-params.Swc()- params.Snc());
        return C1_g_;
    };

    static Scalar C2_w(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C2_w_ = ((sn - params.Snc())*params.C2_wn() + (sg - params.Sgc())*params.C2_wg()) / (1-sw-params.Snc()- params.Sgc());
        return C2_w_;
    };

    static Scalar C2_n(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C2_n_ = ((sw - params.Swc())*params.C2_wn() + (sg - params.Sgc())*params.C2_ng()) / (1-sn-params.Swc()- params.Sgc());
        return C2_n_;
    };

    static Scalar C2_g(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C2_g_ = ((sw - params.Swc())*params.C2_wg() + (sn - params.Snc())*params.C2_ng()) / (1-sg-params.Swc()- params.Snc());
        return C2_g_;
    };

    static Scalar kr0_w(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar kr0_w_ = ((sn - params.Snc())*params.kr0_wn() + (sg - params.Sgc())*params.kr0_wg()) / (1-sw-params.Snc()- params.Sgc());
        return kr0_w_;
    };

    static Scalar kr0_n(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar kr0_n_ = ((sw - params.Swc())*params.kr0_wn() + (sg - params.Sgc())*params.kr0_ng()) / (1-sn-params.Swc()- params.Sgc());
        return kr0_n_;
    };

    static Scalar kr0_g(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar kr0_g_ = ((sw - params.Swc())*params.kr0_wg() + (sn - params.Snc())*params.kr0_ng()) / (1-sg-params.Swc()- params.Snc());
        return kr0_g_;
    };


   static Scalar s2Pr_w(const Params &params,Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar s2Pr_w_ = ((sn - params.Snc()) * params.redSwrn() + ((sg - params.Sgc()) * params.redSwrg()))/ (1-sw-params.Snc()- params.Sgc());
        return s2Pr_w_;
    };

    static Scalar s2Pr_n(const Params &params,Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar s2Pr_n_ = ((sw - params.Swc()) * params.redSnrw() + ((sg - params.Sgc()) * params.redSnrg()))/ (1-sn-params.Swc()- params.Sgc());
        return s2Pr_n_;
    };

    static Scalar s2Pr_g(const Params &params,Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar s2Pr_g_ = ((sw - params.Swc()) * params.redSgrw() + ((sn - params.Snc()) * params.redSgrn()))/ (1-sg-params.Swc()- params.Snc());

        return s2Pr_g_;
    };

    static Scalar s3Pr_w(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar s2Pr_w_ = s2Pr_w (params, sw, sn, sg);

        Scalar s3Pr_w_ = std::min(sw, std::max(params.Swc(), s2Pr_w_ * (1-params.Bw()*(sn - params.Snc())*(sg-params.Sgc()))));
        return s3Pr_w_;
    };

    static Scalar s3Pr_n(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar s2Pr_n_ = s2Pr_n ( params , sw, sn, sg);
        Scalar s3Pr_n_ = std::min(sn, std::max(params.Snc(), s2Pr_n_ * (1-params.Bn()*(sw - params.Swc())*(sg-params.Sgc()))));
        return s3Pr_n_;
    };

    static Scalar s3Pr_g(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar s2Pr_g_ = s2Pr_g ( params , sw, sn, sg);
        Scalar s3Pr_g_ = std::min(sg, std::max(params.Sgc(), s2Pr_g_ * (1-params.Bg()*(sn - params.Snc())*(sw-params.Sgc()))));
        return s3Pr_g_;
    };

    static Scalar krw(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C1_w_ = C1_w ( params, sw, sn, sg );
        Scalar C2_w_ = C2_w ( params, sw, sn, sg );
        Scalar kr0_w_ = kr0_w ( params, sw, sn, sg );
        Scalar s3Pr_w_ = s3Pr_w ( params, sw, sn, sg );
        Scalar s3Pr_n_ = s3Pr_n ( params, sw, sn, sg );
        Scalar s3Pr_g_ = s3Pr_g ( params,  sw, sn, sg );

        Scalar normalSw = ((sw - std::min(sw, params.TrappedSatWx()))/(1-(s3Pr_w_ + s3Pr_n_ + s3Pr_g_)));   //eq. 4
        Scalar krw_ = kr0_w_ * ((1+C2_w_)*std::pow(normalSw,C1_w_)) / (1+C2_w_*std::pow(normalSw,C1_w_*(1+(1/C2_w_))));

        return krw_;
    };

    static Scalar krn(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C1_n_ = C1_n ( params, sw, sn, sg );
        Scalar C2_n_ = C2_n ( params,  sw, sn, sg );
        Scalar kr0_n_ = kr0_n ( params,  sw, sn, sg );
        Scalar s3Pr_w_ = s3Pr_w ( params,  sw, sn, sg );
        Scalar s3Pr_n_ = s3Pr_n ( params, sw, sn, sg );
        Scalar s3Pr_g_ = s3Pr_g ( params,  sw, sn, sg );

        Scalar normalSn = ((sn - std::min(sn, params.TrappedSatNx()))/(1-(s3Pr_w_ + s3Pr_n_ + s3Pr_g_)));   //eq. 4
        Scalar krn_ = kr0_n_ * ((1+C2_n_)*std::pow(normalSn,C1_n_)) / (1+C2_n_*std::pow(normalSn,C1_n_*(1+(1/C2_n_))));

        return krn_;
    };

    static Scalar krg(const Params &params,    Scalar sw, Scalar sn, Scalar sg)
    {
        Scalar C1_g_ = C1_g ( params,  sw, sn, sg );
        Scalar C2_g_ = C2_g ( params,  sw, sn, sg );
        Scalar kr0_g_ = kr0_g ( params,  sw, sn, sg );
        Scalar s3Pr_w_ = s3Pr_w ( params,  sw, sn, sg );
        Scalar s3Pr_n_ = s3Pr_n ( params,  sw, sn, sg );
        Scalar s3Pr_g_ = s3Pr_g ( params,  sw, sn, sg );

        Scalar normalSg = ((sg - std::min(sg, params.TrappedSatGx()))/(1-(s3Pr_w_ + s3Pr_n_ + s3Pr_g_)));   //eq. 4
        Scalar krg_ = kr0_g_ * ((1+C2_g_)*std::pow(normalSg,C1_g_)) / (1+C2_g_*std::pow(normalSg,C1_g_*(1+(1/C2_g_))));

        return krg_;
    };

    static Scalar kr(const Params &params, const int phase, const Scalar sw, const Scalar sn, const Scalar sg)
    {
        switch (phase)
        {
        case 0:
            return krw(params, sw, sn, sg);
            break;
        case 1:
            return krn(params, sw, sn, sg);
            break;
        case 2:
            return krg(params, sw, sn, sg);
            break;
        }

        return 0;
    };

   static Scalar bulkDensTimesAdsorpCoeff (const Params &params)
   {
      return params.rhoBulk() * params.KdNAPL();
   }

};

} // end namespace Dumux

#endif
