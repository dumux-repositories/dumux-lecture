// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Implementation of van Genuchten's capillary pressure-saturation relation.
 *
 */
#ifndef DUMUX_PARKERVANGENZEROHYST_3P_HH
#define DUMUX_PARKERVANGENZEROHYST_3P_HH

#include <cmath>
#include <algorithm>

#include <dune/common/fvector.hh>
#include <dumux/material/fluidmatrixinteractions/fluidmatrixinteraction.hh>

namespace Dumux::FluidMatrix {
/*!
 * \brief Implementation of van Genuchten's capillary pressure <->
 *        saturation relation. This class bundles the "raw" curves
 *        as static members and doesn't concern itself converting
 *        absolute to effective saturations and vince versa.
 *
 * \sa VanGenuchten, VanGenuchtenThreephase
 */
template <class ScalarT>
class ParkerVanGenZeroHyst3P : Adapter<ParkerVanGenZeroHyst3P<ScalarT>, ThreePhasePcKrSw>
{
public:
    using Scalar = ScalarT;

    /*!
     * \brief The parameter type
     * \tparam Scalar The scalar type
     */
    struct Params
    {
        Params()
        {
            betaGw_ = betaNw_ = betaGn_ = 1.;
        }

        Params(Scalar vgAlpha,
               Scalar vgn,
               Dune::FieldVector<Scalar, 4> residualSaturation,
               Scalar betaNw = 1.,
               Scalar betaGn = 1.,
               Scalar betaGw = 1.,
               bool regardSnr = false,
               Scalar TrappedSatN = 1.)
        {
            setVgAlpha(vgAlpha);
            setVgn(vgn);
            setSwr(residualSaturation[0]);
            setSnr(residualSaturation[1]);
            setSgr(residualSaturation[2]);
            setSwrx(residualSaturation[3]);
            setBetaNw(betaNw);
            setBetaGn(betaGn);
            setBetaGw(betaGw);
            setTrappedSatN(TrappedSatN);
        };

        Scalar TrappedSatN() const
        {
            return TrappedSatN_;
        }

        void setTrappedSatN(Scalar v)
        {
            TrappedSatN_ = v;
        }

        /*!
         * \brief Return the \f$\alpha\f$ shape parameter of van Genuchten's
         *        curve.
         */
        Scalar vgAlpha() const
        {
            return vgAlpha_;
        }

        /*!
         * \brief Set the \f$\alpha\f$ shape parameter of van Genuchten's
         *        curve.
         */
        void setVgAlpha(Scalar v)
        {
            vgAlpha_ = v;
        }

        /*!
         * \brief Return the \f$m\f$ shape parameter of van Genuchten's
         *        curve.
         */
        Scalar vgm() const
        {
            return vgm_;
        }

        /*!
         * \brief Set the \f$m\f$ shape parameter of van Genuchten's
         *        curve.
         *
         * The \f$n\f$ shape parameter is set to \f$n = \frac{1}{1 - m}\f$
         */
        void setVgm(Scalar m)
        {
            vgm_ = m; vgn_ = 1/(1 - vgm_);
        }

        /*!
         * \brief Return the \f$n\f$ shape parameter of van Genuchten's
         *        curve.
         */
        Scalar vgn() const
        {
            return vgn_;
        }

        /*!
         * \brief Set the \f$n\f$ shape parameter of van Genuchten's
         *        curve.
         *
         * The \f$n\f$ shape parameter is set to \f$m = 1 - \frac{1}{n}\f$
         */
        void setVgn(Scalar n)
        {
            vgn_ = n; vgm_ = 1 - 1/vgn_;
        }

        /*!
         * \brief Return the residual saturation.
         */
        Scalar satResidual(int phaseIdx) const
        {
            switch (phaseIdx)
            {
            case 0:
                return swr_;
                break;
            case 1:
                return snr_;
                break;
            case 2:
                return sgr_;
                break;
            }

            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
        }

        /*!
         * \brief Set all residual saturations.
         */
        void setResiduals(Dune::FieldVector<Scalar, 3> residualSaturation)
        {
            setSwr(residualSaturation[0]);
            setSnr(residualSaturation[1]);
            setSgr(residualSaturation[2]);
        }


        /*!
         * \brief Return the residual wetting saturation.
         */
        Scalar swr() const
        {
            return swr_;
        }

        /*!
         * \brief Set the residual wetting saturation.
         */
        void setSwr(Scalar input)
        {
            swr_ = input;
        }

        /*!
         * \brief Return the residual nonwetting saturation.
         */
        Scalar snr() const
        {
            return snr_;
        }

        /*!
         * \brief Set the residual nonwetting saturation.
         */
        void setSnr(Scalar input)
        {
            snr_ = input;
        }

        /*!
         * \brief Return the residual gas saturation.
         */
        Scalar sgr() const
        {
            return sgr_;
        }

        /*!
         * \brief Set the residual gas saturation.
         */
        void setSgr(Scalar input)
        {
            sgr_ = input;
        }

        Scalar swrx() const
        {
            return swrx_;
        }

        /*!
         * \brief Set the residual gas saturation.
         */
        void setSwrx(Scalar input)
        {
            swrx_ = input;
        }

        /*!
         * \brief defines the scaling parameters of capillary pressure between the phases (=1 for Gas-Water)
         */
        void setBetaNw(Scalar input)
        {
            betaNw_ = input;
        }

        void setBetaGn(Scalar input)
        {
            betaGn_ = input;
        }

        void setBetaGw(Scalar input)
        {
            betaGw_ = input;
        }

        /*!
         * \brief Return the values for the beta scaling parameters of capillary pressure between the phases
         */
        Scalar betaNw() const
        {
            return betaNw_;
        }

        Scalar betaGn() const
        {
            return betaGn_;
        }

        Scalar betaGw() const
        {
            return betaGw_;
        }

        bool krRegardsSnr() const
        {
            return krRegardsSnr_;
        }

        /*!
         * \brief defines if residual n-phase saturation should be regarded in its relative permeability.
         */
        void setKrRegardsSnr(bool input)
        {
            krRegardsSnr_ = input;
        }

    private:
        Scalar vgAlpha_;
        Scalar vgm_;
        Scalar vgn_;
        Scalar swr_;
        Scalar snr_;
        Scalar sgr_;
        Scalar swrx_;     /* (sw+sn)_r */

        Scalar betaNw_;
        Scalar betaGn_;
        Scalar betaGw_;

        Scalar TrappedSatN_;

        bool krRegardsSnr_ ;
    };

    ParkerVanGenZeroHyst3P(const Params& params)
    : params_(params)
    {}

    Scalar pcgw(const Scalar sw, const Scalar sn) const
    { return 0;}

    Scalar pcnw(const Scalar sw, const Scalar sn) const
    { return 0; }

    Scalar pcgn(const Scalar sw, const Scalar sn) const
    { return 0; }

    Scalar pcAlpha(const Scalar sw, const Scalar sn) const
    { return 1; }

    void updateTappedSn(const Scalar trappedSn)
    { params_.setTrappedSatN(trappedSn); }

    /*!
     * \brief The relative permeability for the wetting phase of
     *        the medium implied by van Genuchten's
     *        parameterization.
     *
     * The permeability of water in a 3p system equals the standard 2p description.
     * (see p61. in "Comparison of the Three-Phase Oil Relative Permeability Models"
     * MOJDEH  DELSHAD and GARY A. POPE, Transport in Porous Media 4 (1989), 59-83.)
     *
     * \param sw saturation saturation of the water phase.
     * \param sn saturation of the NAPL phase.
     */
    Scalar krw(const Scalar sw, const Scalar sn) const
    {
        //transformation to effective saturation
        const Scalar se = (sw - params_.swr()) / (1 - params_.TrappedSatN() - params_.swr());

        // regularization
        if (se > 1.0)
            return 1.;
        if (se < 0.0)
            return 0.;

        const Scalar r = 1. - std::pow(1 - std::pow(se, 1/params_.vgm()), params_.vgm());

        return std::sqrt(se)*r*r;
    };

    /*!
     * \brief The relative permeability for the nonwetting phase
     *        after the Model of Parker et al. (1987).
     *
     * See model 7 in "Comparison of the Three-Phase Oil Relative Permeability Models"
     * MOJDEH  DELSHAD and GARY A. POPE, Transport in Porous Media 4 (1989), 59-83.
     * or more comprehensive in
     * "Estimation of primary drainage three-phase relative permeability for organic
     * liquid transport in the vadose zone", Leonardo I. Oliveira, Avery H. Demond,
     * Journal of Contaminant Hydrology 66 (2003), 261-285
     *
     *
     * \param sw saturation saturation of the water phase.
     * \param sn saturation of the NAPL phase.
     */
    Scalar krn(const Scalar sw, const Scalar sn) const
    {
        Scalar swe = std::min((sw - params_.swr()) / (1 - params_.TrappedSatN()- params_.swr()), 1.);
        Scalar ste = std::min((sw +  sn - params_.swr()) / (1 - params_.swr()), 1.);

        // regularization
        if (swe <= 0.0)
            swe = 0.;
        if (ste <= 0.0)
            ste = 0.;
        if (ste - swe <= 0.0)
            return 0.;

        Scalar krn_;
        krn_ = std::pow(1 - std::pow(swe, 1/params_.vgm()), params_.vgm());
        krn_ -= std::pow(1 - std::pow(ste, 1/params_.vgm()), params_.vgm());
        krn_ *= krn_;

        if (params_.krRegardsSnr())
        {
            // regard Snr in the permeability of the n-phase, see Helmig1997
            const Scalar resIncluded = std::max(std::min((sn - params_.snr()/ (1-params_.swr())), 1.), 0.);
            krn_ *= std::sqrt(resIncluded );
        }

        else
            krn_ *= std::sqrt(sn / (1 - params_.swr()));   // Hint: (ste - swe) = sn / (1-Srw)

        return krn_;
    };

    /*!
     * \brief The relative permeability for the nonwetting phase
     *        of the medium implied by van Genuchten's
     *        parameterization.
     *
     * The permeability of gas in a 3p system equals the standard 2p description.
     * (see p61. in "Comparison of the Three-Phase Oil Relative Permeability Models"
     * MOJDEH  DELSHAD and GARY A. POPE, Transport in Porous Media 4 (1989), 59-83.)
     *
     * \param sw saturation saturation of the water phase.
     * \param sn saturation of the NAPL phase.
     */
    Scalar krg(const Scalar sw, const Scalar sn) const
    {
        const Scalar st = sw + sn;
        const Scalar se = std::min(((1-st) - params_.sgr()) / (1 - params_.sgr()), 1.);

        // regularization
        if (se > 1.0)
            return 0.0;
        if (se < 0.0)
            return 1.0;

        Scalar scalFact = 1.;

        if (st<=0.1)
        {
          scalFact = (st - params_.sgr())/(0.1 - params_.sgr());
          if (scalFact < 0.)
                scalFact = 0.;
        }

        return scalFact * std::pow(1 - se, 1.0/3.) * std::pow(1 - std::pow(se, 1/params_.vgm()), 2*params_.vgm());
    };

    /*!
     * \brief The relative permeability for a phase.
     * \param sw saturation saturation of the water phase.
     * \param sn saturation of the NAPL phase.
     * \param phase indicator, The saturation of all phases.
     */
    Scalar kr(const int phaseIdx, const Scalar sw, const Scalar sn) const
    {
        switch (phaseIdx)
        {
        case 0:
            return krw(sw, sn);
            break;
        case 1:
            return krn(sw, sn);
            break;
        case 2:
            return krg(sw, sn);
            break;
        }

        return 0;
    };

private:
    Params params_;
};

} // end namespace Dumux::FluidMatrix

#endif
