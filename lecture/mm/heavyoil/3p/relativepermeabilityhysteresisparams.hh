// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Specification of the material params for the van Genuchten
 *        capillary pressure model.
 *
 * In comparison to the 2p version, this parameter container also includes
 * the residual saturations, as their inclusion is very model-specific.
 */
#ifndef DUMUX_RELATIVEPERMHYST_PARAMS_3P_HH
#define DUMUX_RELATIVEPERMHYST_PARAMS_3P_HH

#include <dune/common/fvector.hh>

namespace Dumux {
/*!
 * \brief Reference implementation of a van Genuchten params
 */
template<class ScalarT>
class relativPermHyst3PParams
{
public:
    using Scalar = ScalarT;

    relativPermHyst3PParams()
    {
        betaGw_ = betaNw_ = betaGn_ = 1.;
    }

    relativPermHyst3PParams(Scalar vgAlpha, Scalar vgn, Scalar KdNAPL, Scalar rhoBulk, Dune::FieldVector<Scalar, 4> residualSaturation, Scalar betaNw = 1., Scalar betaGn = 1., Scalar betaGw = 1., bool regardSnr=false,
        Scalar Bw= 1., Scalar Bn= 1., Scalar Bg= 1.,bool firstTimeStepindex=true,
        Scalar redSwrn= 1., Scalar redSwrg= 1., Scalar redSnrw= 1., Scalar redSnrg= 1., Scalar redSgrw= 1., Scalar redSgrn= 1.,
        Scalar C1_wn= 1., Scalar C1_wg= 1., Scalar C1_ng= 1.,Scalar C2_wn= 1., Scalar C2_wg= 1., Scalar C2_ng= 1.,
        Scalar kr0_wn= 1., Scalar kr0_wg= 1., Scalar kr0_ng= 1., Scalar SwMax= 1., Scalar SnMax= 1., Scalar SgMax= 1., Scalar LandAw= 1., Scalar LandDw= 1., Scalar LandAn= 1., Scalar LandDn= 1, Scalar LandAg= 1., Scalar LandDg= 1.
        , Scalar LandBetaW= 0.7, Scalar LandBetaN= 0.3, Scalar LandBetaG= 0.9
        , Scalar TrappedSatWx= 1e-4, Scalar TrappedSatNx= 1e-4, Scalar TrappedSatGx= 1e-4
        , Scalar Swc= 1e-4, Scalar Snc= 1e-4, Scalar Sgc= 1e-4)
    {
        setVgAlpha(vgAlpha);
        setVgn(vgn);
        setSwr(residualSaturation[0]);
        setSnr(residualSaturation[1]);
        setSgr(residualSaturation[2]);
        setSwrx(residualSaturation[3]);
        setKrRegardsSnr(regardSnr);

        setFirstTimeStepindex(firstTimeStepindex);

        setKdNAPL(KdNAPL);
        setBetaNw(betaNw);
        setBetaGn(betaGn);
        setBetaGw(betaGw);
        setRhoBulk(rhoBulk);
//--------------------------------
        setBw(Bw);      //eq. 5, Parameter B depends on rock wettability and phase composition
        setBn(Bn);
        setBg(Bg);

        setredSwrn(redSwrn);    //S* r = Residual saturation corrected for compositional changes [L^3/L^3]
        setredSwrg(redSwrg);

        setredSnrw(redSnrw);
        setredSnrg(redSnrg);

        setredSgrw(redSgrw);
        setredSgrn(redSgrn);

        setC1_wn(C1_wn);
        setC1_wg(C1_wg);
        setC1_ng(C1_ng);

        setC2_wn(C2_wn);
        setC2_wg(C2_wg);
        setC2_ng(C2_ng);

        setkr0_wn(kr0_wn);
        setkr0_wg(kr0_wg);
        setkr0_ng(kr0_ng);

        setSwMax(SwMax);
        setSnMax(SnMax);
        setSgMax(SgMax);

        setLandAw(LandAw);  //eq. 2
        setLandDw(LandDw);  //eq. 2

        setLandAn(LandAn);  //eq. 2
        setLandDn(LandDn);  //eq. 2

        setLandAg(LandAg);  //eq. 2
        setLandDg(LandDg);  //eq. 2

        setLandBetaW(LandBetaW);    //eq. 2
        setLandBetaN(LandBetaN);    //eq. 2
        setLandBetaG(LandBetaG);    //eq. 2

        setTrappedSatWx(TrappedSatWx);  //eq. 2
        setTrappedSatNx(TrappedSatNx);  //eq. 2
        setTrappedSatGx(TrappedSatGx);  //eq. 2

        setSwc(Swc);
        setSnc(Swc);
        setSgc(Swc);
    };

    Scalar Swc() const
    {
        return Swc_;
    }

    void setSwc(Scalar v)
    {
        Swc_ = v;
    }

    Scalar Snc() const
    {
        return Snc_;
    }

    void setSnc(Scalar v)
    {
        Snc_ = v;
    }

    Scalar Sgc() const
    {
        return Sgc_;
    }

    void setSgc(Scalar v)
    {
        Sgc_ = v;
    }

    Scalar TrappedSatWx() const
    {
        return TrappedSatWx_;
    }

    void setTrappedSatWx(Scalar v)
    {
        TrappedSatWx_ = v;
    }

    Scalar TrappedSatNx() const
    {
        return TrappedSatNx_;
    }

    void setTrappedSatNx(Scalar v)
    {
        TrappedSatNx_ = v;
    }

    Scalar TrappedSatGx() const
    {
        return TrappedSatGx_;
    }

    void setTrappedSatGx(Scalar v)
    {
        TrappedSatGx_ = v;
    }

    Scalar Bw() const
    {
        return Bw_;
    }

    void setBw(Scalar v)
    {
        Bw_ = v;
    }

    Scalar Bn() const
    {
        return Bn_;
    }

    void setBn(Scalar v)
    {
        Bn_ = v;
    }

    Scalar Bg() const
    {
        return Bg_;
    }

    void setBg(Scalar v)
    {
        Bg_ = v;
    }

    Scalar redSwrn() const  //1
    {
        return redSwrn_;
    }

    void setredSwrn(Scalar v)
    {
        redSwrn_ = v;
    }

    Scalar redSwrg() const  //2
    {
        return redSwrg_;
    }
    void setredSwrg(Scalar v)
    {
        redSwrg_ = v;
    }
    Scalar redSnrw() const  //3
    {
        return redSnrw_;
    }
    void setredSnrw(Scalar v)
    {
        redSnrw_ = v;
    }
    Scalar redSnrg() const  //4
    {
        return redSnrg_;
    }
    void setredSnrg(Scalar v)
    {
        redSnrg_ = v;
    }
    Scalar redSgrw() const  //5
    {
        return redSgrw_;
    }
    void setredSgrw(Scalar v)
    {
        redSgrw_ = v;
    }
    Scalar redSgrn() const  //6
    {
        return redSgrn_;
    }
    void setredSgrn(Scalar v)
    {
        redSgrn_ = v;
    }

    Scalar C1_wn() const    //1
    {
        return C1_wn_;

    }
    void setC1_wn(Scalar v)
    {
        C1_wn_ = v;
    }

    Scalar C1_wg() const    //1
    {
        return C1_wg_;
    }
    void setC1_wg(Scalar v)
    {
        C1_wg_ = v;
    }

    Scalar C1_ng() const    //1
    {
        return C1_ng_;
    }
    void setC1_ng(Scalar v)
    {
        C1_ng_ = v;
    }

    Scalar C2_wn() const    //1
    {
        return C2_wn_;
    }
    void setC2_wn(Scalar v)
    {
        C2_wn_ = v;
    }

    Scalar C2_wg() const    //1
    {
        return C2_wg_;
    }
    void setC2_wg(Scalar v)
    {
        C2_wg_ = v;
    }

    Scalar C2_ng() const    //1
    {
        return C2_ng_;
    }
    void setC2_ng(Scalar v)
    {
        C2_ng_ = v;
    }

    Scalar kr0_wn() const   //1
    {
        return kr0_wn_;
    }
    void setkr0_wn(Scalar v)
    {
        kr0_wn_ = v;
    }

    Scalar kr0_wg() const   //1
    {
        return kr0_wg_;
    }
    void setkr0_wg(Scalar v)
    {
        kr0_wg_ = v;
    }

    Scalar kr0_ng() const   //1
    {
        return kr0_ng_;

    }
    void setkr0_ng(Scalar v)
    {
        kr0_ng_ = v;
    }

    Scalar SwMax() const
    {
        return SwMax_;
    }
    void setSwMax(Scalar v)
    {
        SwMax_ = v;
    }

    Scalar SnMax() const
    {
        return SnMax_;
    }
    void setSnMax(Scalar v)
    {
        SnMax_ = v;
    }

    Scalar SgMax() const
    {
        return SgMax_;
    }
    void setSgMax(Scalar v)
    {
        SgMax_ = v;
    }

    Scalar LandAw() const
    {
        return LandAw_;
    }
    void setLandAw(Scalar v)
    {
        LandAw_ = v;
    }

    Scalar LandDw() const
    {
        return LandDw_;
    }
    void setLandDw(Scalar v)
    {
        LandDw_ = v;
    }

    Scalar LandAn() const
    {
        return LandAn_;
    }
    void setLandAn(Scalar v)
    {
        LandAn_ = v;
    }

    Scalar LandDn() const
    {
        return LandDn_;
    }
    void setLandDn(Scalar v)
    {
        LandDn_ = v;
    }

    Scalar LandAg() const
    {
        return LandAg_;
    }
    void setLandAg(Scalar v)
    {
        LandAg_ = v;
    }

    Scalar LandDg() const
    {
        return LandDg_;
    }
    void setLandDg(Scalar v)
    {
        LandDg_ = v;
    }

    Scalar LandBetaW() const
    {
        return LandBetaW_;
    }
    void setLandBetaW(Scalar v)
    {
        LandBetaW_ = v;
    }

    Scalar LandBetaN() const
    {
        return LandBetaN_;
    }
    void setLandBetaN(Scalar v)
    {
        LandBetaN_ = v;
    }

    Scalar LandBetaG() const
    {
        return LandBetaG_;
    }
    void setLandBetaG(Scalar v)
    {
        LandBetaG_ = v;
    }

    /*!
     * \brief Return the \f$\alpha\f$ shape parameter of van Genuchten's
     *        curve.
     */
    Scalar vgAlpha() const
    {
        return vgAlpha_;
    }

    /*!
     * \brief Set the \f$\alpha\f$ shape parameter of van Genuchten's
     *        curve.
     */
    void setVgAlpha(Scalar v)
    {
        vgAlpha_ = v;
    }

    /*!
     * \brief Return the \f$m\f$ shape parameter of van Genuchten's
     *        curve.
     */
    Scalar vgm() const
    {
        return vgm_;
    }

    /*!
     * \brief Set the \f$m\f$ shape parameter of van Genuchten's
     *        curve.
     *
     * The \f$n\f$ shape parameter is set to \f$n = \frac{1}{1 - m}\f$
     */
    void setVgm(Scalar m)
    {
        vgm_ = m; vgn_ = 1/(1 - vgm_);
    }

    /*!
     * \brief Return the \f$n\f$ shape parameter of van Genuchten's
     *        curve.
     */
    Scalar vgn() const
    {
        return vgn_;
    }

    /*!
     * \brief Set the \f$n\f$ shape parameter of van Genuchten's
     *        curve.
     *
     * The \f$n\f$ shape parameter is set to \f$m = 1 - \frac{1}{n}\f$
     */
    void setVgn(Scalar n)
    {
        vgn_ = n; vgm_ = 1 - 1/vgn_;
    }

    /*!
     * \brief Return the residual saturation.
     */
    Scalar satResidual(int phaseIdx) const
    {
        switch (phaseIdx)
        {
        case 0:
            return swr_;
            break;
        case 1:
            return snr_;
            break;
        case 2:
            return sgr_;
            break;
        }

        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Set all residual saturations.
     */
    void setResiduals(Dune::FieldVector<Scalar, 3> residualSaturation)
    {
        setSwr(residualSaturation[0]);
        setSnr(residualSaturation[1]);
        setSgr(residualSaturation[2]);
    }


    /*!
     * \brief Return the residual wetting saturation.
     */
    Scalar swr() const
    {
        return swr_;
    }

    /*!
     * \brief Set the residual wetting saturation.
     */
    void setSwr(Scalar input)
    {
        swr_ = input;
    }

    /*!
     * \brief Return the residual nonwetting saturation.
     */
    Scalar snr() const
    {
        return snr_;
    }

    /*!
     * \brief Set the residual nonwetting saturation.
     */
    void setSnr(Scalar input)
    {
        snr_ = input;
    }

    /*!
     * \brief Return the residual gas saturation.
     */
    Scalar sgr() const
    {
        return sgr_;
    }

    /*!
     * \brief Set the residual gas saturation.
     */
    void setSgr(Scalar input)
    {
        sgr_ = input;
    }

    Scalar swrx() const
    {
        return swrx_;
    }

    /*!
     * \brief Set the residual gas saturation.
     */
    void setSwrx(Scalar input)
    {
        swrx_ = input;
    }

    /*!
     * \brief defines the scaling parameters of capillary pressure between the phases (=1 for Gas-Water)
     */
    void setBetaNw(Scalar input)
    {
        betaNw_ = input;
    }

    void setBetaGn(Scalar input)
    {
        betaGn_ = input;
    }

    void setBetaGw(Scalar input)
    {
        betaGw_ = input;
    }

    /*!
     * \brief Return the values for the beta scaling parameters of capillary pressure between the phases
     */
    Scalar betaNw() const
    {
        return betaNw_;
    }

    Scalar betaGn() const
    {
        return betaGn_;
    }

    Scalar betaGw() const
    {
        return betaGw_;
    }

    /*!
     * \brief defines if residual n-phase saturation should be regarded in its relative permeability.
     */
    void setKrRegardsSnr(bool input)
    {
        krRegardsSnr_ = input;
    }

    /*!
     * \brief Calls if residual n-phase saturation should be regarded in its relative permeability.
     */
    bool krRegardsSnr() const
    {
        return krRegardsSnr_;
    }

//----------------------------------------
    void setFirstTimeStepindex(bool input)
    {
        firstTimeStepindex_ = input;
    }

    bool firstTimeStepindex() const
    {
        return firstTimeStepindex_;
    }
//----------------------------------------

    /*!
     * \brief Return the bulk density of the porous medium
     */
    Scalar rhoBulk() const
    {
        return rhoBulk_;
    }

    /*!
     * \brief Set the bulk density of the porous medium
     */
    void setRhoBulk(Scalar input)
    {
        rhoBulk_ = input;
    }

    /*!
     * \brief Return the adsorption coefficient
     */
    Scalar KdNAPL() const
    {
        return KdNAPL_;
    }

    /*!
     * \brief Set the adsorption coefficient
     */
    void setKdNAPL(Scalar input)
    {
        KdNAPL_ = input;
    }


private:
    Scalar vgAlpha_;
    Scalar vgm_;
    Scalar vgn_;
    Scalar swr_;
    Scalar snr_;
    Scalar sgr_;
    Scalar swrx_;     /* (sw+sn)_r */

    Scalar KdNAPL_;
    Scalar rhoBulk_;

    Scalar betaNw_;
    Scalar betaGn_;
    Scalar betaGw_;

    bool krRegardsSnr_ ;
    bool firstTimeStepindex_;

    Scalar Bw_;
    Scalar Bn_;
    Scalar Bg_;
    Scalar redSwrn_;
    Scalar redSwrg_;
    Scalar redSnrw_;
    Scalar redSnrg_;
    Scalar redSgrw_;
    Scalar redSgrn_;
    Scalar C1_wn_;
    Scalar C1_wg_;
    Scalar C1_ng_;
    Scalar C2_wn_;
    Scalar C2_wg_;
    Scalar C2_ng_;
    Scalar kr0_wn_;
    Scalar kr0_wg_;
    Scalar kr0_ng_;
    Scalar SwMax_;
    Scalar SnMax_;
    Scalar SgMax_;
    Scalar LandAw_;
    Scalar LandDw_;
    Scalar LandAn_;
    Scalar LandDn_;
    Scalar LandAg_;
    Scalar LandDg_;

    Scalar LandBetaW_;
    Scalar LandBetaN_;
    Scalar LandBetaG_;

    Scalar TrappedSatWx_;
    Scalar TrappedSatNx_;
    Scalar TrappedSatGx_;

    Scalar Swc_;
    Scalar Snc_;
    Scalar Sgc_;

};

} // end namespace Dumux

#endif
