# SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

add_input_file_links()

dumux_add_test(NAME buckleyleverettexercise
              SOURCES buckleyleverettexercise.cc
              COMMAND ${DUMUX_RUNTEST}
              CMD_ARGS --script fuzzy
                       --files ${CMAKE_SOURCE_DIR}/lecture/references/buckleyleverett-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/buckleyleverett-00062.vtu
                       --command ./buckleyleverettexercise)
