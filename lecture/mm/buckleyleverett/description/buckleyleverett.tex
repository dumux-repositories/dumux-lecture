% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\documentclass[11pt,a4paper]{article}
%\usepackage{german}
\usepackage{pslatex}
\usepackage[english]{babel}
\usepackage{latexsym}
\usepackage{fancyvrb}
\usepackage{fancyheadings}
%\usepackage{psfig}
%\usepackage{epsfig}
%\usepackage{epsf}
\usepackage[dvips]{color}
\usepackage{hyperref}
%\usepackage{makeidx}
\usepackage{listings}
\usepackage{lscape}
\usepackage[pdftex]{graphicx}

% ---- Einstellungen ----------------------------------------
\textwidth 16.5cm
\textheight 23.5cm
\topmargin 0.0pt
\oddsidemargin 0.0pt
\evensidemargin 0.0pt
\setlength{\parindent}{10pt}
\setlength{\unitlength}{1cm}
\selectlanguage{english}
\newcommand{\Pictpath}{pics}
\pagestyle{fancy}

%---- pagestyle ---- start -------------------------------------
\lhead[\fancyplain{}{\thepage}] %Header links,  gerade Seitenzahl
%      {\fancyplain{}{EGW Short Course, March 2002}}
      {\fancyplain{}{MM Exercise}}
\rhead[\fancyplain{}{Multiphase Modeling}]
      {\fancyplain{}{\thepage}} %Header rechts, ungerade Seitenzahl
\cfoot{}
%---- pagestyle ---- ende --------------------------------------


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\sloppy                  % groessere Wortabstaende

{\Large
\begin{center}
{\bfseries MM Exercise\\
           Buckley-Leverett Problem\\ 
%           Multiphase Flow and Transport in Porous Media\\ 
           - Computer Exercises -}\\ 
\end{center}
}

%*************************************
\section*{A quasilinear hyperbolic
transport equation -- The {\it Buckley--Leverett} equation}
\label{Buckley}
 
The {\it Buckley--Leverett} equation for
a one--dimensional, two--phase flow system shows the properties of a
quasilinear
hyperbolic differential equation. Provided that we have
two immiscible fluids (for example, oil = non--wetting phase (n) and water =
wetting phase (w)) and that we can neglect gravitational and
capillary pressure effects, we can combine the two--phase flow equations
in order to get  one equation
\begin{eqnarray}
\phi \, \frac{\partial S_w}{\partial t} + \mbox{\bf v}_t \, \frac{df_w}
{\underbrace{dS_w}_{f_w'}}
\, \frac{\partial S_w}{\partial x} = 0 \; ,
\end{eqnarray}
where $\mbox{\bf v}_t$ represents the total velocity with
$\mbox{\bf v}_t= \mbox{\bf v}_w + \mbox{\bf v}_n$ and
\begin{eqnarray}
f_w =
\frac{\frac{k_{rw}(S_w)}{\mu_w}}{\frac{k_{rw}(S_w)}{\mu_w} +
\frac{k_{rn}(S_n)}{\mu_n}}
\end{eqnarray}
is the \index{fractional flow function} {\it fractional flow} function. We assume a constant porosity
($\phi=const$). Assuming incompressibility the total velocity $\mbox{\bf v}_t$ is constant.
\par{}
 The name `fractional flow function' becomes clear, if we write the fraction of the water flow rate, $Q_w$, and the total inflow rate $Q_t=Q_w+Q_n$:
\begin{eqnarray}
\frac{Q_w}{Q_t}=\frac{Q_w}{Q_w+Q_n}=\frac{K\frac{k_{rw}}{\mu_w}\nabla p_w  }{K\frac{k_{rw}}{\mu_w}\nabla p_w +K\frac{k_{ro}}{\mu_o}\nabla p_o   }=\frac{\frac{k_{rw}(S_w)}{\mu_w}}{\frac{k_{rw}(S_w)}{\mu_w} +
\frac{k_{rn}(S_n)}{\mu_n}}=f_w,
\end{eqnarray}
where we assumed Darcy's law, and that capillary effects can be neglected, i.e. $p_c = 0$, and $p_w=p_o$.
 
% \paragraph{\index{Rankine--Hugoniot jump
% condition} {\it Rankine--Hugoniot} jump condition}
%  
% Dimensionless specific
% front velocity
% \begin{eqnarray} \label{spezfront}
% \tilde{\mbox{\bf v}}_{\Delta S_{w}} = \frac{f_w (S_w^u) - f_w (S_w^d)}{S_w^u - S_w^d}
% = \frac{\Delta f_w}{\Delta S_w} \hspace{0.2cm}.
% \end{eqnarray}
% From equation (\ref{spezfront}), it becomes clear that the jump
% $\Delta S_w = S_w^u - S_w^d$ in
% saturation at the shock
% is associated with the front velocity dependent on the fractional flow function.
% Thus, the continuity condition at the discontinuity is only satisfied if
% the specific front velocity corresponds to the physical process.
% The {\it Rankine--Hugoniot} condition does not answer the question whether
% a physically unique solution at the shock front exists, i.e.~whether
% the form of the front corresponds to the physical solution. Therefore,
% we have to formulate an additional condition which guarantees that, from
% the many possible solutions, the physically unique solution can be found.
% \paragraph{Entropy condition}
% The entropy condition is needed as a result of neglecting the effects of
% physical dispersion in the {\it Buckley--Leverett} equation.
%  
% The shock front
%  must always be self--sharpening
% (${\mbox{\bf v}_u} \ge {\mbox{\bf v}_d}$).
% Otherwise, the entropy condition is not satisfied.
%  
% With
% characteristics flowing into the shock from both the upstream and downstream
% sides, the shock continues to propagate even in the presence of dispersion.
% If characteristics flow away from the shock,
% the shock will dissipate under
% the influence of dispersion.
% Therefore, this shock is not the relevant physical
% solution.
% Accordingly, characteristics upstream must have a faster velocity than the shock
% and characteristics downstream must have a slower velocity than the shock.
 
The analytical solution of
{\it Buckley and Leverett (1942)},
which describes the instationary displacement of oil by water in
a one--dimensional, horizontal system,
is a standard method for the verification of multiphase flow processes
{\it without capillary pressure effects}. It has a fairly simple form for the time dependent distance-saturation profile:
\begin{eqnarray}
x(S_w,t)= \frac{Q_t}{A\phi}\frac{df_w}{dS_w} t.
\end{eqnarray}
Observe, that for an s-shaped $f_w$ this must result in a shock front!

\begin{figure}[h]\centering
\includegraphics[width=0.7\linewidth]{\Pictpath/ebuckley.pdf}
    %\centerline{\epsfig{file=\Pictpath/EPS/ebuckley.eps,width=15cm}}
    \caption{{\it Buckley--Leverett} problem}
    \protect\label{skizzeblp}
\end{figure}



\begin{table}[h]
\centering
%\begin{tabular}{c|c|c|c|c|c|c|c|c}
%parameter & $S_{wr}$ & $S_{nr}$ & $K$ & $\phi$ & $\rho_{\alpha}$ & $\mu_{\alpha}$ & $S_{w,init}$ & $S_w^D$\\
\begin{tabular}{c|lll}
parameter & $S_{w,init}$& $S_w^{\mathrm{Dirichlet}}$ & pressure (altern.: inflow rate)\\
\hline
%values & 0.2 & 0.2 & $10^{-7}$ & 0.2 & 1000 & 1000 & 0.2 & 0.8
values & 0.2 & 0.8 & $2\cdot 10^5$ [Pa]
\end{tabular}
\caption{Parameter values for a classical \textit{Buckley-Leverett} problem.}
\label{tab:param}
\end{table}

%\clearpage

\section{Purpose of this Exercise}

The aim of this exercise is to determine
\begin{itemize}
\item the influence of numerical diffusion on the front,
\item the influence of the linearity (non-linearity) of the system on the front behaviour,
\item the influence of the choice of the residual saturations in the relative
permeability functions on the front behaviour,
\item the impact of fluid properties (viscosity and density) on the front.
\end{itemize}

In the program a one-dimensional tube with Dirichlet on the left and Neumann boundary conditions on the right is simulated. The tube is 300m long.
%We will consider a twodimensional domain with parameters varying only in one direction. The length of our domain is equal to 300~m and the height equals 75~m.

\section{Questions}
\begin{itemize}
\item What is the mathematical type of the Buckley-Leverett equation and what does that
mean physically?
\item What is the influence of the linearity (non-linearity) on the front?\\
      To investigate this,
  \begin{itemize}
%    \item compare a linear relative-permeability-saturation relation to a non-linear one (Brooks-Corey).
    \item vary the pore size distribuion parameter $\lambda$ of the Brooks-Corey relation.
    \item vary the fluid viscosities of the different phases.
  \end{itemize}
\item What is the influence of viscosity on the front? Explain!
\item What is the influence of S$_{\mathrm{wr}}$ and S$_{\mathrm{nr}}$ on the front?
\item What is the influence of density on the front?
\item Why don't we see a sharp front in our numerical model? Try to find out
the influence of the grid size!

%\item If we had gas as a third fluid in our system which fluid would replace which and how would the fronts look like?
\end{itemize}
\clearpage

\begin{figure}[h]
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{\Pictpath/fw.pdf}
\end{minipage}
\begin{minipage}[]{0.1\linewidth}
\end{minipage}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{\Pictpath/kr.pdf}
\end{minipage}
\caption{Fractional flow function and relative permeability for different cases with varying residual saturations and viscosities. Cases are given in the left figure.}
    \protect\label{skizzeblp2}
\end{figure}

%\subsection{Properties and parameters -- buckleyleverettexercise.input}
%\label{prop_paramI}
%{\small
%\lstset{numbers=left}
%\begin{lstlisting}
%tEnd = 5e7                           # end time of the simulation [s]
%dtInitial =  1e3                     # initial time step for the simulation [s]
%
%[SpatialParameters]
%
%permeability = 1e-7                  # intrinsic permeability of the porous medium [m^2]
%porosity = 0.2                       # porosity of the porous medium [-]
%
%BrooksCoreyLambda = 2.0              # pore size distribution parameter for the Brooks-Corey 
				       %capillary pressure - saturation relationship [-]
%BrooksCoreyEntryPressure = 0         # entry pressure for the Brooks-Corey 
			               %capillary pressure - saturation relationship [Pa]
%
%ResidualSaturationWetting = 0.2      # residual saturation of the wetting phase [-]
%ResidualSaturationNonwetting = 0.2   # residual saturation of the nonwetting phase [-]
%
%[Fluid]
%
%densityW = 1e3                       # density of the wetting phase [kg/m^3]
%densityNW = 1e3                      # density of the nonwetting phase [kg/m^3]
%viscosityW = 1e-3                    # dynamic viscosity of the wetting phase [kg/(ms)]
%viscosityNW = 1e-3                   # dynamic viscosity of the nonwetting phase [kg/(ms)]
%
%[Grid]
%numberOfCellsX= 10                   # grid resolution in x direction, max 200
%\end{lstlisting}}

%\newpage
%\input{buckleyleverett_solution}

\end{document}
