// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Properties of pure water \f$H_2O\f$.
 */
#ifndef DUMUX_PSEUDOH2O_HH
#define DUMUX_PSEUDOH2O_HH


#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
/*!
 * \brief Rough estimate for testing purposes of some water.
 */
template <class ScalarT>
class PseudoH2O : public Components::Base<ScalarT, PseudoH2O<ScalarT> >
                , public Components::Liquid<ScalarT, PseudoH2O<ScalarT> >
{
public:
    using Scalar = ScalarT;
    /*!
     * \brief A human readable name for the water.
     */
    static std::string name()
    {
        return "H2O";
    }

    /*!
     * \brief Rough estimate of the density of oil [kg/m^3].
     * \param temperature DOC ME!
     * \param pressure DOC ME!
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        return density_;
    }

    /*!
     * \brief Rough estimate of the viscosity of oil kg/(ms).
     * \param temperature DOC ME!
     * \param pressure DOC ME!
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    {
        return viscosity_;
    }

    /*!
     * \brief DOC ME!
     * \param viscosity DOC ME!
     */
    static void setViscosity(Scalar viscosity)
    {
        viscosity_ =  viscosity;
    }

    /*!
     * \brief DOC ME!
     * \param density DOC ME!
     */
    static void setDensity(Scalar density)
    {
        density_ =  density;
    }

private:
    static Scalar viscosity_;
    static Scalar density_;

};

template <class ScalarT>
typename PseudoH2O<ScalarT>::Scalar PseudoH2O<ScalarT>::viscosity_ = 0.001;

template <class ScalarT>
typename PseudoH2O<ScalarT>::Scalar PseudoH2O<ScalarT>::density_ = 1000;

} // end namepace Dumux

#endif
