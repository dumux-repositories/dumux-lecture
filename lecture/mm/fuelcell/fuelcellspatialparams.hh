// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the fuel cell
 *        problem which uses the (non-)isothermal 2pnc box model.
 */
#ifndef DUMUX_FUELCELL_LECTURE_SPATIAL_PARAMS_HH
#define DUMUX_FUELCELL_LECTURE_SPATIAL_PARAMS_HH

#include <dumux/common/properties.hh>

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include "./material/acosta.hh"
#include "./material/thermalconductivityconstant.hh"

namespace Dumux {

// Forward declaration
template<class TypeTag>
class FuelCellLectureSpatialParams;

namespace Properties {

// The spatial parameters TypeTag
namespace TTag {
struct FuelCellLectureSpatialParams {};
}

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::FuelCellLectureSpatialParams> { using type = FuelCellLectureSpatialParams<TypeTag>; };

// Set the constant thermal conductivity law as in Acosta paper
#if !ISOTHERMAL
template<class TypeTag>
struct ThermalConductivityModel<TypeTag, TTag::FuelCellLectureSpatialParams> { using type = Dumux::ThermalConductivityConstant<GetPropType<TypeTag, Properties::Scalar>>; };
#endif

} // end namespace Properties

/*!
 * \brief Definition of the spatial parameters for the FuelCell
 *        problem which uses the (non-)isothermal 2pnc box model
 */
template<class TypeTag>
class FuelCellLectureSpatialParams: public FVPorousMediumFlowSpatialParamsMP<GetPropType<TypeTag, Properties::GridGeometry>,
                                    GetPropType<TypeTag, Properties::Scalar>,
                                    FuelCellLectureSpatialParams<TypeTag>>
{
    using ThisType = FuelCellLectureSpatialParams<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<FVGridGeometry, Scalar, ThisType>;
    using GridView = typename FVGridGeometry::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using PcKrSwCurve = FluidMatrix::AcostaPcSwDefault<Scalar>;
public:
    using PermeabilityType = Scalar;

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    FuelCellLectureSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    , K_(0)
    {
        // intrinsic permeabilities
        K_ = 5.2e-11; // Acosta absolute permeability diffusion layer
        // porosities
        porosity_ = 0.78; // Acosta porosity diffusion layer
        // thermalconductivity
        lambdaSolid_ = 15.6; // [W/(m*K)] Acosta thermal conductivity used in capillary pressure-saturation

        typename PcKrSwCurve::BasicParams params(-1168.75, 8.5, -0.2, -700, 0.0);
        typename PcKrSwCurve::EffToAbsParams effToAbsParams(0.05, 0.05);
        pcKrSwCurve_ = std::make_unique<PcKrSwCurve>(params, effToAbsParams);

    }

    ~FuelCellLectureSpatialParams()
    {}

    /*!
     * \brief Returns the hydraulic conductivity \f$[m^2]\f$.
     *
     * \param globalPos The global position
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return K_; }

    /*!
     * \brief Defines the porosity \f$[-]\f$ of the spatial parameters.
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        static Scalar reactionLayerWidth = getParam<Scalar>("Problem.ReactionLayerWidth");

        if (globalPos[0] < reactionLayerWidth + eps_)
            return 0.07;

        else
            return porosity_;
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return The wetting phase index
     * \param globalPos The global position
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law which depends on the position.
     *
     * \param globalPos The global position
     */
    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    { return makeFluidMatrixInteraction(*pcKrSwCurve_); }

    /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    template<class ElementSolution>
    Scalar solidHeatCapacity(const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const int scvIdx) const
    { return 710; }// specific heat capacity of diffusion layer Acosta [J / (kg K)]

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    template<class ElementSolution>
    Scalar solidDensity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    { return 1430; } // density of ELAT [kg/m^3] Wöhr

    /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the solid.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidThermalConductivity(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const SubControlVolume& scv) const
    { return lambdaSolid_; }

private:
    Scalar K_;
    Scalar porosity_;
    Scalar eps_ = 1e-6;
    std::unique_ptr<const PcKrSwCurve> pcKrSwCurve_;
    Scalar lambdaSolid_;
};

} // end namespace Dumux

#endif
