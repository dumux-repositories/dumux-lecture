// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Relation for the effective thermal conductivity.
 */
#ifndef DUMUX_LECTURE_FUELCELL_THERMALCONDUCTIVITY_CONSTANT_HH
#define DUMUX_LECTURE_FUELCELL_THERMALCONDUCTIVITY_CONSTANT_HH

namespace Dumux {

/*!
 * \brief A constant dummy effective thermal conductivity law
 */
template<class Scalar>
class ThermalConductivityConstant
{
public:
    //! effective thermal conductivity \f$[W/(m K)]\f$
    template<class VolumeVariables>
    static Scalar effectiveThermalConductivity(const VolumeVariables& volVars)
    { return volVars.solidThermalConductivity(); }
};

} // end namespace Dumumx
#endif
