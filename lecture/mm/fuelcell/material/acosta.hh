// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief   Implementation of the capillary pressure and
 *          water phase saturation according to Acosta (2006) \cite A3:acosta:2006 .
 */
#ifndef ACOSTA_HH
#define ACOSTA_HH

#include <algorithm>
#include <cmath>

#include <dumux/common/parameters.hh>
#include <dumux/common/spline.hh>
#include <dumux/common/optionalscalar.hh>
#include <dumux/material/fluidmatrixinteractions/2p/materiallaw.hh>

namespace Dumux::FluidMatrix {

/*!
 *
 * \brief Implementation of the Acosta capillary pressure <->
 *        saturation relation. This class bundles the "raw" curves
 *        as static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 * For general info: EffToAbsLaw
 *
 * \see AcostaParams
 */
class AcostaPcSw
{
public:
    template<class Scalar>
    struct Params
    {
        Params(Scalar acA, Scalar acB, Scalar acC, Scalar acD, Scalar acE)
        : acA_(acA)
        , acB_(acB)
        , acC_(acC)
        , acD_(acD)
        , acE_(acE)
        {}

        Scalar acA() const { return acA_; }
        void setAcA(Scalar a) { acA_ = a; }

        Scalar acB() const { return acB_; }
        void setAcB(Scalar b) { acB_ = b; }

        Scalar acC() const { return acC_; }
        void setAcC(Scalar c) { acC_ = c; }

        Scalar acD() const { return acD_; }
        void setAcD(Scalar d) { acD_ = d; }

        Scalar acE() const { return acE_; }
        void setAcE(Scalar e) { acE_ = e; }

        bool operator== (const Params& p) const
        {
            return Dune::FloatCmp::eq(acA(), p.acA(), 1e-6)
                && Dune::FloatCmp::eq(acB(), p.acB(), 1e-6)
                && Dune::FloatCmp::eq(acC(), p.acC(), 1e-6)
                && Dune::FloatCmp::eq(acD(), p.acD(), 1e-6)
                && Dune::FloatCmp::eq(acE(), p.acE(), 1e-6);
        }

    private:
        Scalar acA_, acB_, acC_, acD_, acE_;
    };

    /*!
     * \brief Construct from a subgroup from the global parameter tree
     * \note This will give you nice error messages if a mandatory parameter is missing
     */
    template<class Scalar = double>
    static Params<Scalar> makeParams(const std::string& paramGroup)
    {
        const auto acA = getParamFromGroup<Scalar>(paramGroup, "AcA");
        const auto acB = getParamFromGroup<Scalar>(paramGroup, "AcB");
        const auto acC = getParamFromGroup<Scalar>(paramGroup, "AcC");
        const auto acD = getParamFromGroup<Scalar>(paramGroup, "AcD");
        const auto acE = getParamFromGroup<Scalar>(paramGroup, "AcE");
        return {acA, acB, acC, acD, acE};
    }
    /*!
     * \brief The capillary pressure-saturation curve according to Acosta.
     *
     * Acosta's empirical capillary pressure <-> saturation
     * function is given by
     * \f[
     p_c = A*exp(B*Sw+C)+D*(1-Sw)+E/Sw
     \f]
     * Acosta et al equation 28
     * \param swe       effective water phase saturation
     *
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    template<class Scalar>
    static Scalar pc(Scalar swe, const Params<Scalar>& params)
    {
        assert(0 <= swe && swe <= 1);
        Scalar pc = exp(params.acB() * swe + params.acC());
        pc *= params.acA();
        pc += params.acD() * (1.0 - swe);
        pc += params.acE() / swe;
        return pc;
    }


    /*!
     * \brief The saturation-capillary pressure curve according to Acosta.
     *
     * This is the inverse of the capillary pressure-saturation curve:
     * \f[ does not exist yet \f]
     *
     * \param pc        Capillary pressure
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     * \return          The effective saturation of the wetting phase
     */
    template<class Scalar>
    static Scalar sw(Scalar pc, const Params<Scalar>& params)
    {
        DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::sw(params, pc)");
    }

    /*!
     * \brief The partial derivative of the capillary
     *        pressure w.r.t. the effective saturation according to Acosta.
     *
     * This is equivalent to
     * \f[ does not exist yet \f]
     *
     * \param swe       Effective saturation of the wetting phase \f$\overline{S}_w\f$
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
    */
    template<class Scalar>
    static Scalar dpc_dsw(Scalar swe, const Params<Scalar>& params)
    {
        assert(0 <= swe && swe <= 1);

        Scalar dpc_dsw = exp(params.acB() * swe + params.acC());
        dpc_dsw *= params.acA();
        dpc_dsw *= params.acB();
        dpc_dsw -= params.acD();
        dpc_dsw -= params.acE() / (swe*swe);
        return dpc_dsw;
    }

    /*!
     * \brief The partial derivative of the effective
     *        saturation to the capillary pressure according to Acosta.
     *
     *        function does not exist yet!
     *
     * \param pc        Capillary pressure \f$p_C\f$
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    template<class Scalar>
    static Scalar dsw_dpc(Scalar pc, const Params<Scalar>& params)
    {
        DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::dsw_dpc(params, pc)");
    }

    /*!
     * \brief The relative permeability for the wetting phase of
     *        the medium implied by Acosta's
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.     */
    template<class Scalar>
    static Scalar krw(Scalar swe, const Params<Scalar>& params)
    {
        assert(0 <= swe && swe <= 1);
        Scalar krw;
        //imbibition
        if(!params.acE())
        {
            Scalar int0Sw = evaluateIntegral_(intImb0Sw_, swe);
            krw = swe * swe *(int0Sw / intImb01_);
        }
        //drainage
        else
        {
            Scalar int0Sw = evaluateIntegral_(intDra0Sw_, swe);
            krw = swe * swe *(int0Sw / intDra01_);
        }
        assert(0 <= krw && krw <= 1);

        return(krw);

    };

    /*!
     * \brief The derivative of the relative permeability for the
     *        wetting phase in regard to the wetting saturation of the
     *        medium implied by the van Genuchten parameterization.
     *
     * \param swe       The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    template<class Scalar>
    static Scalar dkrw_dsw(Scalar swe, const Params<Scalar>& params)
    {
        DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::dkrw_dsw(params, swe)");
    };

    /*!
     * \brief The relative permeability for the nonwetting phase
     *        of the medium implied by van Genuchten's
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    template<class Scalar>
    static Scalar krn(Scalar swe, const Params<Scalar>& params)
    {
        assert(0 <= swe && swe <= 1);

        Scalar krn;
        //imbibition
        if(!params.acE())
        {
            Scalar intSw1= evaluateIntegral_(intImbSw1_, swe);
            krn = (1.0 - swe) * (1 - swe) *(intSw1 / intImb01_);
        }
        //drainage
        else
        {
            Scalar intSw1 = evaluateIntegral_(intDraSw1_, swe);
            krn = (1.0 - swe) * (1 - swe) *(intSw1 / intDra01_);
        }
        assert(0 <= krn && krn <= 1);

        return(krn);

    };

    /*!
     * \brief The derivative of the relative permeability for the
     *        nonwetting phase in regard to the wetting saturation of
     *        the medium as implied by the van Genuchten
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    template<class Scalar>
    static Scalar dkrn_dsw(Scalar swe, const Params<Scalar>& params)
    {
        DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::dkrn_dsw(params, swe)");
    }

private:
    template<class Scalar>
    static Scalar evaluateIntegral_(const std::vector<double>& intVector, const Scalar swe)
    {
        Scalar satPos = (integrationSteps_ - 1) * swe;
        unsigned satIdx = (unsigned)satPos;
        Scalar weight = satPos - satIdx;

        //interpolate integral at swe
        Scalar integral = intVector[satIdx] * (1 - weight) + intVector[satIdx + 1] * weight;

        return integral;
    }

    static constexpr int integrationSteps_ = 20;
    static constexpr double intImb01_ = 3.1349682090e-08;
    static constexpr double intDra01_ = 9.2189777362e-01;
    static std::vector<double> intImb0Sw_;
    static std::vector<double> intImbSw1_;
    static std::vector<double> intDra0Sw_;
    static std::vector<double> intDraSw1_;
};


template <class Scalar>
class AcostaRegularization
{
public:
    template<class S>
    struct Params
    {
        Params(S thresholdSw = 1e-3)
        : thresholdSw_(thresholdSw)
        {}

        S thresholdSw() const
        { return thresholdSw_; }

        bool operator== (const Params& p) const
        { return Dune::FloatCmp::eq(thresholdSw(), p.thresholdSw(), 1e-6);}

    private:
        S thresholdSw_;
    };

    template<class MaterialLaw, class BaseParams, class EffToAbsParams>
    void init(const MaterialLaw* m, const BaseParams& bp, const EffToAbsParams& etap, const Params<Scalar>& p)
    {
        thresholdSw_ = p.thresholdSw();

        initParameters_(m, bp, thresholdSw_);
    }

    /*!
     * \brief A regularized Acosta capillary pressure-saturation
     *        curve.
     *
     * regularized part:
     *    - low saturation:  extend the \f$p_c(S_w)\f$ curve with the slope at the regularization point (i.e. no kink).
     *    - high saturation: connect the high regularization point with \f$ \overline S_w =1\f$ by a straight line (yes, there is a kink :-( ).
     *
     * For the non-regularized part:
     *
     * \copydetails AcostaPcSw::pc()
     */
    OptionalScalar<Scalar> pc(Scalar swe) const
    {
        // make sure that the capilary pressure observes a derivative != 0 for 'illegal' saturations. This is
        // required for example by newton solvers (if the derivative is calculated numerically) in order to get the
        // saturation moving to the right direction if it temporarily is in an 'illegal' range.

        if (acE_ == 0.0) //imbibition
        {
            if (swe < thresholdSw_)
                return pcsweLow_ + mLow_*( swe - thresholdSw_);
            else if (swe >= (1.0 - thresholdSw_))
                return pcsweHigh_ + mHigh_*(swe - (1.0 - thresholdSw_));
        }
        else //drainage
        {
            if (swe <= thresholdSw_)
                return pcsweLow_ + mLow_*(swe - thresholdSw_);
            else if (swe >= (1.0 - thresholdSw_))
                return pcsweHigh_ + mHigh_*(swe - (1.0 - thresholdSw_));
        }

        // if the effective saturation is in an 'reasonable'
        // range, we use the real Acosta law...
        return {};
    }

    /*!
     * \brief   A regularized Acosta saturation-capillary pressure curve.
     *
     *          function does not exist yet!
     */
    OptionalScalar<Scalar> sw(Scalar pc) const
    {
        DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::sw(bp, pc)");
    }

    /*!
     * \brief A regularized version of the partial derivative
     *        of the \f$p_c(\overline S_w)\f$ w.r.t. effective saturation
     *        according to Acosta.
     *
     * regularized part:
     *    - low saturation:  use the slope of the regularization point (i.e. no kink).
     *    - high saturation: connect the high regularization point with \f$ \overline S_w =1\f$
     *                       by a straight line and use that slope (yes, there is a kink :-( ).
     *
     * For the non-regularized part:
     *
     * \copydetails AcostaPcSw::dpc_dsw()
     */
    OptionalScalar<Scalar> dpc_dsw(Scalar swe) const
    {
        // derivative of the regualarization
        if (swe < 0)
            return m0_;
        else if (swe >= (1.0 - thresholdSw_))
            return mHigh_;

        return {};
    }

    /*!
     * \brief The partial derivative of the effective
     *        saturation to the capillary pressure according to Acosta.
     *
     *        function does not exist yet!
     */
    OptionalScalar<Scalar> dsw_dpc(Scalar pc) const
    { DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::dsw_dpc(pc, bp)"); }

    /*!
     * \brief   Regularized version of the  relative permeability
     *          for the wetting phase of
     *          the medium implied by the Acosta
     *          parameterization.
     *
     *  regularized part:
     *    - below \f$ \overline S_w =0\f$:                  set relative permeability to zero
     *    - above \f$ \overline S_w =1\f$:                  set relative permeability to one
     *    - between \f$ 0.95 \leq \overline S_w \leq 1\f$:  use a spline as interpolation
     *
     *  For not-regularized part: \copydetails AcostaPcSw::krw()
     */
    OptionalScalar<Scalar> krw(Scalar swe) const
    {
        if (swe <= 0.0)
            return 0.0;
        else if (swe >= 1.0)
            return 1.0;

        return {};
    }

    /*!
    * \brief The derivative of the relative permeability for the
    *        wetting phase in regard to the wetting saturation of the
    *        medium implied by the Acosta parameterization.
    */
    OptionalScalar<Scalar> dkrw_dsw(Scalar swe) const
    { DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::dkrw_dsw(swe, bp)"); };

    /*!
    * \brief   Regularized version of the  relative permeability
    *          for the nonwetting phase of
    *          the medium implied by the Acosta
    *          parameterization.
    *
    * regularized part:
    *    - below \f$ \overline S_w =0\f$:                  set relative permeability to zero
    *    - above \f$ \overline S_w =1\f$:                  set relative permeability to one
    *    - for \f$ 0 \leq \overline S_w \leq 0.05 \f$:     use a spline as interpolation
    *    \copydetails AcostaPcSw::krn()
    */
    OptionalScalar<Scalar> krn(Scalar swe) const
    {
        if (swe >= 1.0)
            return 0.0;
        else if (swe <= 0.0)
            return 1.0;

        return {};
    }

    /*!
    * \brief The derivative of the relative permeability for the
    *        nonwetting phase in regard to the wetting saturation of
    *        the medium as implied by the Acosta
    *        parameterization.
    */
    OptionalScalar<Scalar> dkrn_dsw(Scalar swe) const
    {  DUNE_THROW(Dune::NotImplemented, "AcostaPcSw::dkrn_dsw(swe, bp)"); };

private:
    template<class MaterialLaw, class BaseParams>
    void initParameters_(const MaterialLaw* m, const BaseParams& bp, const Scalar thresholdSw)
    {
        thresholdSw_ = thresholdSw;
        m0_ = AcostaPcSw::dpc_dsw(0.0, bp);

        mLow_ = AcostaPcSw::dpc_dsw(thresholdSw_, bp);
        pcsweLow_ = AcostaPcSw::pc(thresholdSw_, bp);

        mHigh_ = AcostaPcSw::dpc_dsw((1.0-thresholdSw_), bp);
        pcsweHigh_ = AcostaPcSw::pc((1.0-thresholdSw_), bp);

        acE_ = bp.acE();
    }

    Scalar thresholdSw_;
    Scalar m0_;
    Scalar mLow_;
    Scalar pcsweLow_;
    Scalar mHigh_;
    Scalar pcsweHigh_;
    Scalar acE_;
};

/*!
 * \ingroup Fluidmatrixinteractions
 * \brief A default configuration for using the VanGenuchten material law
 */
template<typename Scalar = double>
using AcostaPcSwDefault = TwoPMaterialLaw<Scalar, AcostaPcSw, AcostaRegularization<Scalar>, TwoPEffToAbsDefaultPolicy>;


std::vector<double> AcostaPcSw::intImb0Sw_ = {0.0000000000e+00, 1.4448654771e-08, 2.2819077599e-08, 2.7262869143e-08, 2.9465634517e-08, 3.0504079857e-08,
            3.0976893747e-08, 3.1187237078e-08, 3.1279418104e-08, 3.1319431548e-08, 3.1336696665e-08, 3.1344118639e-08, 3.1347301932e-08, 3.1348665344e-08,
            3.1349248803e-08, 3.1349498362e-08, 3.1349605071e-08, 3.1349650691e-08, 3.1349670192e-08, 3.1349678527e-08, 3.1349682090e-08};

std::vector<double> AcostaPcSw::intImbSw1_ = {3.1349682090e-08, 1.6901027319e-08, 8.5306044914e-09, 4.0868129471e-09, 1.8840475735e-09, 8.4560223338e-10,
            3.7278834340e-10, 1.6244501261e-10, 7.0263986151e-11, 3.0250542321e-11, 1.2985425390e-11, 5.5634511134e-12, 2.3801587239e-12, 1.0167462066e-12,
            4.3328717089e-13, 1.8372876392e-13, 7.7019533374e-14, 3.1399697336e-14, 1.1898564029e-14, 3.5629091876e-15, 0.0000000000e+00};

std::vector<double> AcostaPcSw::intDra0Sw_ = {0.0000000000e+00, 9.0286303155e-11, 1.0440347151e-09, 5.3755677032e-09, 2.0922261595e-08, 7.4676307329e-08,
            2.8041525597e-07, 1.3662599439e-06, 2.4661875855e-05, 9.2186966645e-01, 9.2188585863e-01, 9.2189413795e-01, 9.2189728035e-01, 9.2189773354e-01,
            9.2189777056e-01, 9.2189777338e-01, 9.2189777360e-01, 9.2189777360e-01, 9.2189777359e-01, 9.2189777356e-01, 9.2189777362e-01};

std::vector<double> AcostaPcSw::intDraSw1_ = {9.2189777362e-01, 9.2189777350e-01, 9.2189777259e-01, 9.2189776823e-01, 9.2189775271e-01, 9.2189769898e-01,
            9.2189749322e-01, 9.2189640736e-01, 9.2187311174e-01, 2.8107160428e-05, 1.1914958378e-05, 3.6356677731e-06, 4.9325835742e-07, 4.0074681584e-08,
            3.0250536841e-09, 2.3593717317e-10, 1.8918448614e-11, 1.5374841055e-12, 1.2497552223e-13, 9.4684316652e-15, 0.0000000000e+00};
}

#endif
