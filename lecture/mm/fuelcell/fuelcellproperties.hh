// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Properties file for water management in PEM fuel cells.
 */
#ifndef DUMUX_FUELCELL_LECTURE_PROPERTIES_HH
#define DUMUX_FUELCELL_LECTURE_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/material/fluidsystems/h2on2o2.hh>
#include <dumux/discretization/box.hh>

#include "fuelcellspatialparams.hh"

#include "fuelcellproblem.hh"

namespace Dumux::Properties {

#if ISOTHERMAL
// Create new type tags
namespace TTag {
struct FuelCellLectureProblem { using InheritsFrom = std::tuple<FuelCellLectureSpatialParamsTypeTag, TwoPNC, BoxModel>; };
} // end namespace TTag
#else
namespace TTag {
struct FuelCellLectureProblem { using InheritsFrom = std::tuple<FuelCellLectureSpatialParams, TwoPNCNI, BoxModel>; };
} // end namespace TTag
#endif

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::FuelCellLectureProblem> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::FuelCellLectureProblem> { using type = FuelCellLectureProblem<TypeTag>; };

// Set the primary variable combination for the 2pnc model
template<class TypeTag>
struct Formulation<TypeTag, TTag::FuelCellLectureProblem>
{
    static constexpr auto value = TwoPFormulation::p1s0;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FuelCellLectureProblem>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::H2ON2O2<Scalar>;
};

// Define which component balance equation gets replaced by total balance {0,1,2,..,numComponents(none)}
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::FuelCellLectureProblem> { static constexpr int value = 3; };

} // end namespace Dumux::Properties

#endif
