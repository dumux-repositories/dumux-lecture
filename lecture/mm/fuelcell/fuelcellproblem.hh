// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Definition of a problem for water management in PEM fuel cells.
 */
#ifndef DUMUX_FUELCELL_LECTURE_PROBLEM_HH
#define DUMUX_FUELCELL_LECTURE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

#if !ISOTHERMAL
#include <dumux/material/chemistry/electrochemistry/electrochemistryni.hh>
#else
#include <dumux/material/chemistry/electrochemistry/electrochemistry.hh>
#endif

namespace Dumux {

/*!
 * \brief Problem or water management in PEM fuel cells.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_fuelcell -parameterFile ./test_fuelcell.input</tt>
 */
template <class TypeTag>
class FuelCellLectureProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    static constexpr int dim = GridView::dimension;
    static constexpr int numComponents = GetPropType<TypeTag, Properties::ModelTraits>::numFluidComponents();
#if !ISOTHERMAL
    static constexpr int temperatureIdx = Indices::temperatureIdx; // temperature
#endif
    static constexpr int pressureIdx = Indices::pressureIdx; // gas-phase pressure
    static constexpr int switchIdx = Indices::switchIdx; // liquid saturation or mole fraction
#if !ISOTHERMAL
    static constexpr int energyEqIdx = Indices::energyEqIdx; // energy equation
#endif
    static constexpr int conti0EqIdx = Indices::conti0EqIdx;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
#if !ISOTHERMAL
    using ElectroChemistry = typename Dumux::ElectroChemistryNI<Scalar, Indices, FluidSystem, FVGridGeometry, ElectroChemistryModel::Acosta>;
#else
    using ElectroChemistry = typename Dumux::ElectroChemistry<Scalar, Indices, FluidSystem, FVGridGeometry, ElectroChemistryModel::Acosta>;
#endif
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethods::box;
    static constexpr int dofCodim = isBox ? dim : 0;

public:
    /*!
     * \brief The constructor
     *
     * \param fvGridGeometry Contains the Finite-Volume-Grid-Geometry
     */
    FuelCellLectureProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        nTemperature_       = getParam<Scalar>("Problem.NTemperature");
        nPressure_          = getParam<Scalar>("Problem.NPressure");
        pressureLow_        = getParam<Scalar>("Problem.PressureLow");
        pressureHigh_       = getParam<Scalar>("Problem.PressureHigh");
        temperatureLow_     = getParam<Scalar>("Problem.TemperatureLow");
        temperatureHigh_    = getParam<Scalar>("Problem.TemperatureHigh");
        temperature_        = getParam<Scalar>("Problem.InitialTemperature");
        name_               = getParam<std::string>("Problem.Name");

        pO2Inlet_            = getParam<Scalar>("ElectroChemistry.pO2Inlet");
        pgInlet1_            = getParam<Scalar>("OperationalConditions.GasPressureInlet1");
        pgInlet2_            = getParam<Scalar>("OperationalConditions.GasPressureInlet2");
        swInlet_             = getParam<Scalar>("OperationalConditions.LiquidWaterSaturationInlet");

        eps_ = 1e-6;
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);

        const auto& gridView = this->gridGeometry().gridView();
        currentDensity_.resize(gridView.size(dofCodim));
        reactionSourceH2O_.resize(gridView.size(dofCodim));
        reactionSourceO2_.resize(gridView.size(dofCodim));
#if !ISOTHERMAL
        reactionSourceTemp_.resize(gridView.size(dofCodim));
#endif
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    {
        return name_;
    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 70 degrees Celsius.
     */
    Scalar temperature() const
    {
        return temperature_;
    }

    //! \copydoc Dumux::FVProblem::source()
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector values;

        const GlobalPosition globalPos = scv.dofPosition();
        const VolumeVariables& volVars = elemVolVars[scv];

        values = 0.0;

        // reaction sources from electro chemistry
        Scalar reactionLayerWidth = getParam<Scalar>("Problem.ReactionLayerWidth");

        if (globalPos[0] < reactionLayerWidth + eps_)
        {
            // Hack: division by two to get to the (wrongly implemented) current density
            // of the Acosta paper fuelcell model
            auto currentDensity = ElectroChemistry::calculateCurrentDensity(volVars)/2;
            ElectroChemistry::reactionSource(values, currentDensity, "");
        }

        return values;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        values.setAllNeumann();

        if ((globalPos[1]>=0 && globalPos[1]<=0.0005 && globalPos[0] > 0.0005 - eps_)
            || (globalPos[1]<=0.002 && globalPos[1]>=0.0015 && globalPos[0] > 0.0005 - eps_))
        {
            values.setAllDirichlet();
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values = initial_(globalPos);

        // lower inlet
        if ((globalPos[1] >= 0 && globalPos[1] <= 0.0005 && globalPos[0] > 0.0005 - eps_))
        {
            values[pressureIdx] = pgInlet2_;
            values[switchIdx] = swInlet_;    // Acosta Inlet liquid water saturation
            values[switchIdx+1] = pO2Inlet_/Dumux::BinaryCoeff::H2O_O2::henry(temperature_);;
        }
        // upper inlet
        if ((globalPos[1] <= 0.002 && globalPos[1] >= 0.0015 && globalPos[0] > 0.0005 - eps_))
        {
            values[pressureIdx] = pgInlet1_;
            values[switchIdx] = swInlet_;    // Acosta Inlet liquid water saturation
            values[switchIdx+1] = pO2Inlet_/Dumux::BinaryCoeff::H2O_O2::henry(temperature_);
        }
#if !ISOTHERMAL
        values[temperatureIdx] = 343.15;
#endif
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param globalPos The global position
     *
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
#if !ISOTHERMAL
        if (globalPos[1] >= 0.0005 && globalPos[1] <= 0.0015 && globalPos[0] > 0.0005 - eps_)
            values[energyEqIdx] = 9800; // [W/m^2]; heat conduction flow thorough the shoulder;from Acosta-paper: lambda_shoulder/delta_shoulder*DeltaT
#endif

        return values;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    template<class VTKWriter>
    void addVtkFields(VTKWriter& vtk)
    {
        vtk.addField(currentDensity_, "currentDensity [A/cm^2]");
        vtk.addField(reactionSourceH2O_, "reactionSourceH2O [mol/(sm^2)]");
        vtk.addField(reactionSourceO2_, "reactionSourceO2 [mol/(sm^2)]");
#if !ISOTHERMAL
        vtk.addField(reactionSourceTemp_, "reactionSource Heat [J/(sm^2)]");
#endif
    }

    void updateVtkFields(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto elemSol = elementSolution(element, curSol, this->gridGeometry());

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto& globalPos = scv.dofPosition();
                const auto dofIdxGlobal = scv.dofIndex();

                static Scalar reactionLayerWidth = getParam<Scalar>("Problem.ReactionLayerWidth");
                if (globalPos[0] < reactionLayerWidth + eps_)
                {
                    // reactionSource Output
                    PrimaryVariables source(0.0);

                    // Hack: division by two to get to the (wrongly implemented) current density
                    // of the Acosta paper fuelcell model
                    auto i = ElectroChemistry::calculateCurrentDensity(volVars)/2;
                    ElectroChemistry::reactionSource(source, i, "");

                    reactionSourceH2O_[dofIdxGlobal] = source[Indices::conti0EqIdx + FluidSystem::H2OIdx];
                    reactionSourceO2_[dofIdxGlobal] = source[Indices::conti0EqIdx + FluidSystem::O2Idx];
#if !ISOTHERMAL
                    reactionSourceTemp_[dofIdxGlobal] = source[numComponents];
#endif
                    // Current Output in A/cm^2
                    currentDensity_[dofIdxGlobal] = i/10000;
                }

                else
                {
                    reactionSourceH2O_[dofIdxGlobal] = 0.0;
                    reactionSourceO2_[dofIdxGlobal] = 0.0;
#if !ISOTHERMAL
                    reactionSourceTemp_[dofIdxGlobal] = 0.0;
#endif
                    currentDensity_[dofIdxGlobal] = 0.0;
                }
            }
        }
    }

private:
    PrimaryVariables initial_( const GlobalPosition &globalPos) const
    {
        // The internal method for the initial condition
        PrimaryVariables values;

        values[pressureIdx] = pgInlet1_;
        values[switchIdx] = swInlet_;
        values[switchIdx+1] = pO2Inlet_/Dumux::BinaryCoeff::H2O_O2::henry(temperature_);

#if !ISOTHERMAL
        values[temperatureIdx] = 343.15;
#endif
        values.setState(Indices::bothPhases);

        return values;
    }

    Scalar temperature_;
    Scalar eps_;

    int nTemperature_;
    int nPressure_;

    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;

    Scalar pO2Inlet_;
    Scalar pgInlet1_;
    Scalar pgInlet2_;
    Scalar swInlet_;

    std::vector<double> currentDensity_;
    std::vector<double> reactionSourceH2O_;
    std::vector<double> reactionSourceO2_;
#if !ISOTHERMAL
    std::vector<double> reactionSourceTemp_;
#endif
};

} // end namespace Dumux

#endif
