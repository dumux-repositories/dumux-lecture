// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Definition of the spatial parameters for the 1pnc
 *        problems.
 */
#ifndef DUMUX_CONVMIX_SPATIAL_PARAMS_HH
#define DUMUX_CONVMIX_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/porousmediumflow/fvspatialparams1p.hh>

namespace Dumux {

/*!
 * \brief Definition of the spatial parameters for the 1pnc
 *        test problems.
 */
template<class FVGridGeometry, class Scalar>
class ConvmixSpatialParams
: public FVPorousMediumFlowSpatialParamsOneP<FVGridGeometry, Scalar,
                             ConvmixSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVPorousMediumFlowSpatialParamsOneP<FVGridGeometry, Scalar,
                                           ConvmixSpatialParams<FVGridGeometry, Scalar>>;

    static const int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Dune::FieldVector<Scalar, dimWorld>;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    ConvmixSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        permeability_ = getParam<Scalar>("SpatialParams.Permeability", 3e-13);
        porosity_ = 0.2;
        depthBOR_ =  getParam<Scalar>("SpatialParams.DepthBOR");
    }

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param globalPos The global position
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return porosity_; }

    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param scv The sub-control volume
     * \param elemSol The solution for all dofs of the element
     */
    template<class ElementSolution>
    Scalar dispersivity(const Element &element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
    { return 0; }

    /*!
     * \brief Returns the temperature at the domain at the given position
     * \param globalPos The position in global coordinates where the temperature should be specified
     *        // geothermal temperatur in [K]
             // assuming 10°C at surface and a
            // geothermal gradient of 0.032 K/m
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 283.15 + (depthBOR_ - globalPos[1])*0.032;
    }

    /*!
     * \brief Define bottom of reservoir \f$\mathrm{[m]}\f$.
     *
     * \param globalPos The global position
     */
    Scalar depthBorAtPos(const GlobalPosition& globalPos) const
    { return depthBOR_; }

private:
    Scalar permeability_;
    Scalar porosity_;
    Scalar depthBOR_;
};

} // end namespace Dumux

#endif
