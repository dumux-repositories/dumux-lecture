// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/**
 * \file
 * \brief properties file for component transport of nitrogen dissolved in the water phase.
 */
#ifndef DUMUX_CONVMIXING_PROPERTIES_HH
#define DUMUX_CONVMIXING_PROPERTIES_HH

#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif
#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/1pnc/model.hh>

#include <dumux/material/fluidsystems/brineco2.hh>
#include <lecture/common/co2tablesbenchmark3.hh>

#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

#include <dumux/material/fluidsystems/1padapter.hh>

#include "convmixspatialparams.hh"
#include "convmixproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct ConvmixTypeTag { using InheritsFrom = std::tuple<OnePNC>; };
struct ConvmixBoxTypeTag { using InheritsFrom = std::tuple<ConvmixTypeTag, BoxModel>; };
} // end namespace TTag

template<class TypeTag>
struct Grid<TypeTag, TTag::ConvmixTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ConvmixTypeTag> { using type = ConvmixProblem<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ConvmixTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using BrineCO2 = FluidSystems::BrineCO2<Scalar,
        Components::CO2<Scalar, GeneratedCO2Tables::CO2Tables>,
        Components::TabulatedComponent<Components::H2O<Scalar>>,
        FluidSystems::BrineCO2DefaultPolicy</*constantSalinity=*/true, /*simpleButFast=*/true>
    >;
    using type = FluidSystems::OnePAdapter<BrineCO2, BrineCO2::liquidPhaseIdx>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::ConvmixTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = ConvmixSpatialParams<FVGridGeometry, Scalar>;
};

template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::ConvmixTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = DiffusivityConstantTortuosity<Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::ConvmixTypeTag> { static constexpr bool value = true; };

} //end namespace Dumux::Properties

#endif
