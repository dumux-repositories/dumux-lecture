// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the column problem.
 */
#ifndef DUMUX_COLUMNXYLENE_SPATIAL_PARAMS_HH
#define DUMUX_COLUMNXYLENE_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/3p3c/indices.hh>
#include <dumux/common/properties.hh>
#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/3p/parkervangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/3p/napladsorption.hh>

namespace Dumux {

/*!
 * \brief Definition of the spatial parameters for the column problem
 */
template<class FVGridGeometry, class Scalar>
class ColumnSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<FVGridGeometry, Scalar,
                         ColumnSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<FVGridGeometry, Scalar,
                                       ColumnSpatialParams<FVGridGeometry, Scalar>>;

    using GlobalPosition = typename SubControlVolume::GlobalPosition;
    using ThreePhasePcKrSw = FluidMatrix::ParkerVanGenuchten3PDefault<Scalar>;
    using AdsorptionModel = FluidMatrix::ThreePNAPLAdsorption<Scalar>;

public:
    using PermeabilityType = Scalar;

    /*!
     * \brief The constructor
     */
    ColumnSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    , fineThreePhasePcKrSw_("SpatialParams.FineMaterial")
    , coarseThreePhasePcKrSw_("SpatialParams.CoarseMaterial")
    , adsorption_("SpatialParams")
    {
        // intrinsic permeabilities
        fineK_ = getParam<Scalar>("SpatialParams.Permeability");
        coarseK_ = getParam<Scalar>("SpatialParams.PermeabilityExtDomain");

        // porosities
        finePorosity_ = 0.46;
        coarsePorosity_ = 0.46;
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$
     * \note  It is possibly solution dependent.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const auto& globalPos = scv.dofPosition();
        if (isFineMaterial_(globalPos))
            return fineK_;
        return coarseK_;
    }

  /*!
     * \brief Function for defining the solid volume fraction.
     *        That is possibly solution dependent.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \param compIdx The solid component index
     * \return the volume fraction of the inert solid component with index compIdx
     */
    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        const auto& globalPos = scv.dofPosition();
        if (compIdx == SolidSystem::comp0Idx)
        {
            if (isFineMaterial_(globalPos))
                return 1-finePorosity_;
            else
                return 0;
        }
        else
        {
            if (isFineMaterial_(globalPos))
                  return 0;
            else
                return 1-coarsePorosity_;
        }
    }

    /*!
     * \brief return the parameter object for the Brooks-Corey material law
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        const auto& globalPos = scv.dofPosition();
        if (isFineMaterial_(globalPos))
            return makeFluidMatrixInteraction(fineThreePhasePcKrSw_, adsorption_);
        return makeFluidMatrixInteraction(coarseThreePhasePcKrSw_, adsorption_);
    }

private:
    bool isFineMaterial_(const GlobalPosition &pos) const
    {
        if (0.90 < pos[1] + eps_)
            return true;

        else
            return false;
    };

    Scalar fineK_;
    Scalar coarseK_;

    Scalar finePorosity_;
    Scalar coarsePorosity_;

    Scalar fineHeatCap_;
    Scalar coarseHeatCap_;

    const ThreePhasePcKrSw fineThreePhasePcKrSw_;
    const ThreePhasePcKrSw coarseThreePhasePcKrSw_;
    const AdsorptionModel adsorption_;

    static constexpr Scalar eps_ = 1.5e-7;
};

} // end namespace Dumux

#endif
