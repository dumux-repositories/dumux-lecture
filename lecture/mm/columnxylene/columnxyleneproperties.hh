// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Properties file of non-isothermal water injection into a
 *        sand column with a NAPL contamination.
 */
#ifndef DUMUX_COLUMNXYLENEPROPERTIES_HH
#define DUMUX_COLUMNXYLENEPROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/material/fluidsystems/h2oairxylene.hh>
#include <dumux/material/solidstates/compositionalsolidstate.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/3p3c/model.hh>

#include "columnxylenespatialparams.hh"
#include "columnxyleneproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct ColumnTypeTag { using InheritsFrom = std::tuple<ThreePThreeCNI>; };
struct ColumnProblemBoxTypeTag { using InheritsFrom = std::tuple<BoxModel, ColumnTypeTag>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ColumnTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ColumnTypeTag> { using type = ColumnProblem<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ColumnTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::H2OAirXylene<Scalar>;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::ColumnTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Dumux::Components::Constant<1, Scalar>;
    using ComponentTwo = Dumux::Components::Constant<2, Scalar>;
    static constexpr int numInertComponents = 2;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo, numInertComponents>;
};


//! The two-phase model uses the immiscible fluid state
template<class TypeTag>
struct SolidState<TypeTag, TTag::ColumnTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
public:
    using type = CompositionalSolidState<Scalar, SolidSystem>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::ColumnTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = ColumnSpatialParams<FVGridGeometry, Scalar>;
};

} // end namespace Dumux::Properties

#endif
