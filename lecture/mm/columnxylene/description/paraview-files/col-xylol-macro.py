# SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

columnxylol_pvd = GetActiveSource()
RenderView1 = GetRenderView()
DataRepresentation1 = Show()
DataRepresentation1.ScalarOpacityUnitDistance = 0.2441313782491492
DataRepresentation1.EdgeColor = [0.0, 0.0, 0.5000076295109483]

Clip1 = Clip( ClipType="Plane" )

a1_Sw_PVLookupTable = GetLookupTableForArray( "Sw", 1, NanColor=[0.25, 0.0, 0.0], RGBPoints=[0.004999999888241291, 0.23, 0.299, 0.754, 0.004999999888241291, 0.706, 0.016, 0.15], VectorMode='Magnitude', ColorSpace='Diverging', ScalarRangeInitialized=1.0 )

a1_Sw_PiecewiseFunction = CreatePiecewiseFunction()

RenderView1.CameraPosition = [0.05000000074505806, 0.6000000238418579, 2.326257530710312]
RenderView1.CameraFocalPoint = [0.05000000074505806, 0.6000000238418579, 0.0]
RenderView1.CameraClippingRange = [2.302994955403209, 2.361151393670967]
RenderView1.CenterOfRotation = [0.05000000074505806, 0.6000000238418579, 0.0]
RenderView1.CameraParallelScale = 0.6020797527609908

DataRepresentation1.ScalarOpacityFunction = a1_Sw_PiecewiseFunction
DataRepresentation1.ColorArrayName = 'Sw'
DataRepresentation1.LookupTable = a1_Sw_PVLookupTable

Clip1.Scalars = ['CELLS', 'permeability']
Clip1.ClipType.Origin = [0.05000000074505806, 0.6000000238418579, 0.0]
Clip1.ClipType = "Plane"

RenderView1.CameraClippingRange = [2.302994955403209, 2.361151393670967]

Clip1.ClipType.Origin = [0.0, 0.9, 0.0]
Clip1.ClipType.Normal = [0.0, 1.0, 0.0]

DataRepresentation2 = Show()
DataRepresentation2.ColorArrayName = 'Sw'
DataRepresentation2.ScalarOpacityUnitDistance = 0.1017715390583867
DataRepresentation2.EdgeColor = [0.0, 0.0, 0.5000076295109483]

PlotOverLine1 = PlotOverLine( Source="High Resolution Line Source" )

RenderView1.CameraPosition = [0.05000000074505806, 1.050000011920929, 0.6109052643670619]
RenderView1.CameraClippingRange = [0.6047962117233913, 0.620068843332568]
RenderView1.CameraFocalPoint = [0.05000000074505806, 1.050000011920929, 0.0]
RenderView1.CameraParallelScale = 0.15811391717158596
RenderView1.CenterOfRotation = [0.05000000074505806, 1.050000011920929, 0.0]

DataRepresentation1.Visibility = 0

DataRepresentation2.ScalarOpacityFunction = a1_Sw_PiecewiseFunction
DataRepresentation2.LookupTable = a1_Sw_PVLookupTable
DataRepresentation2.ColorAttributeType = 'CELL_DATA'

PlotOverLine1.Source.Point1 = [0.0, 0.8999999761581421, 0.0]
PlotOverLine1.Source.Point2 = [0.10000000149011612, 1.2000000476837158, 0.0]

Clip1.Scalars = ['POINTS', 'permeability']

PlotOverLine1.Source.Point1 = [0.0, 1.2, 0.0]
PlotOverLine1.Source.Point2 = [0.0, 0.9, 0.0]

AnimationScene1 = GetAnimationScene()
XYChartView1 = CreateXYPlotView()
XYChartView1.ChartTitle = 'T-Pg @${TIME}s'
XYChartView1.AxisRange = [273.0, 378.0, 0.0, 0.3, 100000.0, 106000.0, 0.0, 1.0]
XYChartView1.AxisGridColor = [0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258, 0.9499961852445258]
XYChartView1.ViewTime = 1000.0
XYChartView1.AxisBehavior = [1, 1, 1, 0]
XYChartView1.AxisTitle = ['', '', '', '']
XYChartView1.AxisTitleColor = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5000076295109483, 0.0, 0.0, 0.5000076295109483]

DataRepresentation3 = Show()
DataRepresentation3.XArrayName = 'arc_length'
DataRepresentation3.SeriesColor = ['Sw', '0', '0', '0', 'Sn', '0.894118', '0.101961', '0.109804', 'Sg', '0.215686', '0.494118', '0.721569', 'pw', '0.301961', '0.686275', '0.290196', 'pn', '0.596078', '0.305882', '0.639216', 'pg', '1', '0.498039', '0', 'rhow', '0.65098', '0.337255', '0.156863', 'rhon', '0.47451', '0.0901961', '0.0901961', 'rhog', '0.709804', '0.00392157', '0.00392157', 'x^w_H2O', '0.937255', '0.278431', '0.0980392', 'x^w_xylene', '0.976471', '0.513725', '0.141176', 'x^w_Air', '1', '0.705882', '0', 'x^n_H2O', '1', '0.898039', '0.0235294', 'x^n_xylene', '0.458824', '0.694118', '0.00392157', 'x^n_Air', '0.345098', '0.501961', '0.160784', 'x^g_H2O', '0.313725', '0.843137', '0.74902', 'x^g_xylene', '0.109804', '0.584314', '0.803922', 'x^g_Air', '0.231373', '0.407843', '0.670588', 'porosity', '0.603922', '0.407843', '1', 'permeability', '0.372549', '0.2', '0.501961', 'temperature', '0.231373', '0.407843', '0.670588', 'phase presence', '0.109804', '0.584314', '0.803922', 'process rank', '0.305882', '0.85098', '0.917647', 'vtkValidPointMask', '0.45098', '0.603922', '0.835294', 'arc_length', '0.258824', '0.239216', '0.662745', 'Points (0)', '0.313725', '0.329412', '0.529412', 'Points (1)', '0.0627451', '0.164706', '0.321569', 'Points (2)', '0.109804', '0.584314', '0.803922', 'Points (Magnitude)', '0.231373', '0.407843', '0.670588', 'vtkOriginalIndices', '0.4', '0.243137', '0.717647']
DataRepresentation3.SeriesVisibility = ['vtkValidPointMask', '0', 'arc_length', '0', 'Points (0)', '0', 'Points (1)', '0', 'Points (2)', '0', 'Points (Magnitude)', '0', 'vtkOriginalIndices', '0', 'Sw', '0', 'Sn', '0', 'Sg', '0', 'pw', '0', 'pn', '0', 'pg', '1', 'rhow', '0', 'rhon', '0', 'rhog', '0', 'x^w_H2O', '0', 'x^w_xylene', '0', 'x^w_Air', '0', 'x^n_H2O', '0', 'x^n_xylene', '0', 'x^n_Air', '0', 'x^g_H2O', '0', 'x^g_xylene', '0', 'x^g_Air', '0', 'porosity', '0', 'permeability', '0', 'temperature', '1', 'phase presence', '0', 'process rank', '0']
DataRepresentation3.SeriesPlotCorner = ['pg', '1', 'temperature', '0']
DataRepresentation3.UseIndexForXAxis = 0

RenderView1.ViewTime = 1000.0

AnimationScene1.AnimationTime = 1000.0
AnimationScene1.ViewModules = [ RenderView1, XYChartView1 ]

RenderView1.CameraClippingRange = [0.6008450623524043, 0.6250425263347399]

DataRepresentation2.EdgeColor = [0.0, 0.0, 0.5019607843137255]

a1_Sn_PVLookupTable = GetLookupTableForArray( "Sn", 1, NanColor=[0.25, 0.0, 0.0], RGBPoints=[0.0, 0.23, 0.299, 0.754, 0.9657518863677979, 0.706, 0.016, 0.15], VectorMode='Magnitude', ColorSpace='Diverging', ScalarRangeInitialized=1.0 )

a1_Sn_PiecewiseFunction = CreatePiecewiseFunction()

SetActiveView(RenderView1)
ScalarBarWidgetRepresentation1 = CreateScalarBar( Title='Sn', LabelFontSize=12, Enabled=1, LookupTable=a1_Sn_PVLookupTable, TitleFontSize=12 )
GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)

RenderView1.ViewTime = 995.0
RenderView1.CameraClippingRange = [0.6047962117233913, 0.620068843332568]

DataRepresentation2.ScalarOpacityFunction = a1_Sn_PiecewiseFunction
DataRepresentation2.ColorArrayName = 'Sn'
DataRepresentation2.LookupTable = a1_Sn_PVLookupTable
DataRepresentation2.ColorAttributeType = 'POINT_DATA'


RenderView1.ViewTime = 950.0

AnimationScene1.AnimationTime = 950.0

XYChartView1.ViewTime = 950.0

RenderView1 = GetRenderView()
RenderView1.CameraClippingRange = [0.6047962117233913, 0.620068843332568]

Render()

