% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\section{Steam Injection into a NAPL-Contaminated Sand Column}

This exercise aims at discussing the numerical simulation of a
steam injection into a small laboratory column with a particular focus
on illustrating the characteristic physical processes. 
The laboratory column experiment was part of a research project
in the VEGAS facility \cite{betzpromo} in the end 1990s. Thus, the
model can be compared with real data.

We can consider the identification of the relevant processes and a reliable determination of the
model parameters as a pre-requisite for a successful model validation
and for the applicability of the model to natural systems and
field-scale problems.

According to \cite{oberkampf2002},
\begin{quote}
validation is the
substantiation that a computerised model within its domain of applicability possesses a satisfactory range of accuracy consistent with the intended application of the model.
\end{quote}
In other words, we consider validation to be the successful comparison of the simulation result with data from a well-controlled experiment.

\subsection{Experimental Set-Up, Physical Processes, and Measurements}
\label{setup}

A schematic description of the experimental configuration and a photo
of the sand-filled glass column, $30$~cm in length and $10$~cm in
diameter, are shown on the slides for this lecture. The column was insulated
to minimize heat loss. The sand was filled into
the column under air-dry conditions so that the initial water
saturation was expected to be below 1~\%. After being saturated with xylene,
the column was allowed to drain through the bottom for several hours,
reaching close to residual xylene saturation. During the drainage, the
bottom of the column was immersed in the outflowing liquid NAPL. Thus,
the NAPL saturation at the bottom was held constant ($S_n \approx 1$).
The mass of xylene then present in the column
immediately before steam injection
was determined by weighing the column after
drainage and comparing it with the weight of the fully
xylene-saturated column.
A xylene mass of 315 g to be removed by steam
flooding was calculated. Numerical simulation of the drainage
process suggested that the average xylene saturation
was approximately 15~\% above residual. Knowing the density of the sand
grains and the total volume of the column, we calculated the porosity of the
sand from the weight of the sand charge to be 46~\%.
Steam was injected from the top
of the column at an average rate of $q_{st} = 0.21$~kg/h. The standard
deviation of the mean value of the injection rate was $\sigma=0.04$~kg/h.
The enthalpy of the steam is assumed to be approximately 2590~kJ/kg, which
corresponds to a composition of 96~\% steam and 4~\% condensate. When
steam came into contact with the cooler soil matrix, it transferred
its latent heat of vaporization ($\approx 2257$~kJ/kg) to the soil
grains. Steam condensed while the temperature of the porous
medium increased. Because of the coupled processes of flow and
condensation, a very stable steam front developed.
Driven by the applied pressure gradient, this steam
front migrated steadily through the column, eventually
breaking through at the bottom, where the liquid
and the gas phases separated. The liquid phase flowed directly into
a liquid collector, where the water and xylene separated. The gas phase
was drawn off into a condenser, from which the condensed liquids
again flowed into a liquid collector. \\
Vaporized xylene is transported in the gas phase towards the
condensation front, where
it recondenses, increasing the xylene saturation at the front.
An increased effective NAPL permeability allows more xylene to be
displaced by pressure and gravitational forces, thus reducing
the amount being evaporated behind the
steam front. As a result, the length of the temperature plateau critically
depends on both the absolute permeability and the relative permeability
of the free-phase xylene. \\
The sensors to monitor temperature alteration during the steam flooding were located 6.5~cm
(T1, also called $T_{upp}$), 14.5~cm (T2, T3, T4, T5, collectively
called $T_{mid}$) and 23~cm (T6) from the top
of the column. Four sensors were placed on the same horizontal level
to observe potential fingering effects.
After passing through the temperature plateau, the temperature in
all six sensors increased to the boiling temperature of pure water.
This means that all NAPL disappeared from the column.
The plateau of Sensor 1 was --as expected-- significantly shorter than
those of the sensors further downstream.
The slope of the signal of Sensor 1 was not as steep as it was at
Sensors 2 to 5. At the beginning of the injection,
the steam has to heat the whole mass of the glass column head and
the steam-supply pipe
to operation temperature. During the first 400s, only part of the
injected energy could be transfered to the sand since the rest of
the energy was lost in the column head. Thus, the temperature increase
at Sensor 1 was flatter before operation temperature was reached and
all the energy of the injected steam could be transfered to the sand.
The slight time shift of the steam-front
breakthroughs at Sensors 2, 3, 4, and 5
strengthened the assumption that the front did not propagate
uniformly, but that some fingering occurred as a result of small-scale
heterogeneity. Nevertheless, these sensors showed a good agreement
in the length of the temperature plateau.
For the same sand as the one which was used in this experiment, F\"arber (1997)
\cite{arnepromo} measured
heat capacity $c_s = 840$ J/(kg K), density $\varrho_s = 2650$ kg/m$^3$
of the soil grains, as well as permeability of the sand
$K = 1.4 \cdot 10^{-11}$ m$^2$ (see Table \ref{rocks}).
Helmig et al. (1998) \cite{Helmigetal:1998a} present numerical
simulations of F\"arber's experiments.
By matching the experimental data, they determined the
van Genuchten parameters $vg_\alpha = 0.0005$ Pa$^{-1}$ and
$vg_n = 4.0$.

\begin{table}[hbt]
\caption{Properties of the porous medium and the glass wall}
\begin{tabular*}{\textwidth}{@{\extracolsep\fill}lccc}
Parameter & & & \\ \hline
Rock-grain density & $\varrho_s$ & 2650 kg/m$^3$ \\
Specific heat capacity of the sand & $c_{s,Sand}$ & 840 J/(kg \, K) \\
Specific heat capacity of the glass & $c_{s,Glass}$ & 775 J/(kg \, K) \\
Porosity & $\phi$ & 0.46 \\
Heat conductivity of water-saturated porous medium & $\lambda_{Sand}$
& 1.60 J/(m \, s \, K) \\ \hline
\end{tabular*}\label{rocks}
\end{table}

\subsection{Fundamental Physical Processes}
Some time after the steam front has reached the contamination,
the mixture of water and NAPL starts boiling.
NAPL evaporates and subsequently disappears.
The evaporated contaminant is transported with the steam flow or
- in the case of mixed steam/air injection - with the air. When flowing
through cooler regions, significant amounts of the NAPL condense. Thus,
until reaching an extraction well, a given NAPL volume may have to be
evaporated several times.
The two diagrams in Fig. 1 show the temperature behavior
in space and time. This behaviour
is characterized by four distinct segments:
(i) before the
propagating steam front reaches the position of a temperature
sensor, the temperature remains almost constant at the initial value of
about 23~$^\circ$C; (ii) when the steam front approaches the sensor, it
causes a steep temperature increase up to the boiling temperature of
the NAPL-water mixture (e.g. in case of xylene-water: $\approx$
92~$^\circ$C); (iii) the temperature again remains constant on that
plateau until all NAPL has been displaced or evaporated, before (iv)
the temperature increases to the boiling temperature of pure water.
\begin{figure}[h]
{\epsfig{file=./eps/segments.eps,width=0.47\textwidth}
 \hfill
 \epsfig{file=./eps/segmentsx.eps,width=0.47\textwidth}}
 \caption{Temperature behavior in space and time}
\end{figure}

\vspace{1cm}
\subsection{Numerical Simulation}
\label{numsim}

{\bf Constitutive Relationships:}
For the three-phase capillary pressure--satu\-ration relationship, we used the Parker approach. 
It uses simplifying assumptions for the behavior of the system. The water phase is assumed to 
be the phase with the greatest wetting capability. NAPL is assumed to be the wetting phase 
with respect to the gas phase and the nonwetting phase with respect to the water phase. 
Since this approach is based on the van Genuchten
functions, it is necessary to determine the curve parameters
$vg_\alpha$ and $vg_n$.  As mentioned above, we used the values
$vg_\alpha = 0.0005$ Pa$^{-1}$ and $vg_n = 4.0$.
The capillary pressures are calculated using a scaling approach
according to Leverett (1941) \cite{lev1}:\\
Capillary pressure between NAPL phase and water phase:
\begin{equation}
p_{c_{nw}} = \frac{1}{\beta_{nw}}p_{c_{gw}}
\end{equation}
Capillary pressure between gas phase and NAPL phase:
\begin{equation}
p_{c_{gn}} = \frac{1}{\beta_{gn}}p_{c_{gw}}
\end{equation}
The scaling parameters $\beta_{nw}$ and $\beta_{gn}$, which depend on the
ratio of the surface tensions, take the values $\beta_{nw}=1.83$ and
$\beta_{gn}=2.20$.

For the residual saturations, we assumed a total liquid residual
saturation of 12~\% and a gas-phase residual saturation of zero.\\
Note that we do not consider hysteresis in our constitutive relationships.
However, as will be mentioned later on, it may be important for the
results of simulations. \\
\\
{\bf Discretization, Initial and Boundary Conditions:}
Time discretization is realized by a fully implicit Euler scheme.
For the spatial discretization of the problem, we used the BOX method.
Three-dimensional effects, due to heat loss through the column walls
or from heterogeneities, are neglected. We discretized the
domain by quasi one-dimensional elements (which are in fact 2D).
The heat capacities of head, bottom, and column walls are taken into
account by assigning an increased effective heat capacity
$c_{eff}$ to the elements:
\begin{equation}
c_{eff} = c_{s, sand} + \frac{m_{glass} \, c_{s, glass}}{m_{sand}} \; .
\end{equation}
For the standard elements (only considering the column walls), this
yielded an effective heat capacity of 970~J/(kg K); for the top and bottom
elements, we used 1125~J/(kg K).
\\
We assigned initial conditions to the primary variables water saturation
$S_w = 0.005$, temperature $T = 23^\circ$C, and gas-phase pressure
$p_g = 101300$ Pa. The initial condition for the NAPL saturation $S_n$
can be determined from the requirement that the mass of NAPL measured in
the experiment be distributed in the column according to the profile of
the capillary pressure--saturation curve. The maximum NAPL saturation at
the column bottom is assumed to be 99\%.
The initial NAPL saturation
$S_n$ is plotted in Fig. \ref{s_profil0}.

\begin{figure}[ht]
    \centerline{\epsfig{file=eps/s_profil0.eps,width=.5\textwidth}}
    \caption{Initial NAPL saturation $S_n$ at the beginning of the
             steam injection}
    \protect\label{s_profil0}
\end{figure}

The boundary conditions at the top are given by the mass flux of the
steam along with the corresponding enthalpy flux. At the bottom, the
fluid phases can break through into the environment at atmospheric
pressure. Thus, it is obvious that the conditions at the bottom
change with time during the experiment. The gas phase is displaced
such that, immediately after the start of the steam injection, the NAPL
saturation at the bottom has to decrease to allow the gas to
pass and enter into the environment. Furthermore, after the breakthrough
at the bottom, the water saturation increases. The difficulty in
modeling arises from the fact that this boundary cannot be described
correctly by either a Dirichlet or a Neumann boundary condition.
Instead, we had to choose a modified model domain that allows a
quasi-simulation of the ambient conditions at the bottom. We extended
\label{extended-domain}
the domain in the vertical direction by a factor of four (here: 1.2m
instead of 0.3m).  We assigned high storage properties to the ambient
part of the domain in order to make sure that the bottom of the
column is influenced minimally by the lower Dirichlet boundary
conditions.  Then, at the bottom boundary of the extended domain, we
fixed the initial conditions as boundary conditions.\\
\\
{\bf Discussion of the Results:}
Comparing the results of the numerical simulation for the temperature curves
$T_{upp}$ and T$_{mid}$ with the measured curves, one can see that the propagating of the steam front is reproduced in good agreement with the measurements.
The temperature increase due to
the passing of the front at Sensor $T_{upp}$ is obviously steeper
in the numerical simulation. We explained this above by boundary
effects that could not be reproduced exactly by the model.
The steepness of the temperature front could also be matched well.
Note that the steepness of the numerical front is dependent on
the discretization length due to numerical diffusion.
The temperature plateau at the water--xylene boiling point
 is reproduced
exactly by the model, proving that the thermodynamic relations
responsible for this, like, for example, vapor-pressure curves or
Dalton's Law, are implemented correctly. \\
One can see that the length of the temperature plateau is overestimated
in the numerical simulation with the initial parameter set described
above, where $vg_n = 4.0$ was used for the van Genuchten parameter.
Again, we do not pay too much attention to the mismatch at Sensor
$T_{upp}$.  It is significant to note that the results
 are obtained without any model calibration, only by
using the parameters described above. The length of the temperature
plateau critically depends on several factors, so that it is difficult to
calibrate the model. For example, a higher steam-injection rate leads
to an increased evaporation rate of liquid xylene and thus to a shorter
temperature plateau. A similar effect is achieved when the
enthalpy of the injected steam is changed. Furthermore, the effective
permeability of the porous medium, resulting from the interaction of
the pore space with the flowing fluids, is important.
In particular, it is extremely complex to determine the relative
permeability relationships in three-fluid-phase systems. When changing
the van Genuchten parameter to $vg_n = 5.0$, we get a better match
of the plateau length. An increased $vg_n$ value yields a higher
mobility of liquid xylene; thus, the recondensed xylene at the front
can be better displaced by gravity and less xylene has to be
re-evaporated.
Effects of hysteresis are also the possible cause of the different
lengths of the temperature plateau and the better results obtained with
increased $vg_n$ values.  Initially, the sand was saturated with
xylene, which then started to drain in a downward direction. Thus, the
pore space at the beginning of the steam injection is filled with
NAPL and gas.  Vaporized xylene from the hotter upper parts of the
column recondensed at the front,
increasing xylene saturation and thus xylene relative permeability.
The flow of NAPL reversed from
drainage to imbibition. Xylene is the wetting phase with respect to the
gas phase and
thus tended to flow through the smaller pores. However, fluid entrapment
of gas might result in an altered distribution of the
pores available for xylene flow, allowing xylene to flow through larger
pores. This causes an additional increase in the xylene relative
permeability, which can be expressed by changing the value
of $vg_n$. In the case of imbibition, capillary forces counteracting
the gravitational forces decreased, leading to the enhanced downward
displacement of the wetting fluid (here: xylene).
For relatively small saturations of the wetting
phase, this changed behavior resulted in an increased value of $vg_n$.

\begin{figure}[ht]
  \centerline{\epsfig{file=eps/tp_profil950s.eps,width=.5\textwidth}}
    \caption{Profile of temperature and gas-phase pressure in the
             column obtained from the simulation at $t=950$s}
    \protect\label{tp_profil950s}
\end{figure}

\begin{figure}[ht]
    \centerline{\epsfig{file=eps/ssxx_profil950s.eps,
      width=.5\textwidth}}
    \caption{Profile of liquid saturations and gas-phase mole fractions
             of steam and contaminant in the
             column obtained from the simulation at $t=950$s}
    \protect\label{ssxx_profil950s}
\end{figure}

Figs. \ref{tp_profil950s} and \ref{ssxx_profil950s} show the profiles of
the variables temperature, gas-phase pressure, saturations and
gas-phase mole fractions after 950s simulation time. In Fig.
\ref{tp_profil950s}, one can see the sharp temperature front with
the plateau behind, where xylene evaporation still occurs.
The gas-phase pressure gradient is high behind the front. At the front,
all of the vapor condenses; thus, the pressure gradient before the front
is much smaller. Fig. \ref{ssxx_profil950s} illustrates some interesting
effects.
The non-monotone profile of the NAPL saturation illustrates
the recondensation of NAPL at the front. The hill of NAPL
obviously corresponds to the front propagation.
The calibration of a numerical model describing such strongly coupled,
nonlinear processes is hardly feasible by hand (trial-and-error method).
Strong correlations between the model parameters and the large number
of parameters exhibiting significant uncertainties
reduce the reliability of quantitative predictions for real-life
problems. Inverse modeling techniques can make helpful
contributions to a more reliable parameter estimation, assuming
that the measured data are of good quality and reliable (Finsterle, 1999
\cite{itough2}).
In cooperation with Dr. Finsterle (Lawrence Berkeley National
Laboratory), we carried out a parameter
estimation and sensitivity analysis for this experiment, attempting to
identify the essential parameters affecting the temperature
behavior observed during steam flooding. Details are given in
Class (2001) \cite{hollepromoiws}.
The steam-injection rate proved to be a very sensitive quantity; it was
included as a parameter to be estimated even though it was actually
measured. The inverse modeling runs showed that it is difficult to match
experimental data containing sharp fronts (here produced by the
steam front passing a temperature sensor).
These sharp fronts are likely to cause large differences between
experimental and numerical data even for small deviations in the
front-arrival time, thus leading the minimization algorithm to focus
on matching the fronts.
It turned out that the initial parameter set (as described above) is the
most reliable, apart from the fact that the $vg_n$ parameter
deviated.  This is consistent with what we obtained by trial-and-error
matching.  We failed in our attempt to determine the influence of
possibly occurring heat loss by considering the heat capacity and the
heat conductivity of the column wall and the insulation in the
estimated parameter set.  Analysis of these inversions made it clear
that, with the available experimental data, these additional parameters
(describing heat sinks) could not be reasonably
estimated because of the strong correlation between them and
the hydraulic parameters.

The results of the simulation of this column experiment show that
key issues in the modeling of these complex processes are the
determination of the hydraulic constitutive relationships and the
quantification of temperature effects both for heat loss and for the
influence of temperature on the system properties.

\newpage

\subsection{Parameters}
The following (inclomplete) list of model parameters includes some
parameters which could be varied to see how the model reacts on changes.
\begin{verbatim}
permeability            1.400E-11    # [m^2]
vg_alpha                0.0005
vg_n                    4.0     
beta_gn                 2.20 
beta_nw                 1.83
beta_gw                 1.00
enthalpyFlux            -17452.97     # [J/(s m)] 
steamFlux               -0.39571      # [mol/(s m)] 
\end{verbatim}

\subsection{Questions}
\begin{enumerate}
\item Look at the curves which describe the distribution of the NAPL saturation
in the column. How would you explain the area of higher NAPL saturation
("moving hill") proceeding from top to bottom? To explain this effect you
can also take a look at the curves obtained at the sensors showing the
behavior of the NAPL saturation and the temperature over time.
\item How would you explain that two different plateau temperatures
can be observed during the experiment (at 92$^\circ$C and at 100$^\circ$C)?
\item Compare the temperature plateau lengths at 92$^\circ$C of the measured curves and the simulated curves. Explain the differences.
\item Calculate between which limits you can choose the values of the
      steam enthalpy {\it enthalpy\_st} if the steam flux is constant
      ($q$ = -0.395710 $\frac{mol}{sm}$) and not superheated. \\
      This means you have to calculate the enthalpy at which the wet steam area       begins (wet steam/hot water boundary) and the enthalpy at which the
      wet steam area ends (wet steam/superheated steam boundary). Note that
      the heat capacity of superheated steam is comparatively small such that
      the temperature increases rapidly with higher values of
      {\it enthalpy\_st}. \\
      Test your calculated values with the numerical model.
\end{enumerate}

Take care when you plot the results. There is a 'physically meaningful' part of the model domain
as well as the extended domain. You might want to 'clip' that away in paraview.
And then it makes sense to use a 'Plot-Over-Line' to visualize the results.
