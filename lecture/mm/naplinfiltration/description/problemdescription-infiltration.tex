% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\section{Problem description}

In this exercise, the characteristic behavior of NAPL (Non-Aqueous Phase Liquid)
infiltrating through the vadose zone into the the saturated zone can be examined.
The NAPL first enters the unsaturated zone, where it leaves a trace
of 'near-residual' saturation before it reaches the groundwater table.

NAPLs can have a density higher or lower than that of the groundwater.
NAPLs that are denser than water are called \textit{DNAPLs} (Dense Non-Aqueous
Phase Liquids), the lighter ones are \textit{LNAPLs} (Light NAPLs).
While LNAPLs usually pool upon the groundwater table, DNAPLs are able to
penetrate into the saturated zone. Thus, DNAPLs are even more feared than
LNAPLs since this characteristic behavior makes the choice of the remediation
method for a contaminated site rather difficult.

The simulation of the process shows that both LNAPL and DNAPL flow through the porous
medium (Fig. \ref{sickerbild}). When the groundwater table is reached, DNAPL's will
sink even further down into the saturated zone. They pool up at zones of low permeability
(in this case, at the bottom of the domain, where an impermeable boundary is given).
In contrast to that, LNAPL's will pool already on the groundwater table.

The following set-up is imposed:
\begin{figure}[h]
  \centerline{\epsfig{file=./eps/Gebiet.eps,width=0.67\textwidth}}
  \label{sickerbild}
  \caption{Model setup.}
\end{figure}

In order to get an idea of parameter sensitivities, it is useful 
to vary some parameters and observe the differences in the model output.
Since parameters can also strongly correlate, it makes sense to change
only one parameter at a time and procede step by step.
Recommended parameters for a variation in this example problem are
the absolute permeability of the porous medium, the porosity, and the
magnitude of the capillary pressure via the \textit{van Genuchten}
parameter $\alpha$. Additionally, one might choose a DNAPL and
see the different behavior in contrast to an LNAPL (default choice). \\

\section{Exercises}

\begin{itemize}
\item Compile and run the 3p and 3p3c problems. Visualize the results with paraview and compare them.
      Have a look in particular on the NAPL saturations in the 3p compared with the 3p3c.
      Then look at the NAPL saturations in the 3p3c example and compare their contour shapes with the those of the 
      NAPL (contaminant) mole fractions
      in the gas and in the liquid phase. Try to understand their different extensions and think
      about the role of equilibrium assumptions in the model. Note that at any place where the
      liquid phase is present, the corresponding vapor is in this model in a state of equilibrium according
      to the saturation vapor pressure, the letter is a function of temperature.
\item Next, we want to investigate the influence of some parameters. In the input files
      you can modify permeability, porosity, capillary pressure parameters, NAPL density
      and NAPL viscosity. \\
      Check out the influence of those parameters by modifying them in \underline{reasonable} 
      ranges. E.g. permeabilities can be changed by one order of magnitude, while density should
      not vary too much. To model a 'DNAPL' you need, of course, a density larger than that of
      water. Try to get that.
\end{itemize}
