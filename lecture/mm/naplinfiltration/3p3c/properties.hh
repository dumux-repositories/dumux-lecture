// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Isothermal NAPL infiltration problem: LNAPL contaminates
 *        the unsaturated and the saturated groundwater zone.
 */
#ifndef DUMUX_NAPLINFILTRATIONPROPERTIES_3P_3C_HH
#define DUMUX_NAPLINFILTRATIONPROPERTIES_3P_3C_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/3p3c/model.hh>
#include <dumux/material/fluidsystems/h2oairmesitylene.hh>

#include <lecture/mm/naplinfiltration/3p3c/myh2oairmesitylene.hh>
#include <lecture/mm/naplinfiltration/spatialparams.hh>
#include "problem.hh"

namespace Dumux::Properties
{

// Create new type tags
namespace TTag {
struct InfiltrationThreePThreeCTypeTag { using InheritsFrom = std::tuple<ThreePThreeC>; };
struct InfiltrationThreePThreeCBoxTypeTag { using InheritsFrom = std::tuple<InfiltrationThreePThreeCTypeTag, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::InfiltrationThreePThreeCTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::InfiltrationThreePThreeCTypeTag> { using type = InfiltrationThreePThreeCProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::InfiltrationThreePThreeCTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = InfiltrationSpatialParams<FVGridGeometry, Scalar>;
};

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::InfiltrationThreePThreeCTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::MyH2OAirMesitylene<Scalar>;
};

} // end namespace Dumux::Properties

#endif
