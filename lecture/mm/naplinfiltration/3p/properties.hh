// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Isothermal NAPL infiltration problem: L- or DNAPL contaminates
 *        the unsaturated and the saturated groundwater zone.
 */
#ifndef DUMUX_NAPLINFILTRATIONPROPERTIES_3P_HH
#define DUMUX_NAPLINFILTRATIONPROPERTIES_3P_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/3p/model.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/fluidsystems/3pimmiscible.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/air.hh>
#include "mymesitylene.hh"
#include <dumux/material/components/h2o.hh>

#include <lecture/mm/naplinfiltration/spatialparams.hh>
#include "problem.hh"

namespace Dumux::Properties
{
// Create new type tags
namespace TTag {
struct InfiltrationThreePTypeTag { using InheritsFrom = std::tuple<ThreeP>; };
struct InfiltrationThreePBoxTypeTag { using InheritsFrom = std::tuple<InfiltrationThreePTypeTag, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::InfiltrationThreePTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::InfiltrationThreePTypeTag> { using type = InfiltrationThreePProblem<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::InfiltrationThreePTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Water = Components::TabulatedComponent<Components::H2O<Scalar>>;
    using WettingFluid = FluidSystems::OnePLiquid<Scalar, Water>;
    using NonwettingFluid = FluidSystems::OnePLiquid<Scalar, Components::MyMesitylene<Scalar>>;
    using Gas = FluidSystems::OnePGas<Scalar, Components::Air<Scalar>>;
public:
    using type = FluidSystems::ThreePImmiscible<Scalar, WettingFluid, NonwettingFluid, Gas>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::InfiltrationThreePTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = InfiltrationSpatialParams<FVGridGeometry, Scalar>;
};

} // end namespace Dumux::Properties

#endif
