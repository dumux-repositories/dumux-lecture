% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\section{Problem description}

Fig. \ref{kuevette} shows the setup of a quasi 2D experiment that was
carried out a few years ago in the VEGAS (Versuchseinrichtung zur Grundwasser-
und Altlastensanierung) research facility at the University of Stuttgart.

\begin{figure}[h]
  \centerline{\epsfig{file=./eps/kuev_exp_init_sw.eps,width=0.67\textwidth}}
  \label{kuevette}
  \caption{Setup of the 2D VEGAS experiment.}
\end{figure}

A NAPL contamination is placed partly into a fine sand lense and partly into
a coarse sand region. The NAPL saturation is below residual. Thus the NAPL
is immobile. This is a typical situation one can have when NAPL has infiltrated
through the unsaturated zone and reaches a low permeable lense where further
downward migration is prevented. The aim of this exercise is to show how
different remediation techniques cause different behavior. This concerns
both the processes and the time-scale on which the remediation takes place.

Fig. \ref{kuevfoto} shows, for example, a photograph of the experiment with
injection of almost pure steam.

\begin{figure}[h]
  \centerline{\epsfig{file=./eps/img0010.eps,width=0.67\textwidth}}
  \label{kuevfoto}
  \caption{Photograph of the experiment after 1:15~h steam injection.}
\end{figure}

The picture shows the characteristic behavior; NAPL (re-)condenses
at the steam/condensation front. There, it accumulates and with increasing
liquid saturation, the NAPL phase flows due to gravity further downward.
Thus, even more NAPL is trapped in the fine sand lense, where it is difficult
to remove. \\
It is desirable to inject a lot of energy to evaporate the contaminant. On the
other hand, one should avoid that a lot of liquid NAPL is shifted towards
low permeable regions. The solution is to add some air to the injected fluid.
Air cannot condense. Thus, the air can always transport some evaporated contaminant
out of the domain. Unfortunately, the air has a low heat capacity. Thus it cools
down soon. Nevertheless, this might be sufficient to broaden the condensation
front and to limit the saturation values of liquid NAPL to avoid too high
mobilities. \\
Check this out in this exercise and model three different remediation scenarios
as explained below. 

Observe the different time-scales on which the remediation of the contamination
occurs.

\subsection{Exercises and questions}
\begin{enumerate}
\item Compile and run the three different remediation scenarios
      as explained below. Switching from one scenario to another can be done in 
      the input file.
\item Compare the different time-scales on which the remediation of the contamination
      occurs.
\item Is it possible to prevent the infiltration of recondensed liquid NAPL into
      the fine-sand lense by injecting a steam-air mixture? \\
      Explain why. What might be practical problems in field-scale application?
\end{enumerate}


Initial conditions for all three remediation scenarios:
\begin{itemize}
\item Gas phase pressure: $p_g = 1.013 \cdot 10^5$ Pa
\item Temperature: $T = 20^\circ$C
\item Water saturation: $S_w = 0.11$ (little less than residual saturation $S_{wr} = 0.12$)
\item NAPL (mesitylene) saturation $S_n = S_{nr} = 0.07$ in the contaminated area
\end{itemize}

Use the same total molar injection rate of 0.3435~mol/(s~m) at the left boundary.
The molar composition of the injected fluid and the enthalpy flux vary for each 
scenario:

\begin{tabbing}
\hspace{7.0cm} \= \hspace{5.0cm} \= \kill
1. Air ($\approx$~20$^\circ$C) \> air/water: 0.3355/0.008 \> $q_h = 1353$ J/(s m) \\
2. Steam ($\approx$~100$^\circ$C) \> air/water: 0/0.3435 \> $q_h = 15150$ J/(s m) \\
3. Steam/air mixture ($\approx$~75$^\circ$C) \> air/water: 0.2/0.1435 \> $q_h =$ 6929 J/(s m) \\
\end{tabbing}
