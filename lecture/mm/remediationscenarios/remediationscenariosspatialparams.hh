// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Definition of the spatial parameters for the kuevette problem.
 */
#ifndef DUMUX_KUEVETTE3P3CNI_SPATIAL_PARAMS_HH
#define DUMUX_KUEVETTE3P3CNI_SPATIAL_PARAMS_HH

#include <dune/common/float_cmp.hh>

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/3p/parkervangenuchten.hh>

namespace Dumux {

/*!
 * \brief Definition of the spatial parameters for the kuevette problem
 */
template<class TypeTag>
class KuevetteSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GetPropType<TypeTag, Properties::GridGeometry>, GetPropType<TypeTag, Properties::Scalar>, KuevetteSpatialParams<TypeTag>>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<FVGridGeometry, Scalar, KuevetteSpatialParams<TypeTag>>;

    using ThreePhasePcKrSw = FluidMatrix::ParkerVanGenuchten3PDefault<Scalar>;

public:
    using PermeabilityType = Scalar;

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    KuevetteSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    , pcKrSwCurveFine_("SpatialParams.Fine")
    , pcKrSwCurveCoarse_("SpatialParams.Coarse")
    {
        // intrinsic permeabilities
        fineK_ = getParam<Scalar>("SpatialParams.Fine.Permeability");
        coarseK_ = getParam<Scalar>("SpatialParams.Coarse.Permeability");

        // porosities
        finePorosity_ = getParam<Scalar>("SpatialParams.Fine.Porosity");
        coarsePorosity_ = getParam<Scalar>("SpatialParams.Coarse.Porosity");
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$
     * \note  It is possibly solution dependent.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const auto& globalPos = scv.dofPosition();
        if (isFineMaterial_(globalPos))
            return fineK_;
        return coarseK_;
    }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     */

    template<class SolidSystem>
    Scalar inertVolumeFractionAtPos(const GlobalPosition& globalPos,
                                    int compIdx) const
    {
        if (compIdx == SolidSystem::comp0Idx)
        {
            if (isFineMaterial_(globalPos))
                return 1-finePorosity_;
            else
                return 0;
        }
        else
        {
            if (isFineMaterial_(globalPos))
                  return 0;
            else
                return 1-coarsePorosity_;
        }
    }

    /*!
     * \brief Returns the fluid-matrix interaction law (kr-sw, pc-sw, etc.).
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                 const SubControlVolume& scv,
                                 const ElementSolution& elemSol) const
    {
        const auto& globalPos = scv.dofPosition();
        if (isFineMaterial_(globalPos))
            return makeFluidMatrixInteraction(pcKrSwCurveFine_);
        else
            return makeFluidMatrixInteraction(pcKrSwCurveCoarse_);
    }

private:
    bool isFineMaterial_(const GlobalPosition &globalPos) const
    {
        return (0.13 < globalPos[0] + eps_ && 1.20 > globalPos[0] - eps_ &&
                0.32 < globalPos[1] + eps_ && globalPos[1] < 0.57 - eps_);
    }

    Scalar fineK_;
    Scalar coarseK_;

    Scalar finePorosity_;
    Scalar coarsePorosity_;

    const ThreePhasePcKrSw pcKrSwCurveFine_;
    const ThreePhasePcKrSw pcKrSwCurveCoarse_;

    static constexpr Scalar eps_ = 1.5e-7;
};

} // end namespace Dumux

#endif
