// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Non-isothermal gas injection problem where a gas (e.g. steam/air)
 *        is injected into a unsaturated porous medium with a residually
 *        trapped NAPL contamination.
 */
#ifndef DUMUX_REMEDIATIONSCENARIOS_PROPERTIES_HH
#define DUMUX_REMEDIATIONSCENARIOS_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/material/fluidsystems/h2oairmesitylene.hh>
#include <dumux/material/solidstates/compositionalsolidstate.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/3p3c/model.hh>

#include "remediationscenariosspatialparams.hh"
#include "remediationscenariosproblem.hh"

#define ISOTHERMAL 0

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct KuevetteTypeTag { using InheritsFrom = std::tuple<ThreePThreeCNI>; };
struct KuevetteBoxTypeTag { using InheritsFrom = std::tuple<BoxModel, KuevetteTypeTag>; };
struct KuevetteCCTpfaTypeTag { using InheritsFrom = std::tuple<CCTpfaModel, KuevetteTypeTag>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::KuevetteTypeTag> { using type = Dune::YaspGrid<2>; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::KuevetteTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::KuevetteTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::KuevetteTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::KuevetteTypeTag> { static constexpr bool value = true; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::KuevetteTypeTag> { using type = KuevetteProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::KuevetteTypeTag> { using type = KuevetteSpatialParams<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::KuevetteTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::H2OAirMesitylene<Scalar>;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::KuevetteTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Dumux::Components::Constant<1, Scalar>;
    using ComponentTwo = Dumux::Components::Constant<2, Scalar>;
    static constexpr int numInertComponents = 2;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo, numInertComponents>;
};

template<class TypeTag>
struct SolidState<TypeTag, TTag::KuevetteTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
public:
    using type = CompositionalSolidState<Scalar, SolidSystem>;
};

} //end namespace Dumux::Properties

#endif
