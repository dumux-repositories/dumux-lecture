// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Non-isothermal gas injection problem where a gas (e.g. steam/air)
 *        is injected into a unsaturated porous medium with a residually
 *        trapped NAPL contamination.
 */
#ifndef DUMUX_REMEDIATIONSCENARIOS_PROBLEM_HH
#define DUMUX_REMEDIATIONSCENARIOS_PROBLEM_HH

#include <dune/common/float_cmp.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \brief Non-isothermal gas injection problem where a gas (e.g. steam/air)
 *        is injected into a unsaturated porous medium with a residually
 *        trapped NAPL contamination.
 *
 * The domain is a quasi-two-dimensional container (kuevette). Its dimensions
 * are 1.5 m x 0.74 m. The top and bottom boundaries are closed, the right
 * boundary is a Dirichlet boundary allowing fluids to escape. From the left,
 * an injection of a hot water-air mixture is applied (Neumann boundary condition
 * for the mass components and the enthalpy), aimed at remediating an initial
 * NAPL (Non-Aquoeus Phase Liquid) contamination in the heterogeneous domain.
 * The contamination is initially placed partly into the coarse sand
 * and partly into a fine sand lense.
 *
 * This simulation can be varied through assigning different boundary conditions
 * at the left boundary as described in Class (2001):
 * Theorie und numerische Modellierung nichtisothermer Mehrphasenprozesse in
 * NAPL-kontaminierten por"osen Medien, Dissertation, Eigenverlag des Instituts
 * f"ur Wasserbau
 *
 * This problem uses the \ref ThreePThreeCModel and \ref NIModel model.
 *
 * To see the basic effect and the differences to scenarios with pure steam or
 * pure air injection, it is sufficient to simulated for about 2-3 hours (10000 s).
 * Complete remediation of the domain requires much longer (about 10 days simulated time).
 * To adjust the simulation time it is necessary to edit the input file.
 *
 * To run the simulation execute:
 * <tt>./test_box3p3cnikuevette test_box3p3cnikuevette.input</tt> or
 * <tt>./test_cc3p3cnikuevette test_cc3p3cnikuevette.input</tt>
 */
template <class TypeTag >
class KuevetteProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    enum {

        pressureIdx = Indices::pressureIdx,
        switch1Idx = Indices::switch1Idx,
        switch2Idx = Indices::switch2Idx,
        temperatureIdx = Indices::temperatureIdx,
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::wCompIdx,
        contiGEqIdx = Indices::conti0EqIdx + FluidSystem::gCompIdx,
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::nCompIdx,
        energyEqIdx = Indices::energyEqIdx,

        // phase states
        threePhases = Indices::threePhases,
        wgPhaseOnly = Indices::wgPhaseOnly,

        // world dimension
        dimWorld = GridView::dimensionworld
    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GlobalPosition = typename SubControlVolumeFace::GlobalPosition;


public:
    /*!
     * \brief The constructor.
     */
    KuevetteProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        FluidSystem::init();
        int isSteam = getParam<bool>("Problem.SteamScenario") ? 1 : 0;

        int isSteamAir = getParam<bool>("Problem.SteamAirScenario") ? 1 : 0;
        int isAir = getParam<bool>("Problem.AirScenario") ? 1 : 0;

        // error checking
        if (isSteam + isSteamAir + isAir > 1)
            DUNE_THROW(Dune::InvalidStateException, "Please choose a single scenario!");

        if (isSteam == 1)
        {
            waterFlux_ = getParam<Scalar>("Problem.WaterFluxS"); // 0.3435 [mol/(s m)] in total
            airFlux_ = getParam<Scalar>("Problem.AirFluxS");
            contaminantFlux_ = getParam<Scalar>("Problem.ContaminantFluxS");
            heatFlux_ = getParam<Scalar>("Problem.HeatFluxS");
            name_ = "remediation-steam";
        }
        else if (isSteamAir == 1)
        {
            waterFlux_ = getParam<Scalar>("Problem.WaterFluxSA"); // 0.3435 [mol/(s m)] in total
            airFlux_ = getParam<Scalar>("Problem.AirFluxSA");
            contaminantFlux_ = getParam<Scalar>("Problem.ContaminantFluxSA");
            heatFlux_ = getParam<Scalar>("Problem.HeatFluxSA");
            name_ = "remediation-steamair";
        }
        else if (isAir == 1)
        {
            waterFlux_ = getParam<Scalar>("Problem.WaterFluxA"); // 0.3435 [mol/(s m)] in total
            airFlux_ = getParam<Scalar>("Problem.AirFluxA");
            contaminantFlux_ = getParam<Scalar>("Problem.ContaminantFluxA");
            heatFlux_ = getParam<Scalar>("Problem.HeatFluxA");
            name_ = "remediation-air";
        }
        else
        {
            // free scenario
            waterFlux_ = getParam<Scalar>("Problem.WaterFlux"); // 0.3435 [mol/(s m)] in total
            airFlux_ = getParam<Scalar>("Problem.AirFlux");
            contaminantFlux_ = getParam<Scalar>("Problem.ContaminantFlux");
            heatFlux_ = getParam<Scalar>("Problem.HeatFlux");
            name_ = "remediation";
        }
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }


    NumEqVector sourceAtPos( const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        if(globalPos[0] > 1.5 - eps_)
            bcTypes.setAllDirichlet();
        else
            bcTypes.setAllNeumann();
         return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     *
     */
    PrimaryVariables dirichletAtPos( const GlobalPosition &globalPos) const
    {
       return initial_(globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub control volume face
     *
     * Negative values mean influx.
     * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element &element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scvf.ipGlobal();

        // negative values for injection
        if (globalPos[0] > 0.01 + eps_)
        {
            values[contiWEqIdx] = 0.0; // 0.3435 [mol/(s m)] in total
            values[contiGEqIdx] = 0.0;
            values[contiNEqIdx] = 0.0;
            values[energyEqIdx] = 0.0;
        }
        else
        {
            values[contiWEqIdx] = waterFlux_;
            values[contiGEqIdx] = airFlux_;
            values[contiNEqIdx] = contaminantFlux_;
            values[energyEqIdx] = heatFlux_;
        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     */
    PrimaryVariables initialAtPos( const GlobalPosition &globalPos) const
    {
       return initial_(globalPos);
    }

private:
    // checks whether a point is located inside the contamination zone
    bool isInContaminationZone(const GlobalPosition &globalPos) const
    {
        return (Dune::FloatCmp::ge<Scalar>(globalPos[0], 0.2)
               && Dune::FloatCmp::le<Scalar>(globalPos[0], 0.8)
               && Dune::FloatCmp::ge<Scalar>(globalPos[1], 0.4)
               && Dune::FloatCmp::le<Scalar>(globalPos[1], 0.65));
    }
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        if (isInContaminationZone(globalPos))
            values.setState(threePhases);
        else
            values.setState(wgPhaseOnly);

        values[pressureIdx] = 1e5 ;
        values[switch1Idx] = 0.12;
        values[switch2Idx] = 1.e-6;
        values[temperatureIdx] = 293.15;

        if (isInContaminationZone(globalPos))
            values[switch2Idx] = 0.07;

        return values;
    }

    static constexpr Scalar eps_ = 1.5e-7;
    std::string name_;
    Scalar waterFlux_, airFlux_, contaminantFlux_, heatFlux_;
    std::vector<Scalar> Kxx_;
};

} //end namespace Dumux

#endif
