// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerTests
 * \brief The properties of the radioactive tracer test.
 */
#ifndef DUMUX_LFRITRACER_TEST_PROPERTIES_HH
#define DUMUX_LFRITRACER_TEST_PROPERTIES_HH

#include <dune/grid/uggrid.hh>

#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/fluidmatrixinteractions/dispersiontensors/scheidegger.hh>
#include "fluidsystem_tracer_i129.hh"

#include "landfillmodel_tracer.hh"
#include "landfillspatialparams_tracer.hh"
#include "landfillproblem_tracer.hh"

namespace Dumux::Properties {
//Create new type tags
namespace TTag {
struct LFRiTracerTest { using InheritsFrom = std::tuple<TracerSorption>; };
struct LFRiTracerTestTpfa { using InheritsFrom = std::tuple<LFRiTracerTest, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::LFRiTracerTest> { using type = Dune::UGGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::LFRiTracerTest> { using type = LFRiTracerTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::LFRiTracerTest>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = LFRiTracerTestSpatialParams<GridGeometry, Scalar>;
};

template<class TypeTag>
struct CompositionalDispersionModel<TypeTag, TTag::LFRiTracerTest>
{ using type = ScheideggersDispersionTensor<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::LFRiTracerTest> { using type = FluidSystems::TracerFluidSystem<TypeTag>; };

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::LFRiTracerTest> { static constexpr bool value = true; };
template<class TypeTag>
struct SolutionDependentMolecularDiffusion<TypeTag, TTag::LFRiTracerTestTpfa> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableCompositionalDispersion<TypeTag, TTag::LFRiTracerTest> { static constexpr bool value = true; };

} // end namespace Properties

#endif
