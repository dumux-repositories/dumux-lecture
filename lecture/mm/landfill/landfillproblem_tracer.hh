// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerTests
 * \brief  A problem with a radioactive tracer
 */
#ifndef DUMUX_LFRITRACER_TEST_PROBLEM_HH
#define DUMUX_LFRITRACER_TEST_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "landfillspatialparams_tracer.hh"

namespace Dumux {
/*!
 * \ingroup TracerTests
 * \brief A problem with a radioactive tracer
 */
template <class TypeTag>
class LFRiTracerTestProblem;

/*!
 * \ingroup TracerTests
 * \brief A problem with a radioactive tracer
 */
template <class TypeTag>
class LFRiTracerTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using TracerSolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;

    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
    static constexpr bool useDispersion = getPropValue<TypeTag, Properties::EnableCompositionalDispersion>();
    static constexpr int numComponents = FluidSystem::numComponents;
    enum {
        dim=GridView::dimension,
        conti0EqIdx = Indices::transportEqIdx,
    };

public:
    LFRiTracerTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        // stating in the console if dispersion is used
        if(useDispersion)
            std::cout<<"problem uses dispersion" << '\n';
        // stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions" << '\n';
        else
            std::cout<<"problem uses mass fractions" << '\n';

        initialValue_ = getParam<Scalar>("Problem.InitialTracerValue", 1e0);
        useKdModel_ = getParam<bool>("Problem.UseKdModel", false);
        if(useKdModel_)
            std::cout<<"problem uses Kd model" << '\n';
        solidBulkDensity_ = getParam<Scalar>("Problem.SolidBulkDensity", 1500);
    }

    void setTimeStepSize( Scalar timeStepSize )
    {
        timeStepSize_ = timeStepSize;
    }

    /*!
     * \brief Evaluates the source term at a given position.
     * The radioactive decay is given as source term
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scvf The sub-control volume faceolVars.massFraction(
     */
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        const auto& volVars = elemVolVars[scv];
        std::array<Scalar, numComponents> qRadDecay = {0.0};

        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if(!FluidSystem::isRadioactive(compIdx))
                continue;

            qRadDecay[compIdx] = useMoles ? - volVars.moleFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * volVars.molarDensity(0)
                                    * volVars.porosity()
                                    * volVars.saturation(0)
                                 : - volVars.massFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * volVars.density(0)
                                    * volVars.porosity()
                                    * volVars.saturation(0);
            if(useKdModel_)
                qRadDecay[compIdx] += useMoles ? - volVars.moleFraction(0, compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * FluidSystem::kdValue(compIdx)
                                    * volVars.molarDensity(0)
                                    * solidBulkDensity_
                                 : - volVars.massFraction(0, compIdx)
                                    * FluidSystem::kdValue(compIdx)
                                    * FluidSystem::decayRate(compIdx)
                                    * volVars.density(0)
                                    * solidBulkDensity_;

            if (FluidSystem::decayDaughter(compIdx) != "stable") //checking if component has a daughter
            {
                unsigned int compDIdx = getDaughterName(FluidSystem::decayDaughter(compIdx)); //name and index of the component are determined
                qRadDecay[compDIdx] += -qRadDecay[compIdx];

                const unsigned int eqDIdx = conti0EqIdx + compDIdx;
                source[eqDIdx] += qRadDecay[compDIdx];
            }

            const unsigned int eqIdx = conti0EqIdx + compIdx;
            source[eqIdx] += qRadDecay[compIdx];
        }

        return source;
    }

    /*!
     * \brief Determines the name of the daughter after the radioactive decay.
     * The name of the daughter is compared with the component name in the system.
     *
     * \param daughter Name of the daughter
     */
    int getDaughterName(const std::string daughter) const
    {
        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            if(FluidSystem::componentName(compIdx) == daughter)
                return compIdx;
            else
                continue;
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid daughter name: " << daughter);
    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if(onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }
    // \}

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        return values;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        if (onRightBoundary_(globalPos))
            values = 0.0;

        return values;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables initialValues(0.0);
        if(isLandfill_(globalPos))
        {
            initialValues[conti0EqIdx] = initialValue_;
        }
        return initialValues;
    }

    /*!
     * \brief Returns the time for a variable neumann boundary condition.
     *
     */
    void setTime(Scalar time)
    {
        time_ = time;
    }

private:
    static constexpr Scalar eps_ = 1e-6;

    bool useKdModel_;
    Scalar time_;
    Scalar timeStepSize_;

    Scalar initialValue_;
    Scalar solidBulkDensity_;
    std::vector<std::vector<Scalar>> vtkBqPerL_;
    std::vector<std::vector<Scalar>> vtkBqPerG_;

    bool isLandfill_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > 1+5 - eps_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[dim-1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[dim-1] - eps_;
    }
};

} //end namespace Dumux

#endif
