// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \ingroup RichardsTests
 * \brief The flow problem using the Richards-equation for a radioactive tracer test.
 */
#ifndef DUMUX_LFRICHARDSFLOW_TEST_PROBLEM_HH
#define DUMUX_LFRICHARDSFLOW_TEST_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "landfillspatialparams_ri.hh"

namespace Dumux {
template<class TypeTag> class RichardsFlowTestProblem;

/*!
 * \ingroup RichardsTests
 * \brief The flow problem of a radioactive 2p test.
 */
template<class TypeTag>
class RichardsFlowTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using PcKrSwCurve = typename ParentType::SpatialParams::PcKrSwCurve;
    enum {
        dim=GridView::dimension,
        pressureIdx = Indices::pressureIdx,
        conti0EqIdx = Indices::conti0EqIdx + FluidSystem::H2OIdx,
    };

public:
    RichardsFlowTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        hasAnnualRainCycle_ = getParam<bool>("Problem.HasAnnualRainCycle", false);
        injectionMass_ = getParam<Scalar>("Problem.InjectionMass", -0.0);
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    template<class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scvf.ipGlobal();
        if (onUpperBoundary_(globalPos))
        {
            if(hasAnnualRainCycle_)
                values[conti0EqIdx] = (sin(2*M_PI*time_/31536000.0)+1)*injectionMass_;
            else
                values[conti0EqIdx] = injectionMass_;
        }
        else if (onLeftBoundary_(globalPos) && isAquifer_(globalPos))
        {
            values[conti0EqIdx] = -1.27e-2; //400m/y in kg/s*m^2
        }
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub-control volume face
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.ipGlobal();

        if (onRightBoundary_(globalPos))
        {
            const Scalar sw = 1.0;
            const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);
            values[pressureIdx] = nonwettingReferencePressure() - pc;
        }

        return values;
    }

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param element The finite element
     */
    PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos =  element.geometry().center();
        if (isAquifer_(globalPos))
        {
            const Scalar sw = 1.0;
            const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);
            values[pressureIdx] = nonwettingReferencePressure() - pc ;
        }
        else
        {
            const Scalar sw = this->spatialParams().swr(element);
            const Scalar pc = this->spatialParams().fluidMatrixInteraction(element).pc(sw);
            values[pressureIdx] = nonwettingReferencePressure() - pc;
        }

        return values;
    }

    /*!
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     */
    Scalar nonwettingReferencePressure() const
    { return 1.0e5; };

    /*!
     * \brief Returns the time for a variable neumann boundary condition.
     *
     */
    void setTime(Scalar time)
    {
        time_ = time;
    }

private:
    bool isAquifer_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < 1 + eps_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < this->gridGeometry().bBoxMin()[dim-1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > this->gridGeometry().bBoxMax()[dim-1] - eps_;
    }

    static constexpr Scalar eps_ = 1e-6;

    Scalar time_;
    Scalar injectionMass_;
    bool hasAnnualRainCycle_;
};

} // end namespace Dumux

#endif
