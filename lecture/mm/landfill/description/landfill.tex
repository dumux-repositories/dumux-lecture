% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\documentclass[11pt,a4paper]{article}
%\usepackage{german}
\usepackage{pslatex}
\usepackage[english]{babel}
\usepackage{latexsym}
\usepackage{fancyvrb}
\usepackage{fancyhdr}
%\usepackage{psfig}
%\usepackage{epsfig}
%\usepackage{epsf}
\usepackage[dvips]{color}
\usepackage{hyperref}
%\usepackage{makeidx}
\usepackage{listings}
\usepackage{lscape}
\usepackage[pdftex]{graphicx}

% ---- Einstellungen ----------------------------------------
\textwidth 16.5cm
\textheight 23.5cm
\topmargin 0.0pt
\oddsidemargin 0.0pt
\evensidemargin 0.0pt
\setlength{\parindent}{10pt}
\setlength{\unitlength}{1cm}
\selectlanguage{english}
\newcommand{\Pictpath}{pics}
\pagestyle{fancy}

%---- pagestyle ---- start -------------------------------------
\lhead[\fancyplain{}{\thepage}] %Header links,  gerade Seitenzahl
%      {\fancyplain{}{EGW Short Course, March 2002}}
      {\fancyplain{}{MM Exercise}}
\rhead[\fancyplain{}{Multiphase Modeling}]
      {\fancyplain{}{\thepage}} %Header rechts, ungerade Seitenzahl
\cfoot{}
%---- pagestyle ---- ende --------------------------------------


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\sloppy                  % groessere Wortabstaende

{\Large
\begin{center}
{\bfseries MM Exercise\\
           Landfill Problem}\\
\end{center}
}

%*************************************
\section*{Problem description}
\label{problem}

When dismantling nuclear power plants, only a very small part is heavily contaminated with radioactivity and must be stored in a disposal site. The remaining material, mainly broken concrete (building rubble), is decontaminated if necessary and released. After release, the material is stored in conventional surface landfills. The radioactive doses are so low that the material is legally considered non-radioactive. However, rainfall and heavy rainfall events can transport radioactive particles into the subsurface and ultimately lead to a radiation dose in humans.

\begin{figure}[h]\centering
\includegraphics[width=0.65\linewidth]{pics/Merk_model.png}
    \caption{landfill setup after \cite{Merk2012}}
    \protect\label{Merk_model}
\end{figure}

Here we investigate an example of such a landfill. Fig.~\ref{Merk_model} shows the setup of a landfill as proposed by \cite{SR44, Merk2012}. On the top we have the landfill with inflowing rain. Below that is the vadose zone and on the bottom the aquifer.\\ 
Here we use a quasi-1D approach, similar to the procedure proposed in \cite{SR44, Merk2012}. First we model the vertical flow of the rain to the aquifer, then the horizontal flow through the aquifer to our observation point, resulting in an big L.\\

For this example we decouple the water flow from the transport of the radionuclides. For the water flow we use the Richards-equation:
\begin{equation}
    \frac{\partial (\phi \rho_{w} S_{w})}{\partial t}\
    +\ \nabla \cdot (\rho_{w} \mathbf{v}_{w})\ -\ \rho_{w} q_{w} =\ 0 \;.
\label{eq:richards}
\end{equation}
The Richards equation is an often used simplification of the two-phase equation for water and air.It can be used for case that the non-wetting phase is significantly more mobile than the wetting phase. Then the air phase is neglected and only the equation for the water phase is solved. This reduces the equations to be solved, but brings with it the need to determine the water saturation via the inverse of the $p_c$-$S_w$-relationship. Especially at high saturations(steep gradient in the inverse) this can lead to a high amount of Newton steps.\\
The calculated velocities and saturations are then used to model the radionuclide transport using a tracer model:
\begin{equation}
    \phi \frac{ \partial \varrho X^\kappa}{\partial t} - \nabla \cdot \left( \varrho X^\kappa \mathbf{v}_{f} + \varrho D^\kappa_{pm} \nabla X^\kappa \right) = q.
 \label{eq:tracer}
\end{equation}
$D^\kappa_{pm}$ is reffering here to the dispersion, which is realized with the Scheidegger approach:
\begin{equation}
D = \alpha_L v_L
\end{equation}
Because radionuclides are usually ionic charged, they interact via sorption with the porous medium. Here we extend Eq.~\ref{eq:tracer} with the very simple and widely used Kd-approach.
\begin{equation}
X_s = K_d \cdot X_l
\label{eq:sorption}
\end{equation}
It describes the equilibrium of the concentration in the liquid phase $l$ and the solid phase $s$.\\
We also have to include the radioactive decay, which is described with the differential equation:
\begin{equation}
\frac{dN}{dt} = N\lambda,
\label{eq:raddecay}
\end{equation}
with the decay rate $\lambda\ =\ \frac{log(2)}{t_{1/2}}$. We include this as a source term and end up with Eq.~\ref{eq:tracerwithKdDecay}:
\begin{equation}
     \frac{ \partial( \phi \varrho_w X^\kappa_l + \varrho_b X^\kappa_s)}{\partial t} - \nabla \cdot \left( \varrho_w X^\kappa_l \mathbf{v}_{f} + \varrho_w D^\kappa_{pm} \nabla X^\kappa_l \right) = \lambda \phi X^\kappa_l + \lambda \varrho_b X^\kappa_s .
 \label{eq:tracerwithKdDecay}
\end{equation}
$\kappa$ refers here to the radioactive component. We also have two densities, one for the water $\varrho_w$ and for the bulk $\varrho_b$. For the solid phase the bulk density is used, because the Kd-value is determined in respect to that.\\
Therefore we have \verb|properties|, \verb|problem| and \verb|spatialparams| files for each the Richards- and the tracer-model.

\section*{Exercise}
\label{exercise}
\begin{itemize}
\item Compile and run the problem. Look at the results in paraview.
\item Check out the realization of the radioactive decay in the source term in \verb|landfillproblem_tracer.hh|. Try to assign the parameters in Eq.~\ref{eq:tracerwithKdDecay} to the parameters in the code.\\ 
What is it about the \verb|getDaughterName| function and why is it necessary?
\item Then play around with parameters for: Half-life, Dispersion, Kd-value. You can also try to change the modelled radionuclide by changing the tracer fluidsystem. Don't forget to change the output name in \verb|landfill.input|. \item Compare the results with each other. Use the Paraview functions PlotOverLine in the landfill and PlotOvertime at a point of your choice. For PlotOverTime select a cell (toggle select cells on below the Layout tab) and go to "Edit" $\rightarrow$ "Find Data", where you can choose PlotOverTime for the selected cell.\\ 
Did the results fit your expectations?

\end{itemize}

%% BIBLIOGRAPHY
\bibliographystyle{plain}
\bibliography{bibliography.bib}

\end{document}