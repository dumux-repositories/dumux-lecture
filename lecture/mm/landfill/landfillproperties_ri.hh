// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup RichardsTests
 * \brief The properties using the Richards-equation for a radioactive tracer test.
 */
#ifndef DUMUX_LFRICHARDSFLOW_TEST_PROPERTIES_HH
#define DUMUX_LFRICHARDSFLOW_TEST_PROPERTIES_HH

#include <dune/grid/uggrid.hh>

#include <dumux/common/properties.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/box.hh>

#include <dumux/porousmediumflow/richards/model.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include "landfillspatialparams_ri.hh"
#include "landfillproblem_ri.hh"

namespace Dumux::Properties {
// Create new type tags
namespace TTag {
struct RichardsFlow { using InheritsFrom = std::tuple<Richards>; };
struct RichardsFlowTpfa { using InheritsFrom = std::tuple<RichardsFlow, CCTpfaModel>; };
struct RichardsFlowMpfa { using InheritsFrom = std::tuple<RichardsFlow, CCMpfaModel>; };
struct RichardsFlowBox { using InheritsFrom = std::tuple<RichardsFlow, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::RichardsFlow> { using type = Dune::UGGrid<2>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::RichardsFlow> { using type = RichardsFlowTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::RichardsFlow>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = RichardsFlowTestSpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::RichardsFlow> { static constexpr bool value = true; };
}// end namespace Properties

#endif
