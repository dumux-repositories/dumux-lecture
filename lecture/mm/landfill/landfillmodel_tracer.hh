// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux Project contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup TracerModel
 * \brief Adaption of the fully implicit tracer transport model, incorporating sorption.
 *
 * This model implements a transport of a tracer, where density and viscosity of the
 * fluid phase in which the tracer gets transported are not affected by the tracer.
 * The velocity field is a given spatial parameter.
 * The model is mainly useful for fast computations on given or precomputed
 * velocity fields and thus generally makes sense only in combination with an incompressible
 * one-phase flow velocity field or analytically given / artificial fields. However, reactions
 * between multiple tracer components can be implemented.
 *
 * The transport of the components \f$\kappa \in \{ a, b, c, ... \}\f$ is described by the following equation:
 \f[
 \frac{ \partial (\phi \varrho X^\kappa + K_d^\kappa \varrho_b X^\kappa)}{\partial t}
 + \nabla \cdot \left\lbrace \varrho X^\kappa {\textbf v_f}
 - \varrho D^\kappa_\text{pm} \nabla X^\kappa \right\rbrace = q,
 \f]
* where:
 * * \f$ \phi \f$ is the porosity of the porous medium,
 * * \f$ \varrho \f$ is the fluid mass density,
 * * \f$ \varrho_b \f$ is the bulk solid mass density,
 * * \f$ X^\kappa \f$ is the mass fraction of component \f$ \kappa \f$,
 * * \f$ K_d^\kappa \f$ is the distribution coefficient of component \f$ \kappa \f$,
 * * \f$ \textbf{v}_f \f$ is the velocity of the fluid,
 * * \f$ {\bf D_{pm}^\kappa} \f$ is the effective diffusivity in the porous medium,
 * * \f$ q \f$ is a source or sink term.
 *
 * The model is able to use either mole or mass fractions. The property useMoles can be set to either true or false in the
 * problem file. Make sure that the according units are used in the problem setup. useMoles is set to true by default.
 *
 * The primary variables the mole or mass fraction of dissolved components \f$x\f$.
 * Note that the tracer model is always considered non-isothermal.
 * The velocity output is fully compatible with the tracer model if you want to write the velocity field to vtk.
*/


#ifndef DUMUX_LECTURE_TRACERSORPTION_MODEL_HH
#define DUMUX_LECTURE_TRACERSORPTION_MODEL_HH

#include <dumux/common/numeqvector.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/discretization/defaultlocaloperator.hh>
#include <dumux/discretization/extrusion.hh>
#include <dumux/discretization/method.hh>
#include <dumux/flux/referencesystemformulation.hh>

#include <dumux/porousmediumflow/tracer/model.hh>
#include <dumux/porousmediumflow/tracer/localresidual.hh>

//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tags for the fully implicit tracer model with sorption.
namespace Dumux::Properties::TTag {
struct TracerSorption { using InheritsFrom = std::tuple<Tracer>; };
} // end namespace Dumux::Properties::TTag


//////////////////////////////////////////////////////////////////
// Extended local residual with added storage term modeling sorption with a distribution coefficient
//////////////////////////////////////////////////////////////////

namespace Dumux {

/*!
 * \ingroup TracerModel
 * \brief Element-wise calculation of the local residual for problems
 *        using fully implicit tracer model with sorption model.
 */
template<class TypeTag>
class TracerSorptionLocalResidual
: public TracerLocalResidual<TypeTag>
{
    using ParentType = TracerLocalResidual<TypeTag>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using Extrusion = Extrusion_t<GridGeometry>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;

    static constexpr int numComponents = ModelTraits::numFluidComponents();
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
    bool useKdModel = getParam<bool>("Problem.UseKdModel", false);
    Scalar solidBulkDensity = getParam<Scalar>("Problem.SolidBulkDensity", 1500);
    static constexpr int phaseIdx = 0;

public:
    using ParentType::ParentType;

    /*!
     * \brief Evaluates the amount of all conservation quantities
     *        (e.g. phase mass) within a sub-control volume.
     *
     * The result should be averaged over the volume (e.g. phase mass
     * inside a sub control volume divided by the volume)
     *
     * \param problem The problem
     * \param scv The sub control volume
     * \param volVars The primary and secondary variables on the scv
     */
    NumEqVector computeStorage(const Problem& problem,
                               const SubControlVolume& scv,
                               const VolumeVariables& volVars) const
    {
        NumEqVector storage = ParentType::computeStorage(problem, scv, volVars);

        if (!useKdModel)
            return storage;

        // formulation with mole balances
        if (useMoles)
        {
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                storage[compIdx] += volVars.moleFraction(phaseIdx, compIdx)
                                    * FluidSystem::kdValue(compIdx)
                                    * volVars.molarDensity()
                                    * solidBulkDensity;
            }
        }
        // formulation with mass balances
        else
        {
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                storage[compIdx] += volVars.massFraction(phaseIdx, compIdx)
                                * FluidSystem::kdValue(compIdx)
                                * volVars.density(phaseIdx)
                                * solidBulkDensity;
            }
        }

        return storage;
    }

    /*!
     * \brief Adds the storage derivative, including sorption terms
     *
     * \param partialDerivatives The partial derivatives
     * \param problem The problem
     * \param element The element
     * \param fvGeometry The finite volume geometry context
     * \param curVolVars The current volume variables
     * \param scv The sub control volume
     */
    template<class PartialDerivativeMatrix>
    void addStorageDerivatives(PartialDerivativeMatrix& partialDerivatives,
                               const Problem& problem,
                               const Element& element,
                               const FVElementGeometry& fvGeometry,
                               const VolumeVariables& curVolVars,
                               const SubControlVolume& scv) const
    {
        ParentType::addStorageDerivatives(partialDerivatives, problem, element, fvGeometry, curVolVars, scv);

        if (useKdModel)
        {
            const auto rho = useMoles ? curVolVars.molarDensity() : curVolVars.density();

            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                const auto d_storage = Extrusion::volume(fvGeometry, scv)*rho*solidBulkDensity*FluidSystem::kdValue(compIdx)/this->timeLoop().timeStepSize();
                partialDerivatives[compIdx][compIdx] += d_storage;
            }
        }
    }

};

///////////////////////////////////////////////////////////////////////////
// properties for the tracer model
///////////////////////////////////////////////////////////////////////////

namespace Properties {

//! Use the tracer-sorption local residual function for the tracer-sorption model
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::TracerSorption> { using type = TracerSorptionLocalResidual<TypeTag>; };

} // end namespace Properties
} // end namespace Dumux

#endif
