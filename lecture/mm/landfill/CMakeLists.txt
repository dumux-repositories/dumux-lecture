# SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

add_input_file_links()

dumux_add_test(NAME landfill
              TIMEOUT 1800
              SOURCES landfill.cc
              COMMAND ${DUMUX_RUNTEST}
              CMD_ARGS --script fuzzy
                       --files ${CMAKE_SOURCE_DIR}/lecture/references/landfill-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/test_I129-00020.vtu
                               ${CMAKE_SOURCE_DIR}/lecture/references/landfill_ri-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/test_I129_ri-00020.vtu
                       --command "${CMAKE_CURRENT_BINARY_DIR}/landfill -ParameterFile ${CMAKE_CURRENT_SOURCE_DIR}/landfill.input -TimeLoop.TEnd 1e6")
