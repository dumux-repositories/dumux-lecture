% SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
% SPDX-License-Identifier: CC-BY-4.0

\section{Henry Problem}
This exercise aims at examining two different approaches to modeling salt-water intrusion in an
aquifer.  A conceptual model for two-phase flow and another one for the conceptual idea of
single-phase two-component flow are both applied to the so-called {\it Henry problem}. 

This setting has become widely 
used as a benchmark verification example for many numerical models of density-dependent 
flow. The Henry problem specifies the process of salt-water intrusion in a homogeneous, 
confined aquifer (in this case here, we use $k= 1 \cdot 10^{-9}$ m$^2$ and $\phi=0.35$). 
The top and the bottom boundaries are closed, i.e. Neumann no-flow. At the left boundary, a constant fresh-water inflow 
($q=6.6 \cdot 10^{-5}$~m$^2$/s and $\mathsf{\rho}_f=1000$~kg/m$^3$) is given. At the open right boundary, 
a hydrostatic pressure distribution ($\mathsf{\rho}_f=1025$~kg/m$^3$)
is imposed, and in the upper part, this boundary is open for fresh-water outflow. 
A linear relative permeability--saturation relationship and zero capillarity are assumed
for the two-phase flow approach.

Due to the higher density, the salt-water tongue enters the system until it reaches a 
steady state solution. The fresh-water flow located on top of the salt-water leaves 
the system at the upper part of the right boundary. 
\begin{figure}[h]
   \centerline{\epsfig{file=./eps/henry.eps,width=0.8\textwidth}}
   %\caption{Setup of the Henry problem}
   \label{henry}
\end{figure}

\subsection {Two-phase flow approach}
This approach assumes salt-water and fresh-water as two different phases. As a 
consequence, diffusive exchange is not taken into account and a sharp interface seperating 
the two liquids is assumed (Fig. \ref{henrys}). 

\subsection {Single-phase two-component flow approach}
This approach considers only a single phase with the two components water 
and salt. Thus, a diffusive interface occurs (Fig. \ref{henryca}). The transition 
from regions of higher salt concentration to regions of lower concentration 
requires a concept capable to include diffusive exchange and transport of salt. 

\subsection {Simulation results}

Figures \ref{henrys} and \ref{henryca} show exemplary results for the Henry problem
as comparisons obtained with different approaches both in the analytical solutions
and the numerical models. Both figures show excellent agreement.

\begin{figure}[H]
    \centerline{\epsfig{file=./eps/henrys.eps,width=0.5\textwidth}}
    \caption{Comparison between numerical results and Henry's
             analytical solution for two-phase flow (sharp interface)}
    \label{henrys}
\end{figure}

\begin{figure}[H]
    \centerline{\epsfig{file=./eps/henryca.eps,width=0.5\textwidth}}
    \caption{Comparison between numerical results and Henry's
             analytical solution for single phase two-component flow (diffusive interface)}
    \label{henryca}
\end{figure}
 
%\newpage

\subsection{Questions}
\begin{enumerate}
\item Investigate and explain the influence of the following parameters on the steady-state
   conditions:
\begin{itemize}
   \item fresh-water flux rate (left boundary)
   \item molecular diffusion coefficient 
   \item dispersivities $\alpha_L$ and $\alpha_T$
\end{itemize}

\item Compare the computation times required for the two-phase flow approach
   and the single-phase two-component approach, maybe at different levels of mesh refinement.
 
\item Refine the mesh and describe the effects of refinement on both approaches.
\end{enumerate}
