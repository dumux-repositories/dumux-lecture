# SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

add_input_file_links()
dune_symlink_to_source_files(FILES grids)

dumux_add_test(NAME henry1p2c
              TIMEOUT 1800
              SOURCES henry1p2c.cc
              COMMAND ${DUMUX_RUNTEST}
              CMD_ARGS --script fuzzy
                       --files ${CMAKE_SOURCE_DIR}/lecture/references/Henry1p2c-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/Henry1p2c-00048.vtu
                       --command "${CMAKE_CURRENT_BINARY_DIR}/henry1p2c -ParameterFile ${CMAKE_CURRENT_SOURCE_DIR}/henry1p2c.input -Grid.File ${CMAKE_CURRENT_SOURCE_DIR}/grids/henry.dgf -TimeLoop.TEnd 1e10")
