// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/**
 * \file
 * \brief properties file for Henry exercise
  *
 */
#ifndef DUMUX_HENRY1P2C_PROPERTIES_HH
#define DUMUX_HENRY1P2C_PROPERTIES_HH

#include <dumux/discretization/box.hh>

#include <dumux/porousmediumflow/1pnc/model.hh>

#include <dune/grid/yaspgrid.hh>

#include "henry1p2cspatialparameters.hh"
#include "henry1p2cproblem.hh"

#include "watersaltfluidsystem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct Henry1p2cProblemTypeTag { using InheritsFrom = std::tuple<BoxModel, OnePNC>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Henry1p2cProblemTypeTag> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Henry1p2cProblemTypeTag> { using type = Henry1p2cProblem<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Henry1p2cProblemTypeTag> { using type = WaterSaltFluidSystem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Henry1p2cProblemTypeTag>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = Henry1p2cSpatialParams<FVGridGeometry, Scalar>;
};

template<class TypeTag>
struct EnableCompositionalDispersion<TypeTag, TTag::Henry1p2cProblemTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct CompositionalDispersionModel<TypeTag, TTag::Henry1p2cProblemTypeTag> { using type = ScheideggersDispersionTensor<TypeTag>; };

//Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::Henry1p2cProblemTypeTag> { static constexpr bool value = false; };

} // end namespace Dumux::Properties

#endif
