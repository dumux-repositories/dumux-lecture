// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef DUMUX_MCWHORTER_ANALYTIC_HH
#define DUMUX_MCWHORTER_ANALYTIC_HH

/**
 * @file
 * @brief  Analytic solution of
 * the McWhorter problem
 * @author Markus Wolff, Anneli Schöniger
 */

#include <dumux/common/properties.hh>
#include <dumux/porousmediumflow/2p/sequential/properties.hh>

namespace Dumux {
/**
 * @brief Analytic solution of
 * the McWhorter problem
 *
 * for naming of variables see "An Improved Semi-Analytical Solution for Verification
 * of Numerical Models of Two-Phase Flow in Porous Media"
 * (R. Fucik, J. Mikyska, M. Benes, T. H. Illangasekare; 2007)
 */

template<typename TypeTag>
class McWhorterAnalytic
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using PrimaryVariables = typename GetProp<TypeTag, Properties::SolutionTypes>::PrimaryVariables;
    using Indices = GetPropType<TypeTag, Properties::Indices>;
    static constexpr int dimworld = GridView::dimensionworld;
    static constexpr int wPhaseIdx = Indices::wPhaseIdx;
    static constexpr int nPhaseIdx = Indices::nPhaseIdx;
    static constexpr int saturationIdx = Indices::saturationIdx;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;
    using BlockVector = Dune::BlockVector<Dune::FieldVector<Scalar, 1> >;
    using Element = typename GridView::Traits::template Codim<0>::Entity;
    using ElementIterator = typename GridView::template Codim<0>::Iterator;

public:

    // functions needed for analytical solution
    void initializeAnalytic()
    {
        analyticSolution_.resize(size_);
        analyticSolution_=0;
        error_.resize(size_);
        error_=0;
        elementVolume_.resize(size_);
        elementVolume_=0;
    }

    void calcSatError()
    {
        error_=0;
        elementVolume_=0;

        ElementIterator eItEnd = problem_.gridView().template end<0> ();

        for (ElementIterator eIt = problem_.gridView().template end<0> (); eIt!= eItEnd; ++eIt)
        {
            // get entity
            const Element& element = *eIt;
            int index = problem_.variables().index(*eIt);
            elementVolume_[index]= element.geometry().volume();
        }

        for (int i=0; i<size_; i++)
        {
            error_[i]=analyticSolution_[i]-problem_.variables().cellData(i).saturation(wPhaseIdx);;
        }
    }

    void prepareAnalytic()
    {
        const auto& dummyElement = *problem_.gridView().template begin<0>();
        const auto fluidMatrixInteraction = problem_.spatialParams().fluidMatrixInteractionAtPos(dummyElement.geometry().center());
        swr_ = fluidMatrixInteraction.effToAbsParams().swr();
        snr_ = fluidMatrixInteraction.effToAbsParams().snr();
        porosity_ = problem_.spatialParams().porosity(dummyElement);
        permeability_ = problem_.spatialParams().intrinsicPermeability(dummyElement);
        PrimaryVariables initVec;
        problem_.initialAtPos(initVec, dummyGlobal_);
        sInit_ = initVec[saturationIdx];
        Scalar s0 =(1 - snr_ - swr_);
        time_=0;
        h_= (s0)/intervalNum_;

        // define saturation range for analytic solution
        satVec_= swr_;
        for (int i=1; i<pointNum_; i++)
        {
            satVec_[i]=satVec_[i-1]+h_;
        }

        FluidState fluidState;
        fluidState.setTemperature(problem_.temperatureAtPos(dummyGlobal_));
        fluidState.setPressure(wPhaseIdx, problem_.referencePressureAtPos(dummyGlobal_));
        fluidState.setPressure(nPhaseIdx, problem_.referencePressureAtPos(dummyGlobal_));
        Scalar viscosityW = FluidSystem::viscosity(fluidState, wPhaseIdx);
        Scalar viscosityNW = FluidSystem::viscosity(fluidState, nPhaseIdx);

        // get fractional flow function vector
        for (int i=0; i<pointNum_; i++)
        {
            fractionalW_[i] = fluidMatrixInteraction.krw(satVec_[i])/viscosityW;
            fractionalW_[i] /= (fractionalW_[i] + fluidMatrixInteraction.krn(satVec_[i])/viscosityNW);
        }

        // get capillary pressure derivatives
        dpcdsw_=0;

        for (int i=0; i<pointNum_; i++)
        {
            dpcdsw_[i] = fluidMatrixInteraction.dpc_dsw(satVec_[i]);
        }

        // set initial fW
        if (sInit_ == 0)
        fInit_=0;

        else
        fInit_=fractionalW_[0];

        fractionalW_[0]=0;

        // normalize fW
        for (int i=0; i<pointNum_; i++)
        {
            fn_[i]= r_ * (fractionalW_[i] - fInit_)/ (1 - r_ * fInit_);
        }

        // diffusivity function
        for (int i=0; i<pointNum_; i++)
        {
            d_[i] = fractionalW_[i]*(-dpcdsw_[i])*(fluidMatrixInteraction.krn(satVec_[i])/viscosityNW)*permeability_;
        }

        // gk_: part of fractional flow function
        // initial guess for gk_
        for (int i=0; i<pointNum_; i++)
        {
            gk_[i] = d_[i]/(1-fn_[i]);
        }

        gk_[0] = 0;
    }

    void updateExSol()
    {
        // with displacing phase flux at boundary q0 = A * 1/sqrt(t)
        // Akm1, Ak: successive approximations of A
        double Ak = 0;
        double Akm1 = 0;
        double diff = 1e100;

        // partial numerical integrals a_, b_
        a_=0, b_=0;
        fp_=0;

        // approximation of integral I
        double I0 = 0;
        double Ii = 0;

        int k = 0;

        while (diff> tolAnalytic_)
        {
            k++;
            if (k> 50000)
            {
                std::cout<<"Analytic solution: Too many iterations!"<<std::endl;
                break;
            }

            Akm1=Ak;
            I0=0;
            for (int i=0; i<intervalNum_; i++)
            {
                a_[i] = 0.5 * h_ * sInit_ *(gk_[i] + gk_[i+1])+ pow(h_, 2) / 6* ((3* i + 1) * gk_[i]
                        + (3 * i + 2) * gk_[i+1]);
                b_[i] = 0.5 * h_ * (gk_[i] + gk_[i+1]);
                I0 += (a_[i] - sInit_ * b_[i]);
            }

            gk_[0]=0;
            for (int i=1; i<pointNum_; i++)
            {
                Ii=0;
                for (int j = i; j<intervalNum_; j++)
                Ii += (a_[j] - satVec_[i] * b_[j]);
                //gk_[i] = d_[i] + gk_[i]*(fn_[i] + Ii/I0); // method A
                gk_[i] = (d_[i] + gk_[i]*fn_[i])/(1 - Ii/I0); // method B
            }

            // with f(sInit) = 0: relationship between A and sInit
            Ak = pow((0.5*porosity_/pow((1 - fInit_), 2)*I0), 0.5);
            diff=fabs(Ak - Akm1);
        }

        // fp_: first derivative of f
        for (int i = 0; i<pointNum_; i++)
        {
            for (int j = i; j<intervalNum_; j++)
            fp_[i] += b_[j]/I0;
        }
        for (int i = 0; i<pointNum_; i++)
        {
            xf_[i]= 2 * Ak * (1 - fInit_ * r_)/ porosity_ * fp_[i]* pow(time_, 0.5);
        }

        // iterate over vertices and get analytical saturation solution
        ElementIterator eItEnd = problem_.gridView().template end<0> ();
        for (ElementIterator eIt = problem_.gridView().template begin<0> (); eIt!= eItEnd; ++eIt)
        {
            // get global coordinate of cell center
            const GlobalPosition& globalPos = eIt->geometry().center();

            // find index of current vertex
            int index = problem_.variables().index(*eIt);

            // find x_f next to global coordinate of the vertex
            int xnext = 0;
            for (int i=intervalNum_; i>0; i--)
            {
                if (globalPos[0] <= xf_[i])
                {
                    xnext = i;
                    break;
                }
            }

            // account for the area not yet reached by the front
            if (globalPos[0]> xf_[0])
            {
                analyticSolution_[index] = sInit_;
                continue;
            }

            if (globalPos[0] <= xf_[0])
            {
                analyticSolution_[index] = satVec_[xnext];
                continue;
            }
        }
        // call function to calculate the saturation error
        calcSatError();
    }

    void calculateAnalyticSolution()
    {
        time_ += problem_.timeManager().timeStepSize();
        updateExSol();
    }

    BlockVector AnalyticSolution() const
    {
        return analyticSolution_;
    }

    //Write saturation and pressure into file
    template<class MultiWriter>
    void addOutputVtkFields(MultiWriter &writer)
    {
        BlockVector *analyticSolution = writer.allocateManagedBuffer (size_);
        BlockVector *error = writer.allocateManagedBuffer (size_);

        *analyticSolution = analyticSolution_;
        *error = error_;

        writer.attachCellData(*analyticSolution, "saturation (exact solution)");
        writer.attachCellData(*error, "error_");
    }


    void initialize()
    {
        initializeAnalytic();
        prepareAnalytic();
    }

    McWhorterAnalytic(Problem& problem, Scalar tolAnalytic = 1e-14)
        : problem_(problem)
        , analyticSolution_(0)
        , error_(0)
        , elementVolume_(0)
        , size_(problem.gridView().size(0))
        , tolAnalytic_(tolAnalytic)
    {
        dummyGlobal_ = 0.0;
        dummyGlobal_[0] = 1.0;
    }

private:
    Problem& problem_;

    BlockVector analyticSolution_;
    BlockVector error_;
    BlockVector elementVolume_;

    int size_;
    GlobalPosition dummyGlobal_;

    Scalar tolAnalytic_;
    Scalar r_;

    Scalar swr_;
    Scalar snr_;
    Scalar porosity_;
    Scalar sInit_;
    Scalar time_;
    Scalar permeability_;

    static constexpr int intervalNum_ = 1000, pointNum_ = intervalNum_+1 ;

    Dune::FieldVector<Scalar, pointNum_> satVec_;
    Dune::FieldVector<Scalar,pointNum_> fractionalW_;
    Dune::FieldVector<Scalar, pointNum_> dpcdsw_;
    Dune::FieldVector<Scalar, pointNum_> fn_;
    Dune::FieldVector<Scalar, pointNum_> d_;
    Dune::FieldVector<Scalar, pointNum_> gk_;
    Dune::FieldVector<Scalar, pointNum_> xf_;
    Scalar fInit_;
    Scalar h_;
    Dune::FieldVector<Scalar,intervalNum_> a_;
    Dune::FieldVector<Scalar,intervalNum_> b_;
    Dune::FieldVector<Scalar,pointNum_> fp_;

};

} // end namespace Dumux

#endif
