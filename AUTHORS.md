The DuMux Lecture contributors are

| Year            | Name                   |
|-----------------|------------------------|
| 2017-2018       | Sina Ackermann         |
| 2008-2012       | Katherina Baber        |
| 2015            | Martin Beck            |
| 2022-2024       | Ivan Buntic            |
| 2012-2021, 2023-2024 | Holger Class           |
| 2018-2021       | Edward 'Ned' Coltman   |
| 2012-2013       | Melanie Darcis         |
| 2018-2020       | Simon Emmert           |
| 2015-2017       | Thomas Fetzer          |
| 2011-2021       | Bernd Flemisch         |
| 2018, 2020-2021 | Dennis Gläser          |
| 2012-2016       | Christoph Grüninger    |
| 2016-2020, 2022 | Katharina Heck         |
| 2015-2016, 2019 | Johannes Hommel        |
| 2012            | Johannes Irtenkauf     |
| 2024            | Leon Keim              |
| 2020, 2022-2024 | Mathis Kelm            |
| 2021-2022       | Stefanie Kiemle        |
| 2014            | Alexander Kissinger    |
| 2015-2024       | Timo Koch              |
| 2018            | Melanie Lipp           |
| 2011            | Andreas Lauser         |
| 2023            | Hamza Oukili           |
| 2012            | Karen Schmid           |
| 2014-2017       | Martin Schneider       |
| 2018-2020, 2022 | Theresa Schollenberger |
| 2012-2015       | Nicolas Schwenck       |
| 2018            | Gabriele Seitz         |
| 2020,2024       | Maziar Veyskarami      |
| 2012-2014       | Lena Walter            |
| 2022, 2024      | Yue Wang               |
| 2020            | Felix Weinhardt        |
| 2015-2020       | Kilian Weishaupt       |
| 2018            | Kai Wendel             |
| 2019, 2023      | Roman Winter           |
| 2012            | Markus Wolff           |
| 2021            | Hanchuan Wu            |
