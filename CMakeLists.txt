# SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

cmake_minimum_required(VERSION 3.22)

project("dumux-lecture" C CXX)

if(NOT (dune-common_DIR
        OR dune-common_ROOT
        OR "${CMAKE_PREFIX_PATH}" MATCHES ".*dune-common.*"))
    string(REPLACE  ${CMAKE_PROJECT_NAME}
      dune-common dune-common_DIR
      ${PROJECT_BINARY_DIR})
endif()

#find dune-common and set the module path
find_package(dune-common REQUIRED)
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/modules"
  ${dune-common_MODULE_PATH})

#include the dune macros
include(DuneMacros)

# start a dune project with information from dune.module
dune_project()

# find test executable
find_file(DUMUX_RUNTEST
  NAMES runtest.py
  HINTS ${dumux_INCLUDE_DIRS}
  PATH_SUFFIXES bin/testing bin
  REQUIRED NO_DEFAULT_PATH
)

add_subdirectory(lecture EXCLUDE_FROM_ALL)

# finalize the dune project, e.g. generating config.h etc.
finalize_dune_project()
