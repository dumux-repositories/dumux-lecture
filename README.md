Readme
======

What is DuMuX-Lecture?
----------------------

DuMuX-Lecture contains several numerical examples for modeling flow
and transport processes in porous media. It is based on the open-source
porous-media simulator [DuMuX][0]. The examples are used in the lectures
of the [LH2 group at the University of Stuttgart][1]. They are contained
in subfolders of the folder `lecture`.


Installation
------------

DuMuX-Lecture is a DUNE module that depends on DuMuX. It can be installed
like DuMuX itself, please refer to the DuMuX and DUNE installation
instructions for details.


License
-------

DuMuX-Lecture is licensed under the terms and conditions of the GNU
General Public License (GPL) version 3 or - at your option - any later
version. The [GPL can be read online][2], or in the `LICENSE.md` file
provided in the topmost directory of the DuMuX source code tree.

Please note that DuMuX-Lecture's license, unlike DUNE, does NOT feature
a template exception to the GNU General Public License. This means that
you must publish any source code which uses any of the DuMuX-Lecture
header files if you want to redistribute your program to third parties.
If this is unacceptable to you, please [contact us][1] for a commercial
license.

See the file `LICENSE.md` for full copying permissions.


Automated Testing
------------------
* Master branch (development): [![master build badge](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture/badges/master/pipeline.svg)](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture/-/pipelines?page=1&scope=all&ref=master)
* Latest release: [![release build badge](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture/badges/releases/3.4/pipeline.svg)](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-lecture/-/pipelines?page=1&scope=all&ref=releases/3.4)


DuMu<sup>x</sup> lecture contains test problems for every example that
are continuously and automatically tested in the GitLab-CI framework (see badges).
The test suite is based on CTest and can also be built and run manually
```
make -j8 build_tests && ctest -j8
```

[0]: http://dumux.org
[1]: http://www.hydrosys.uni-stuttgart.de/index.en.php
[2]: https://www.gnu.org/licenses/gpl-3.0.en.html
