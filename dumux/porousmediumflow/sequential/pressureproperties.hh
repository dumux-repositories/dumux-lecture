// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef DUMUX_PRESSURE_PROPERTIES_HH
#define DUMUX_PRESSURE_PROPERTIES_HH

//Dune-includes
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>

#include "properties.hh"


/*!
 * \ingroup Sequential
 * \ingroup IMPETProperties
 */
/*!
 * \file
 * \brief Base file for properties related to sequential IMPET algorithms
 */
namespace Dumux
{
namespace Properties
{
/*!
 *
 * \brief General properties for sequential IMPET algorithms
 *
 * This class holds properties necessary for the sequential IMPET solution.
 */

//////////////////////////////////////////////////////////////////
// Type tags tags
//////////////////////////////////////////////////////////////////

//! The type tag for models based on the diffusion-scheme
// Create new type tags
namespace TTag {
struct Pressure { using InheritsFrom = std::tuple<SequentialModel>; };
} // end namespace TTag

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
//Properties for linear solvers
template<class TypeTag, class MyTypeTag>
struct PressureRHSVector { using type = UndefinedProperty; };//!< Type of the right hand side vector given to the linear solver
template<class TypeTag, class MyTypeTag>
struct PressureSolutionVector { using type = UndefinedProperty; };//!Type of solution vector or pressure system
template<class TypeTag, class MyTypeTag>
struct VisitFacesOnlyOnce { using type = UndefinedProperty; }; //!< Indicates if faces are only regarded from one side
}
}

#include <dumux/porousmediumflow/sequential/cellcentered/velocitydefault.hh>

namespace Dumux
{
namespace Properties
{
//! Faces are only regarded from one side and not from both cells
template<class TypeTag>
struct VisitFacesOnlyOnce<TypeTag, TTag::Pressure> { static constexpr bool value = false; };

//Set defaults
template<class TypeTag>
struct PressureCoefficientMatrix<TypeTag, TTag::Pressure>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using MB = Dune::FieldMatrix<Scalar, 1, 1>;

public:
    using type = Dune::BCRSMatrix<MB>;
};
template<class TypeTag>
struct PressureRHSVector<TypeTag, TTag::Pressure>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

public:
    using type = Dune::BlockVector<Dune::FieldVector<Scalar, 1> >;
};

template<class TypeTag>
struct PressureSolutionVector<TypeTag, TTag::Pressure> { using type = typename GetProp<TypeTag, SolutionTypes>::ScalarSolution; };


template<class TypeTag>
struct Velocity<TypeTag, TTag:: Pressure> { using type = FVVelocityDefault<TypeTag>; };

}
}

#endif
