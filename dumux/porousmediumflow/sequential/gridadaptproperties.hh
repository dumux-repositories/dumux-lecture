// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \ingroup IMPETProperties
 * \ingroup IMPET
 * \file
 *
 * \brief Defines a type tag and some fundamental properties for
 *        linear solvers
 */
#ifndef DUMUX_GRIDADAPT_PROPERTIES_HH
#define DUMUX_GRIDADAPT_PROPERTIES_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

namespace Dumux
{
// forward declarations
template<class TypeTag>
class GridAdaptInitializationIndicatorDefault;

template<class TypeTag, bool>
class GridAdapt;


namespace Properties
{
//! Grid adaption type tag for all sequential models.
namespace TTag {
struct GridAdapt {};
}

//! Defines if the grid is h-adaptive
template<class TypeTag, class MyTypeTag>
struct  AdaptiveGrid { using type = UndefinedProperty; };

//! The type of grid adaptation
template<class TypeTag, class MyTypeTag>
struct  GridAdaptModel  { using type = UndefinedProperty; };

//! Class defining the refinement/coarsening indicator
template<class TypeTag, class MyTypeTag>
struct AdaptionIndicator { using type = UndefinedProperty; };

//! Class defining the refinement/coarsening indicator for grid initialization
template<class TypeTag, class MyTypeTag>
struct AdaptionInitializationIndicator { using type = UndefinedProperty; };

//! Tolerance for refinement
template<class TypeTag, class MyTypeTag>
struct GridAdaptRefineThreshold { using type = UndefinedProperty; };

//! Tolerance for coarsening
template<class TypeTag, class MyTypeTag>
struct GridAdaptCoarsenThreshold { using type = UndefinedProperty; };

//no adaptive grid
template<class TypeTag>
struct AdaptiveGrid<TypeTag, TTag::GridAdapt> { static constexpr bool value = false; };

//Set default class for adaptation initialization indicator
template<class TypeTag>
struct AdaptionInitializationIndicator<TypeTag, TTag::GridAdapt> { using type = GridAdaptInitializationIndicatorDefault<TypeTag>; };
//Set default class for adaptation
template<class TypeTag>
struct GridAdaptModel<TypeTag, TTag::GridAdapt> { using type = GridAdapt<TypeTag, getPropValue<TypeTag, Properties::AdaptiveGrid>()>; };

//standard setting
template<class TypeTag>
struct GridAdaptRefineThreshold<TypeTag, TTag::GridAdapt> { static constexpr auto value  = 0.0; };
template<class TypeTag>
struct GridAdaptCoarsenThreshold<TypeTag, TTag::GridAdapt> { static constexpr auto value  = 0.0; };
} // namespace Properties
} // namespace Dumux


#endif
