// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
#ifndef DUMUX_VELOCITYDEFAULT_HH
#define DUMUX_VELOCITYDEFAULT_HH

/**
 * @file
 * @brief  Default implementation of velocity class.
 */

#include <dune/grid/common/gridenums.hh>

#include <dumux/porousmediumflow/sequential/properties.hh>

namespace Dumux
{
//! \ingroup IMPET
/*! \brief Default implementation of a velocity class.
 *
 * If the velocity is reconstructed in the pressure model this default implementation is used in the transport model.
 *
 * \tparam TypeTag The Type Tag
 */
template<class TypeTag>
class FVVelocityDefault
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;

    using CellData = GetPropType<TypeTag, Properties::CellData>;
    using Intersection = typename GridView::Intersection;


public:
    //! Constructs a FVVelocityDefault object
    /*!
     * \param problem A problem class object
     */
    FVVelocityDefault(Problem& problem)
    {}

    //! For initialization
    void initialize()
    {}

    //! Local velocity calculation
    void calculateVelocity(const Intersection& intersection, CellData& cellData)
    {}

    //! Local velocity calculation
    void calculateVelocityOnBoundary(const Intersection& intersection, CellData& cellData)
    {}

    //! \brief Indicates if velocity is reconstructed the transport step
    bool calculateVelocityInTransport()
    {
        return false;
    }

    /*! \brief Adds velocity output to the output file
     *
     * \tparam MultiWriter Class defining the output writer
     * \param writer The output writer (usually a <tt>VTKMultiWriter</tt> object)
     *
     */
    template<class MultiWriter>
    void addOutputVtkFields(MultiWriter &writer)
    {}


};
}
#endif
