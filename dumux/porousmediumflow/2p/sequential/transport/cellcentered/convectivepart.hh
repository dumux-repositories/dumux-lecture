// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup SequentialTwoPModel
 * \brief  Base class for defining a convective part of the saturation transport equation.
 */
#ifndef DUMUX_CONVECTIVEPART_HH
#define DUMUX_CONVECTIVEPART_HH

#include <dumux/porousmediumflow/2p/sequential/properties.hh>

namespace Dumux {

/*!
 * \ingroup SequentialTwoPModel
 * \brief  Base class for defining a convective part of the saturation transport equation.
 *
 * \tparam TypeTag The problem TypeTag
 */
template<class TypeTag>
class ConvectivePart
{
private:
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
      using Scalar = GetPropType<TypeTag, Properties::Scalar>;
      using Problem = GetPropType<TypeTag, Properties::Problem>;

    enum{dimWorld = GridView::dimensionworld};
    using Intersection = typename GridView::Intersection;
    using DimVector = Dune::FieldVector<Scalar, dimWorld>;

public:
    //! For initialization
    void initialize()
    {}

    /*!
     * \brief Returns convective term for current element face
     *  \param intersection  Intersection of two grid elements/global boundary
     *  \param sat           Saturation of current element
     *  \return     Convective flux
     */
    Scalar getFlux(const Intersection& intersection, const Scalar sat) const
    {
        return 0.0;
    }

    /*!
     * \brief Returns convective term for current intersection
     *
     *  \param flux        Flux vector (gets the flux from the function)
     *  \param intersection  Intersection of two grid elements/global boundary
     *  \param satI           Saturation of current element
     *  \param satJ           Saturation of neighbor element
     */
    void getFlux(DimVector& flux, const Intersection& intersection, const Scalar satI, const Scalar satJ) const
    {}

    /*!
     * \brief Constructs a ConvectivePart object
     *
     *  \param problem A problem class object
     */
    ConvectivePart(Problem& problem)
    {}

};
} // end namespace Dumux

#endif
