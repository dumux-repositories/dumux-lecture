// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup SequentialTwoPModel
 * \brief Properties related to the sequential IMPES algorithms.
 */
#ifndef DUMUX_IMPES2P_PROPERTIES_HH
#define DUMUX_IMPES2P_PROPERTIES_HH

#include <dumux/porousmediumflow/sequential/impetproperties.hh>
#include <dumux/porousmediumflow/2p/sequential/properties.hh>

namespace Dumux {
namespace Properties {

//////////////////////////////////////////////////////////////////
// Type tags tags
//////////////////////////////////////////////////////////////////

//! TypeTag for the two-phase IMPES scheme
// Create new type tags
namespace TTag {
struct IMPESTwoP { using InheritsFrom = std::tuple<SequentialTwoP, IMPET>; };
} // end namespace TTag

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
} // end namespace Properties
} // end namespace Dumux

#include <dumux/porousmediumflow/2p/sequential/impes/problem.hh>

namespace Dumux {
namespace Properties {
} // end namespace Properties
} // end namespace Dumux

#endif
