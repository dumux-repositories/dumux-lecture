// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup SequentialTwoPModel
 * \brief Properties for adaptive implementations of the sequential IMPES algorithms
 */
#ifndef DUMUX_IMPES2PADAPTIVE_PROPERTIES_HH
#define DUMUX_IMPES2PADAPTIVE_PROPERTIES_HH

#include <dumux/porousmediumflow/sequential/impetproperties.hh>
#include <dumux/porousmediumflow/2p/sequential/properties.hh>

namespace Dumux {
namespace Properties {
/*!
 * \ingroup SequentialTwoPModel
 * \brief General properties for adaptive implementations of the sequential IMPES algorithms
 */

//////////////////////////////////////////////////////////////////
// Type tags tags
//////////////////////////////////////////////////////////////////

//!  TypeTag for grid-adaptive two-phase IMPES scheme
// Create new type tags
namespace TTag {
struct IMPESTwoPAdaptive { using InheritsFrom = std::tuple<SequentialTwoP, IMPET>; };
} // end namespace TTag

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
} // end namespace Properties
} // end namespace Dumux

#include <dumux/porousmediumflow/sequential/variableclassadaptive.hh>
#include <dumux/porousmediumflow/2p/sequential/celldataadaptive.hh>
#include "gridadaptionindicator.hh"
#include <dumux/porousmediumflow/2p/sequential/impes/problem.hh>
#include <dumux/porousmediumflow/sequential/gridadaptinitializationindicator.hh>

namespace Dumux {
namespace Properties {
//! Enable adaptive grid
template<class TypeTag>
struct AdaptiveGrid<TypeTag, TTag::IMPESTwoPAdaptive> { static constexpr bool value = true; };
//! Set variable class for adaptive impet schemes
template<class TypeTag>
struct Variables<TypeTag, TTag::IMPESTwoPAdaptive> { using type = VariableClassAdaptive<TypeTag>; };
//! Set cell data class for adaptive two-phase IMPES schemes
template<class TypeTag>
struct CellData<TypeTag, TTag::IMPESTwoPAdaptive> { using type = CellData2PAdaptive<TypeTag>; };
//! Set the standard indicator class of two-phase models for adaption or coarsening
template<class TypeTag>
struct AdaptionIndicator<TypeTag, TTag::IMPESTwoPAdaptive> { using type = GridAdaptionIndicator2P<TypeTag>; };
//! Set default class for adaptation initialization indicator
template<class TypeTag>
struct AdaptionInitializationIndicator<TypeTag, TTag::IMPESTwoPAdaptive> { using type = GridAdaptInitializationIndicator<TypeTag>; };
} // end namespace Properties
} // end namespace Dumux

#endif
