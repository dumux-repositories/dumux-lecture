// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup SequentialOnePModel
 * \brief Defines the indices required for the sequential one-phase model.
 */

#ifndef DUMUX_SEQUENTIAL_1P_INDICES_HH
#define DUMUX_SEQUENTIAL_1P_INDICES_HH

namespace Dumux {
/*!
 * \ingroup SequentialOnePModel
 */
// \{

/*!
 * \brief The common indices for the 1-p models.
 */
struct SequentialOnePCommonIndices
{
   static const int pressureEqIdx = 0;  //!< Index of the pressure equation
};

// \}
} // namespace Dumux


#endif
