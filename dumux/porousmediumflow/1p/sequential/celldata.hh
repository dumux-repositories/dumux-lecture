// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup SequentialOnePModel
 * \brief  Class including data of one grid cell.
 */

#ifndef DUMUX_CELLDATA1P_HH
#define DUMUX_CELLDATA1P_HH

#include "properties.hh"
#include "fluxdata.hh"


namespace Dumux {
template<class TypeTag>
class FluxData1P;

/*!
 * \ingroup SequentialOnePModel
 * \brief Class including data of one grid cell.
 *
 * The variables of one-phase flow, which are the pressure as well as
 * additional data assigned to cell-cell interfaces,
 * so-called flux-data, are stored.
 *
 * \tparam TypeTag The problem TypeTag
 */
template<class TypeTag>
class CellData1P
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluxData = FluxData1P<TypeTag>;

private:
    Scalar pressure_;
    FluxData fluxData_;

public:

    //! Constructs a CellData1P object
    CellData1P() :
        pressure_(0.0)
    {
    }
    //! Returns the flux data of the cell
    FluxData& fluxData()
    {
        return fluxData_;
    }
    //! Returns the flux data of the cell
    const FluxData& fluxData() const
    {
        return fluxData_;
    }

    ////////////////////////////////////////////////////////////
    // functions returning primary variables
    ////////////////////////////////////////////////////////////

    //! Returns the cell pressure
    Scalar pressure()
    {
        return pressure_;
    }
    //! Returns the cell pressure
    Scalar pressure() const
    {
        return pressure_;
    }
    //! Sets the cell pressure
    void setPressure(Scalar press)
    {
        pressure_ = press;
    }
};

} // end namespace Dumux
#endif
