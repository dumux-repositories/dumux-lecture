// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup SequentialOnePModel
 * \brief Specifies the properties for 1p pressure models.
 */

#ifndef DUMUX_DIFFUSION_PROPERTIES_1P_HH
#define DUMUX_DIFFUSION_PROPERTIES_1P_HH

#include <dumux/porousmediumflow/sequential/pressureproperties.hh>
#include <dumux/porousmediumflow/1p/sequential/properties.hh>


namespace Dumux {
namespace Properties {
// \{

//////////////////////////////////////////////////////////////////
// Type tags tags
//////////////////////////////////////////////////////////////////

//! The type tag for 1p pressure models
// Create new type tags
namespace TTag {
struct PressureOneP { using InheritsFrom = std::tuple<SequentialOneP, Pressure>; };
} // end namespace TTag

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
} // end namespace Properties
} // end namespace Dumux

namespace Dumux {
namespace Properties {
//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
} // end namespace Properties
} // end namespace Dumux

#endif
