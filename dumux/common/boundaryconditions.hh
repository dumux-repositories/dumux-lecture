// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Lecture contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Common
 * \brief Definition of boundary condition types, extend if necessary
 * \todo can this be removed for the sake of boundarytypes.hh?
 */
#ifndef DUMUX_BOUNDARYCONDITIONS_HH
#define DUMUX_BOUNDARYCONDITIONS_HH

namespace Dumux {

/*!
 * \ingroup Common
 * \brief Define a class containing boundary condition flags
 */
struct BoundaryConditions
{
    /** \brief These values are ordered according to precedence */
    enum Flags {
        outflow = 0, //!< An outflow boundary
        neumann = 1, //!< Neumann boundary
        process = 2, //!< Processor boundary
        dirichlet = 3 //!< Dirichlet boundary
    };
};

} // end namespace Dumux

#endif
